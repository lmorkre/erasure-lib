/*
 * GF-Complete: A Comprehensive Open Source Library for Galois Field Arithmetic
 * James S. Plank, Ethan L. Miller, Kevin M. Greenan,
 * Benjamin A. Arnold, John A. Burnum, Adam W. Disney, Allen C. McBride.
 *
 *
 * gf_lar2.c
 * taken from gf_example_5.c
 *
 */

#include <stdio.h>
#include <getopt.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "gf_complete.h"
#include "gf_rand.h"

int         l_gf16_multiply_region_from_2(void *src, void *dst, int size, uint32_t m, uint32_t ipp);
gf_val_32_t l_gf16_multiply(uint32_t a16, uint32_t b16, uint32_t i_prim_poly);
void        l_print_i128(__m128i *in1);
uint16_t    l_mult_matrix_vector_i128(uint16_t *in_array, uint16_t mult1);
uint16_t    l_mult_matrix_by_hand(uint16_t *in_array, uint16_t mult1, int size);
int         l_shift_multiply(uint32_t a32, uint32_t b32);
int         l_matrix_translate_column(void *column_data, void *dst_row, int items);
inline int  l_perf_start(struct timeval *tv);
inline int  l_perf_stop(struct timeval *tv);
inline void l_perf_print(struct timeval *stop, struct timeval *start, long long dsize);
inline void l_perf_print2(struct timeval *stop, struct timeval *start, long long count);
static void l_gf_w16_split_4_16_lazy_sse_altmap_multiply_region(void *src, void *dest, int bytes, gf_val_32_t val, int xor, int pp);

#define DSIZE 1024
#define SIMD_DSIZE 4096
#define MULTI_ITER 80000
#define MATRIX_ITER 800000

typedef struct l_int128 {
    uint64_t  high;
    uint64_t  low;
} l_int128_t;

#define INT128_HIGH(ll)    (uint64_t)(ll->high)
#define INT128_LOW(ll)     (uint64_t)(ll->low)
#define INT128_INT16W7(ll) (uint16_t)(((uint64_t)(ll->high)>>48) & 0xffff)
#define INT128_INT16W6(ll) (uint16_t)(((uint64_t)(ll->high)>>32) & 0xffff)
#define INT128_INT16W5(ll) (uint16_t)(((uint64_t)(ll->high)>>16) & 0xffff)
#define INT128_INT16W4(ll) (uint16_t)(((uint64_t)(ll->high))  & 0xffff)
#define INT128_INT16W3(ll) (uint16_t)(((uint64_t)(ll->low)>>48) & 0xffff)
#define INT128_INT16W2(ll) (uint16_t)(((uint64_t)(ll->low)>>32) & 0xffff)
#define INT128_INT16W1(ll) (uint16_t)(((uint64_t)(ll->low)>>16) & 0xffff)
#define INT128_INT16W0(ll) (uint16_t)(((uint64_t)(ll->low))  & 0xffff)

#define INT128_INT16W7_SUBWORD3(ll) (uint8_t)(((INT128_INT16W7(ll))>>12)& 0xf)
#define INT128_INT16W7_SUBWORD2(ll) (uint8_t)(((INT128_INT16W7(ll))>>8) & 0xf)
#define INT128_INT16W7_SUBWORD1(ll) (uint8_t)(((INT128_INT16W7(ll))>>4) & 0xf)
#define INT128_INT16W7_SUBWORD0(ll) (uint8_t)(((INT128_INT16W7(ll))) & 0xf)
#define INT128_INT16W6_SUBWORD3(ll) (uint8_t)(((INT128_INT16W6(ll))>>12)& 0xf)
#define INT128_INT16W6_SUBWORD2(ll) (uint8_t)(((INT128_INT16W6(ll))>>8) & 0xf)
#define INT128_INT16W6_SUBWORD1(ll) (uint8_t)(((INT128_INT16W6(ll))>>4) & 0xf)
#define INT128_INT16W6_SUBWORD0(ll) (uint8_t)(((INT128_INT16W6(ll))) & 0xf)
#define INT128_INT16W5_SUBWORD3(ll) (uint8_t)(((INT128_INT16W5(ll))>>12)& 0xf)
#define INT128_INT16W5_SUBWORD2(ll) (uint8_t)(((INT128_INT16W5(ll))>>8) & 0xf)
#define INT128_INT16W5_SUBWORD1(ll) (uint8_t)(((INT128_INT16W5(ll))>>4) & 0xf)
#define INT128_INT16W5_SUBWORD0(ll) (uint8_t)(((INT128_INT16W5(ll))) & 0xf)
#define INT128_INT16W4_SUBWORD3(ll) (uint8_t)(((INT128_INT16W4(ll))>>12)& 0xf)
#define INT128_INT16W4_SUBWORD2(ll) (uint8_t)(((INT128_INT16W4(ll))>>8) & 0xf)
#define INT128_INT16W4_SUBWORD1(ll) (uint8_t)(((INT128_INT16W4(ll))>>4) & 0xf)
#define INT128_INT16W4_SUBWORD0(ll) (uint8_t)(((INT128_INT16W4(ll))) & 0xf)
#define INT128_INT16W3_SUBWORD3(ll) (uint8_t)(((INT128_INT16W3(ll))>>12)& 0xf)
#define INT128_INT16W3_SUBWORD2(ll) (uint8_t)(((INT128_INT16W3(ll))>>8) & 0xf)
#define INT128_INT16W3_SUBWORD1(ll) (uint8_t)(((INT128_INT16W3(ll))>>4) & 0xf)
#define INT128_INT16W3_SUBWORD0(ll) (uint8_t)(((INT128_INT16W3(ll))) & 0xf)
#define INT128_INT16W2_SUBWORD3(ll) (uint8_t)(((INT128_INT16W2(ll))>>12)& 0xf)
#define INT128_INT16W2_SUBWORD2(ll) (uint8_t)(((INT128_INT16W2(ll))>>8) & 0xf)
#define INT128_INT16W2_SUBWORD1(ll) (uint8_t)(((INT128_INT16W2(ll))>>4) & 0xf)
#define INT128_INT16W2_SUBWORD0(ll) (uint8_t)(((INT128_INT16W2(ll))) & 0xf)
#define INT128_INT16W1_SUBWORD3(ll) (uint8_t)(((INT128_INT16W1(ll))>>12)& 0xf)
#define INT128_INT16W1_SUBWORD2(ll) (uint8_t)(((INT128_INT16W1(ll))>>8) & 0xf)
#define INT128_INT16W1_SUBWORD1(ll) (uint8_t)(((INT128_INT16W1(ll))>>4) & 0xf)
#define INT128_INT16W1_SUBWORD0(ll) (uint8_t)(((INT128_INT16W1(ll))) & 0xf)
#define INT128_INT16W0_SUBWORD3(ll) (uint8_t)(((INT128_INT16W0(ll))>>12)& 0xf)
#define INT128_INT16W0_SUBWORD2(ll) (uint8_t)(((INT128_INT16W0(ll))>>8) & 0xf)
#define INT128_INT16W0_SUBWORD1(ll) (uint8_t)(((INT128_INT16W0(ll))>>4) & 0xf)
#define INT128_INT16W0_SUBWORD0(ll) (uint8_t)(((INT128_INT16W0(ll))) & 0xf)

void usage(char *s)
{
  fprintf(stderr, "usage: gf_example_5\n");
  exit(1);
}

int main(int argc, char **argv)
{
  struct timeval start, stop;
  int       i, j, k;
  uint16_t  *a, *b, *bv, *b2, *z, *zv, *row, *src, *dst;
  uint16_t  temp16, mat16[16], g[10][10];
  uint16_t  a3_temp2, a3_temp3, a3_temp4, a3_temp5, a3_temp6;
  uint16_t  a2_var1, a2_var2, a2_var3, a2_var4, a2_var5;
  uint16_t  a2_var6, a2_var7, a2_var8, a2_var9, a2_var10;
  uint16_t  b_var2, b_var3, b_var4, b_var5, b_var6, b_var7, b_var8, b_var9;
  uint16_t  b2_var1, b2_var2, b2_var3, b2_var4, b2_var5;
  uint16_t  g22_var, *int16p;
  gf_t gf;

  if (gf_init_hard(&gf, 16, GF_MULT_BYTWO_p, GF_REGION_DEFAULT, GF_DIVIDE_DEFAULT, 
                   0, 0, 0, NULL, NULL) == 0) {
    fprintf(stderr, "gf_init_hard failed\n");
    exit(1);
  }

  // a = (uint16_t *) malloc(200);
  // b = (uint16_t *) malloc(200);
  i = posix_memalign((void **)&a, 0x10, DSIZE);
  i = posix_memalign((void **)&b, 0x10, DSIZE);
  i = posix_memalign((void **)&b2, 0x10, DSIZE);
  i = posix_memalign((void **)&bv, 0x10, DSIZE);
  i = posix_memalign((void **)&z, 0x10, DSIZE);
  i = posix_memalign((void **)&zv, 0x10, DSIZE);
  i = posix_memalign((void **)&row, 0x10, DSIZE);
  i = posix_memalign((void **)&src, 0x1000, SIMD_DSIZE * 2);
  i = posix_memalign((void **)&dst, 0x1000, SIMD_DSIZE * 2);

#if 0
  a += 6;
  b += 6;
#endif

  MOA_Seed(0);

  // initialize Generator Matrix Zinv
  a[0] = 0x0001;
  a[1] = 0x8000;
  a[2] = 0x4000;
  a[3] = 0x2000;
  a[4] = 0x1001;
  a[5] = 0x0800;
  a[6] = 0x0400;
  a[7] = 0x0200;
  a[8] = 0x0100;
  a[9] = 0x0080;
  a[10] = 0x0040;
  a[11] = 0x0020;
  a[12] = 0x0010;
  a[13] = 0x0009;
  a[14] = 0x0004;
  a[15] = 0x0003;

  // initialize Generator Matrix B
  b[0] = 0xb0b1;
  b[1] = 0x5858;
  b[2] = 0x2c2c;
  b[3] = 0x9616;
  b[4] = 0xfbba;
  b[5] = 0xfddd;
  b[6] = 0xfeee;
  b[7] = 0xff77;
  b[8] = 0xffbb;
  b[9] = 0x7fdd;
  b[10] = 0xbfee;
  b[11] = 0xdff7;
  b[12] = 0xeffb;
  b[13] = 0x474c;
  b[14] = 0xa3a6;
  b[15] = 0x6162;

  // B vertical/column matrix
  bv[0] = 0x9fba;
  bv[1] = 0x4fdd;
  bv[2] = 0xafeb;
  bv[3] = 0xdff0;
  bv[4] = 0x6ff8;
  bv[5] = 0x37fc;
  bv[6] = 0x1bfe;
  bv[7] = 0x0dff;
  bv[8] = 0x8efa;
  bv[9] = 0x477d;
  bv[10] = 0xabbb;
  bv[11] = 0xddd8;
  bv[12] = 0x6eec;
  bv[13] = 0x3776;
  bv[14] = 0x1bbb;
  bv[15] = 0x85d8;

  // B ^ 2 Matrix
  b2[0] = 0xf44b;
  b2[1] = 0x7a25;
  b2[2] = 0x3d12;
  b2[3] = 0x1e89;
  b2[4] = 0x7b0f;
  b2[5] = 0x3d87;
  b2[6] = 0x9ec3;
  b2[7] = 0xcf61;
  b2[8] = 0xe7b0;
  b2[9] = 0x73d8;
  b2[10] = 0x39ec;
  b2[11] = 0x1cf6;
  b2[12] = 0x0e7b;
  b2[13] = 0x7376;
  b2[14] = 0x39bb;
  b2[15] = 0xe896;

  // initialize Matrix Z
  z[0] = 0x4000;
  z[1] = 0x2000;
  z[2] = 0x1000;
  z[3] = 0x8800;
  z[4] = 0x0400;
  z[5] = 0x0200;
  z[6] = 0x0100;
  z[7] = 0x0080;
  z[8] = 0x0040;
  z[9] = 0x0020;
  z[10] = 0x0010;
  z[11] = 0x0008;
  z[12] = 0x8004;
  z[13] = 0x0002;
  z[14] = 0x8001;
  z[15] = 0x8000;

  // initailize Z Vectors
  zv[0] = 0x100b;
  zv[1] = 0x8000;
  zv[2] = 0x4000;
  zv[3] = 0x2000;
  zv[4] = 0x1000;
  zv[5] = 0x0800;
  zv[6] = 0x0400;
  zv[7] = 0x0200;
  zv[8] = 0x0100;
  zv[9] = 0x0080;
  zv[10] = 0x0040;
  zv[11] = 0x0020;
  zv[12] = 0x0010;
  zv[13] = 0x0008;
  zv[14] = 0x0004;
  zv[15] = 0x0002;

  // G Matrix
  g[0][0] = 0x52d8;
  g[1][0] = 0xa16c;
  g[2][0] = 0x9267;
  g[3][0] = 0xe44d;
  g[4][0] = 0x13f3;
  g[5][0] = 0xc178;
  g[6][0] = 0xb1ec;
  g[7][0] = 0x4b47;
  g[8][0] = 0x9919;
  g[9][0] = 0x1e99;
  g[0][1] = 0x3479;
  g[1][1] = 0x437c;
  g[2][1] = 0x6569;
  g[3][1] = 0x4b51;
  g[4][1] = 0xa4cf;
  g[5][1] = 0x4b96;
  g[6][1] = 0x2ed7;
  g[7][1] = 0x7287;
  g[8][1] = 0x1ac5;
  g[9][1] = 0xd60f;
  g[0][2] = 0xe60f;
  g[1][2] = 0x70ad;
  g[2][2] = 0xf1a6;
  g[3][2] = 0xeecc;
  g[4][2] = 0xf418;
  g[5][2] = 0xd1a6;
  g[6][2] = 0x5bb5;
  g[7][2] = 0xa871;
  g[8][2] = 0xa6bc;
  g[9][2] = 0xa0f8;
  g[0][3] = 0x70ae;
  g[1][3] = 0x5838;
  g[2][3] = 0xff51;
  g[3][3] = 0xeb6b;
  g[4][3] = 0xca0a;
  g[5][3] = 0x0399;
  g[6][3] = 0x918b;
  g[7][3] = 0xb2f8;
  g[8][3] = 0x9d03;
  g[9][3] = 0xe954;
  g[0][4] = 0xb2ad;
  g[1][4] = 0x1b6f;
  g[2][4] = 0x3b2e;
  g[3][4] = 0x9abd;
  g[4][4] = 0xf6d9;
  g[5][4] = 0x0409;
  g[6][4] = 0xe291;
  g[7][4] = 0x0986;
  g[8][4] = 0x3996;
  g[9][4] = 0x41ed;

  //==========================================================
  // Hand multiplication of 0x0001
  temp16 = l_mult_matrix_by_hand(a, 0x0001, 16);
  printf("Multi Matrix Zinv input=0x0001 prod16 is 0x%x\n", temp16);
  a2_var1 = 0x1;
  a2_var2 = temp16;

  // Hand multiplication of 0x8805
  temp16 = l_mult_matrix_by_hand(a, temp16, 16);
  printf("Multi Matrix Zinv input=0x8805 prod16 is 0x%x\n", temp16);
  a2_var3 = temp16;

  // Hand multiplication of 0xcc07
  temp16 = l_mult_matrix_by_hand(a, temp16, 16);
  printf("Multi Matrix Zinv input=0xcc07 prod16 is 0x%x\n", temp16);
  a2_var4 = temp16;

  // Hand multiplication of 0xee06
  temp16 = l_mult_matrix_by_hand(a, temp16, 16);
  printf("Multi Matrix Zinv input=0xee06 prod16 is 0x%x\n", temp16);
  a2_var5 = temp16;

  // Hand multiplication of 0x85d8
  temp16 = l_mult_matrix_by_hand(b, temp16, 16);
  printf("Multi Matrix B input=0x85d8 prod16 is 0x%x\n", temp16);

  // ===========================================================
  // A2 
  a3_temp2 = l_mult_matrix_by_hand(a, a2_var2, 16);
  a3_temp3 = l_mult_matrix_by_hand(a, a3_temp2, 16);
  a3_temp4 = l_mult_matrix_by_hand(a, a3_temp3, 16);
  printf("A2 - Multi Matrix Zinv input=x%x prod16 is 0x%x, 0x%x, 0x%x\n", 
     a2_var2, a3_temp2, a3_temp3, a3_temp4);

  // A3 
  a3_temp2 = l_mult_matrix_by_hand(a, a2_var3, 16);
  a3_temp3 = l_mult_matrix_by_hand(a, a3_temp2, 16);
  a3_temp4 = l_mult_matrix_by_hand(a, a3_temp3, 16);
  printf("A3 - Multi Matrix Zinv input=x%x prod16 is 0x%x, 0x%x, 0x%x\n", 
     a2_var3, a3_temp2, a3_temp3, a3_temp4);

  // A4 
  a3_temp2 = l_mult_matrix_by_hand(a, a2_var4, 16);
  a3_temp3 = l_mult_matrix_by_hand(a, a3_temp2, 16);
  a3_temp4 = l_mult_matrix_by_hand(a, a3_temp3, 16);
  printf("A4 - Multi Matrix Zinv input=x%x prod16 is 0x%x, 0x%x, 0x%x\n", 
     a2_var4, a3_temp2, a3_temp3, a3_temp4);

  //==========================================================
  // Hand multiplication of 0xdf0a
  temp16 = l_mult_matrix_by_hand(b, 0xdf0a, 16);
  printf("Multi Matrix B input=0xdf0a prod16 is 0x%x\n", temp16);
  b_var2 = 0xdf0a;
  b_var3 = temp16;

  // Hand multiplication of 0x34c5
  temp16 = l_mult_matrix_by_hand(b, 0x34c5, 16);
  printf("Multi Matrix B input=0x34c5 prod16 is 0x%x\n", temp16);
  b_var4 = temp16;

  // Hand multiplication of 0x3cce
  temp16 = l_mult_matrix_by_hand(b, 0x3cce, 16);
  printf("Multi Matrix B input=0x3cce prod16 is 0x%x\n", temp16);
  b_var5 = temp16;

  // Hand multiplication of 0xa3b9
  temp16 = l_mult_matrix_by_hand(b, 0xa3b9, 16);
  printf("Multi Matrix B input=0xa3b9 prod16 is 0x%x\n", temp16);
  b_var6 = temp16;

  // Hand multiplication of 0x35fd
  temp16 = l_mult_matrix_by_hand(b, 0x35fd, 16);
  printf("Multi Matrix B input=0x35fd prod16 is 0x%x\n", temp16);
  b_var7 = temp16;

  // Hand multiplication of 0x29be
  temp16 = l_mult_matrix_by_hand(b, 0x29be, 16);
  printf("Multi Matrix B input=0x29be prod16 is 0x%x\n", temp16);
  b_var8 = temp16;

  // Hand multiplication of 0x7754
  temp16 = l_mult_matrix_by_hand(b, 0x7754, 16);
  printf("Multi Matrix B input=0x7754 prod16 is 0x%x\n", temp16);
  b_var9 = temp16;

  // Hand multiplication of 0xb3e8
  temp16 = l_mult_matrix_by_hand(b, 0xb3e8, 16);
  printf("Multi Matrix B input=0xb3e8 prod16 is 0x%x\n", temp16);

  // Matrix B * Matrix B multiplication 
  // prints out columns of B ^ 2
  int16p = src;
  for (i=0; i<16; i++) {
     mat16[i] = l_mult_matrix_by_hand(b, bv[i], 16);
     printf("Multi Matrix B by B column %d) - is 0x%x\n", i, mat16[i]);
     *int16p++ = mat16[i]; 
  }

  // B2
  a3_temp3 = l_mult_matrix_by_hand(b2, b_var2, 16);
  a3_temp4 = l_mult_matrix_by_hand(b2, a3_temp3, 16);
  a3_temp5 = l_mult_matrix_by_hand(b2, a3_temp4, 16);
  a3_temp6 = l_mult_matrix_by_hand(b2, a3_temp5, 16);
  b2_var1 = b_var2;
  b2_var2 = a3_temp3;
  b2_var3 = a3_temp4;
  b2_var4 = a3_temp5;
  b2_var5 = a3_temp6;
  printf("B^2 2 - Multi Matrix Zinv input=x%x prod16 is 0x%x, 0x%x, 0x%x, 0x%x\n", 
     b_var2, a3_temp3, a3_temp4, a3_temp5, a3_temp6);

  // translate B^2 column output into rows
  l_matrix_translate_column(src, row, 16); 
  // Use B^2 row output to calculate B^3
  // Matrix B ^ 3 
  // prints out columns of B ^ 3
  printf("Use B^2 row output to calculate B^3: Multiply B^2 row * B\n");
  int16p = src;
  for (i=0; i<16; i++) {
     mat16[i] = l_mult_matrix_by_hand(row, bv[i], 16);
     printf(" Matrix B ^ 3  column %d) - is 0x%x\n", i, mat16[i]);
     *int16p++ = mat16[i]; 
  }
  l_matrix_translate_column(src, row, 16); 

  // B2
  a3_temp3 = l_mult_matrix_by_hand(b2, b_var2, 16);
  a3_temp4 = l_mult_matrix_by_hand(b2, a3_temp3, 16);
  a3_temp5 = l_mult_matrix_by_hand(b2, a3_temp4, 16);
  a3_temp6 = l_mult_matrix_by_hand(b2, a3_temp5, 16);
  b2_var1 = b_var2;
  b2_var2 = a3_temp3;
  b2_var3 = a3_temp4;
  b2_var4 = a3_temp5;
  b2_var5 = a3_temp6;
  printf("B^2 2 - Multi Matrix Zinv input=x%x prod16 is 0x%x, 0x%x, 0x%x, 0x%x\n", 
     b_var2, a3_temp3, a3_temp4, a3_temp5, a3_temp6);

  // B3
  a3_temp3 = l_mult_matrix_by_hand(row, b_var3, 16);
  a3_temp4 = l_mult_matrix_by_hand(row, a3_temp3, 16);
  a3_temp5 = l_mult_matrix_by_hand(row, a3_temp4, 16);
  printf("B^3 3 - Multi Matrix Zinv input=x%x prod16 is 0x%x, 0x%x, 0x%x\n", 
     b_var3, a3_temp3, a3_temp4, a3_temp5);

#if 0
  // B4
  a3_temp3 = l_mult_matrix_by_hand(b2, b_var4);
  a3_temp4 = l_mult_matrix_by_hand(b2, a3_temp3);
  a3_temp5 = l_mult_matrix_by_hand(b2, a3_temp4);
  printf("B^2 4 - Multi Matrix Zinv input=x%x prod16 is 0x%x, 0x%x, 0x%x\n", 
     b_var4, a3_temp3, a3_temp4, a3_temp5);
#endif


#if 0 // doesn't work with XOR and AND, need GF(2^16)
  // Calculate G22 generator
  g22_var = (a2_var1 & b2_var1) ^ (a2_var2 & b2_var2) ^ (a2_var3 & b2_var3) ^
            (a2_var4 & b2_var4) ^ (a2_var5 & b2_var5); 
  printf(" A2 var's are 0x%x 0x%x 0x%x 0x%x 0x%x\n", a2_var1, a2_var2, a2_var3,
           a2_var4, a2_var5);
  printf(" B2 var's are 0x%x 0x%x 0x%x 0x%x 0x%x\n", b2_var1, b2_var2, b2_var3,
           b2_var4, b2_var5);
  printf("G22 generator is 0x%x\n", g22_var);
#endif
 
  // set up 128bits of 8 x 16bit words, then multiply by a const 
  g22_var = 0x89ad;
  int16p = src;
  printf("Source Matrix\n");
  for (i=0; i<SIMD_DSIZE/sizeof(uint16_t); i++) {
    *int16p = 0x4;
#ifdef DDEBUG
    printf(" 0x%x ", *int16p); 
    if ((i+1)%8 == 0)
      printf("\n 0x%x: %d:", int16p, i);
#endif
    int16p++;
  }
  l_perf_start(&start);
  for (i=0; i<MULTI_ITER; i++) {
    l_gf16_multiply_region_from_2(src, dst, SIMD_DSIZE, 0x89ad, 0x1100b);
    //l_gf_w16_split_4_16_lazy_sse_altmap_multiply_region(src, dst, SIMD_DSIZE, 0x89ad, 0, 0x1100b);
    int16p = dst;
#ifdef DDEBUG
    for (j=0; j<SIMD_DSIZE/sizeof(uint16_t); j++) {
      if (*int16p != 0x6a2) {
        printf(" mismatch for %d.%d: 0x%x != 0x6a2\n", i, j, *int16p); 
      }
      int16p++;
    }
#endif
  }
  l_perf_stop(&stop);
  printf("GF(2^16) mult of region by 0x89ad, result is:\n");
  l_perf_print(&stop, &start, MULTI_ITER*SIMD_DSIZE);
#ifdef DDEBUG
  int16p = dst;
  for (i=0; i<SIMD_DSIZE/sizeof(uint16_t); i++) {
    printf(" 0x%x ", *int16p++); 
    if ((i+1)%8 == 0)
      printf("\n 0x%x: %d:", int16p, i);
  }
  printf("\n");
#endif

  // set up 128bits of 8 x 16bit words, then multiply by a const 
  g22_var = 0x89ad;
  int16p = src;
  printf("Source Matrix\n");
  for (i=0; i<SIMD_DSIZE/sizeof(uint16_t); i++) {
    *int16p = 0x4;
#ifdef DDEBUG
    printf(" 0x%x ", *int16p); 
    if ((i+1)%8 == 0)
      printf("\n 0x%x: %d:", int16p, i);
#endif
    int16p++;
  }
  l_perf_start(&start);
  for (i=0; i<MULTI_ITER; i++) {
    //l_gf16_multiply_region_from_2(src, dst, SIMD_DSIZE, 0x89ad, 0x1100b);
    l_gf_w16_split_4_16_lazy_sse_altmap_multiply_region(src, dst, SIMD_DSIZE, 0x89ad, 0, 0x1100b);
    int16p = dst;
    for (j=0; j<SIMD_DSIZE/sizeof(uint16_t); j++) {
#ifdef DDEBUG
      if (*int16p != 0x6a2) {
        printf(" mismatch for %d.%d: 0x%x != 0x6a2\n", i, j, *int16p); 
      }
#endif
      int16p++;
    }
  }
  l_perf_stop(&stop);
  printf("GF(2^16) ALTMAP multiply region by 0x89ad, result is:\n");
  l_perf_print(&stop, &start, MULTI_ITER*SIMD_DSIZE);
#ifdef DDEBUG
  int16p = dst;
  for (i=0; i<SIMD_DSIZE/sizeof(uint16_t); i++) {
    printf(" 0x%x ", *int16p++); 
    if ((i+1)%8 == 0)
      printf("\n 0x%x: %d:", int16p, i);
  }
  printf("\n");
#endif

  int16p = src;
  for (i=0; i<10; i++)
    *int16p++ = 0x2;
  for (i=10; i<100; i++)
    *int16p++ = 0x0;
  a2_var1 = l_shift_multiply(g[0][0], 0x2);
  a2_var2 = l_shift_multiply(g[1][0], 0x4);
  a2_var3 = l_shift_multiply(g[2][0], 0x8);
  a2_var4 = l_shift_multiply(g[3][0], 0x10);
  a2_var5 = l_shift_multiply(g[4][0], 0x20);
  a2_var6 = l_shift_multiply(g[5][0], 0x40);
  a2_var7 = l_shift_multiply(g[6][0], 0x80);
  a2_var8 = l_shift_multiply(g[7][0], 0x100);
  a2_var9 = l_shift_multiply(g[8][0], 0x200);
  a2_var10 = l_shift_multiply(g[9][0], 0x400);
  a3_temp5 = a2_var1 ^ a2_var2 ^ a2_var3 ^ a2_var4 ^ a2_var5 ^ a2_var6 ^ a2_var7 ^ a2_var8 ^ a2_var9 ^ a2_var10;
  printf("Partial Data Indics 0 = x%x\n", a3_temp5); 
  
  a2_var1 = l_shift_multiply(g[0][1], 0x2);
  a2_var2 = l_shift_multiply(g[1][1], 0x4);
  a2_var3 = l_shift_multiply(g[2][1], 0x8);
  a2_var4 = l_shift_multiply(g[3][1], 0x10);
  a2_var5 = l_shift_multiply(g[4][1], 0x20);
  a2_var6 = l_shift_multiply(g[5][1], 0x40);
  a2_var7 = l_shift_multiply(g[6][1], 0x80);
  a2_var8 = l_shift_multiply(g[7][1], 0x100);
  a2_var9 = l_shift_multiply(g[8][1], 0x200);
  a2_var10 = l_shift_multiply(g[9][1], 0x400);
  a3_temp5 = a2_var1 ^ a2_var2 ^ a2_var3 ^ a2_var4 ^ a2_var5 ^ a2_var6 ^ a2_var7 ^ a2_var8 ^ a2_var9 ^ a2_var10;
  printf("Partial Data Indics 1 = x%x\n", a3_temp5); 

  a2_var1 = l_shift_multiply(g[0][2], 0x2);
  a2_var2 = l_shift_multiply(g[1][2], 0x4);
  a2_var3 = l_shift_multiply(g[2][2], 0x8);
  a2_var4 = l_shift_multiply(g[3][2], 0x10);
  a2_var5 = l_shift_multiply(g[4][2], 0x20);
  a2_var6 = l_shift_multiply(g[5][2], 0x40);
  a2_var7 = l_shift_multiply(g[6][2], 0x80);
  a2_var8 = l_shift_multiply(g[7][2], 0x100);
  a2_var9 = l_shift_multiply(g[8][2], 0x200);
  a2_var10 = l_shift_multiply(g[9][2], 0x400);
  a3_temp5 = a2_var1 ^ a2_var2 ^ a2_var3 ^ a2_var4 ^ a2_var5 ^ a2_var6 ^ a2_var7 ^ a2_var8 ^ a2_var9 ^ a2_var10;
  printf("Partial Data Indics 2 = x%x\n", a3_temp5); 

  a2_var1 = l_shift_multiply(g[0][3], 0x2);
  a2_var2 = l_shift_multiply(g[1][3], 0x4);
  a2_var3 = l_shift_multiply(g[2][3], 0x8);
  a2_var4 = l_shift_multiply(g[3][3], 0x10);
  a2_var5 = l_shift_multiply(g[4][3], 0x20);
  a2_var6 = l_shift_multiply(g[5][3], 0x40);
  a2_var7 = l_shift_multiply(g[6][3], 0x80);
  a2_var8 = l_shift_multiply(g[7][3], 0x100);
  a2_var9 = l_shift_multiply(g[8][3], 0x200);
  a2_var10 = l_shift_multiply(g[9][3], 0x400);
  a3_temp5 = a2_var1 ^ a2_var2 ^ a2_var3 ^ a2_var4 ^ a2_var5 ^ a2_var6 ^ a2_var7 ^ a2_var8 ^ a2_var9 ^ a2_var10;
  printf("Partial Data Indics 3 = x%x\n", a3_temp5); 

  a2_var1 = l_shift_multiply(g[0][4], 0x2);
  a2_var2 = l_shift_multiply(g[1][4], 0x4);
  a2_var3 = l_shift_multiply(g[2][4], 0x8);
  a2_var4 = l_shift_multiply(g[3][4], 0x10);
  a2_var5 = l_shift_multiply(g[4][4], 0x20);
  a2_var6 = l_shift_multiply(g[5][4], 0x40);
  a2_var7 = l_shift_multiply(g[6][4], 0x80);
  a2_var8 = l_shift_multiply(g[7][4], 0x100);
  a2_var9 = l_shift_multiply(g[8][4], 0x200);
  a2_var10 = l_shift_multiply(g[9][4], 0x400);
  a3_temp5 = a2_var1 ^ a2_var2 ^ a2_var3 ^ a2_var4 ^ a2_var5 ^ a2_var6 ^ a2_var7 ^ a2_var8 ^ a2_var9 ^ a2_var10;
  printf("Partial Data Indics 4 = x%x\n", a3_temp5); 

  l_perf_start(&start);
  for (i=0 ;i<MATRIX_ITER; i++) {
     // Hand multiplication of 0x8805
     temp16 = l_mult_matrix_by_hand(a, temp16, 16);
  }
  l_perf_stop(&stop);
  printf("Matrix Multiplication done %d times\n", MATRIX_ITER);
  l_perf_print2(&stop, &start, MATRIX_ITER);

  l_perf_start(&start);
  for (i=0; i<MATRIX_ITER; i++) {
      a2_var1 = l_shift_multiply(g[0][0], 0x2);
      a2_var2 = l_shift_multiply(g[1][0], 0x4);
      a2_var3 = l_shift_multiply(g[2][0], 0x8);
      a2_var4 = l_shift_multiply(g[3][0], 0x10);
      a2_var5 = l_shift_multiply(g[4][0], 0x20);
      a2_var6 = l_shift_multiply(g[5][0], 0x40);
      a2_var7 = l_shift_multiply(g[6][0], 0x80);
      a2_var8 = l_shift_multiply(g[7][0], 0x100);
      a2_var9 = l_shift_multiply(g[8][0], 0x200);
      a2_var10 = l_shift_multiply(g[9][0], 0x400);
      a2_var1 = l_shift_multiply(g[0][0], 0x2);
      a2_var2 = l_shift_multiply(g[1][0], 0x4);
      a2_var3 = l_shift_multiply(g[2][0], 0x8);
      a2_var4 = l_shift_multiply(g[3][0], 0x10);
      a2_var5 = l_shift_multiply(g[4][0], 0x20);
      a2_var6 = l_shift_multiply(g[5][0], 0x40);
  }
  l_perf_stop(&stop);
  printf("16 Shift Multiplication done %d times\n", MATRIX_ITER);
  l_perf_print2(&stop, &start, MATRIX_ITER);
  l_perf_print(&stop, &start, MATRIX_ITER * 16 * 2);
  // Matrix * Matrix multiplication 
//  for (j=0; j<24; j++) {
//  for (i=0; i<16; i++) {
//     mat16[i] = l_mult_matrix_by_hand(z, zv[i]);
//     printf("Multi Z ^ %d, column %d) - Z * Z vector is 0x%x\n", j + 2, i, mat16[i]);
//       zv[i] = mat16[i];
//  }
//  }
#if 0
  temp16 = l_mult_matrix_vector_i128(a, 0x0001); 
  printf("mult of 0x0001 returned 0x%4.4x\n", temp16);
  temp16 = l_mult_matrix_vector_i128(a, 0x8805);
  printf("mult of 0x8805 returned 0x%4.4x\n", temp16);
  temp16 = l_mult_matrix_vector_i128(a, 0xcc07);
  printf("mult of 0xcc07 returned 0x%4.4x\n", temp16);
  temp16 = l_mult_matrix_vector_i128(a, 0xee06);
  printf("mult of 0xee06 returned 0x%4.4x\n", temp16);
#endif

#if 0
  //_mm_storeu_si128(ret_array, mm128_prod2);
  //int1 = *(uint32_t *)&ret_array[0];
  //int2 = *(uint32_t *)&ret_array[4];
  //int3 = *(uint32_t *)&ret_array[8];
  //int4 = *(uint32_t *)&ret_array[12];
  //printf(" prod2=  %8.8x %8.8x %8.8x %8.8x\n", int4, int3, int2, int1);
  //printf("   %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n",
  //      ret_array[7], ret_array[6], ret_array[5], ret_array[4], ret_array[3],
  //      ret_array[2], ret_array[1], ret_array[0]);
  //printf("   %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n",
  //      ret_array[15], ret_array[14], ret_array[13], ret_array[12], ret_array[11],
  //      ret_array[10], ret_array[9], ret_array[8]);


  for (i = 0; i < 15; i ++) {
    printf("Word %2d: 0x%04x * XXX = 0x%04x    ", i,
           gf.extract_word.w32(&gf, a, 30*2, i),
           gf.extract_word.w32(&gf, b, 30*2, i));
    printf("Word %2d: 0x%04x * XXX = 0x%04x\n", i+15,
           gf.extract_word.w32(&gf, a, 30*2, i+15),
           gf.extract_word.w32(&gf, b, 30*2, i+15));
  }
#endif

  return 0;
}

/* multiplication table for 0x17 in GF(16) */
#if 0
static uint8_t    gfx17_multi_table[16] = 
{0x00, 0x17, 0x2e, 0x39, 0x5c, 0x4b, 0x72, 0x65,
 0xb8, 0xaf, 0x96, 0x81, 0xe4, 0xf3, 0xca, 0xdd};
#endif

int
split128_into_8_by_16(void)
{
    __m128i   gfx17_multi_table;
    l_int128_t tst1, *tst1_p;
    l_int128_t work_array[36];
    uint64_t   var1, var2, var3, var4;
    uint64_t   var5, var6, var7, var8;
    uint8_t    *int8_p;
    uint8_t    ret_array[16];
    __m128i    si, so, res128;

    gfx17_multi_table = _mm_setr_epi8(
                          0x00, 0x17, 0x2e, 0x39, 0x5c, 0x4b, 0x72, 0x65,
                          0xb8, 0xaf, 0x96, 0x81, 0xe4, 0xf3, 0xca, 0xdd);
    tst1.high = 0x1234234534564567;
    tst1.low = 0x5555666677778888;
    tst1_p = &tst1;
    var1 = 0xfa;
    si  = _mm_load_si128((__m128i *)&var1);
    so  = _mm_load_si128((__m128i *)&var1);
    int8_p = (uint8_t *)&work_array[0];
    printf("Data 128b Word is 0x%16.16"PRIx64".0x%16.16"PRIx64"\n",
        tst1.high, tst1.low);

    bzero(work_array, sizeof(work_array));
    var1 =  INT128_INT16W7_SUBWORD0((l_int128_t *)tst1_p);
    printf("print subword1 of Word 7 is 0x%"PRIx64"\n", var1); 
    var1 =  INT128_INT16W6_SUBWORD0((l_int128_t *)tst1_p);
    printf("print subword1 of Word 6 is 0x%"PRIx64"x\n", var1); 
    var1 =  INT128_INT16W3_SUBWORD0((l_int128_t *)tst1_p);
    printf("print subword1 of Word 3 is 0x%"PRIx64"x\n", var1); 

    var1 = INT128_INT16W7_SUBWORD0((l_int128_t *)tst1_p); 
    var2 = INT128_INT16W6_SUBWORD0((l_int128_t *)tst1_p);
    var3 = INT128_INT16W5_SUBWORD0((l_int128_t *)tst1_p);
    var4 = INT128_INT16W4_SUBWORD0((l_int128_t *)tst1_p);
    var5 = INT128_INT16W3_SUBWORD0((l_int128_t *)tst1_p);
    var6 = INT128_INT16W2_SUBWORD0((l_int128_t *)tst1_p);
    var7 = INT128_INT16W1_SUBWORD0((l_int128_t *)tst1_p);
    var8 = INT128_INT16W0_SUBWORD0((l_int128_t *)tst1_p);
    work_array[0].high = var1 << 48 | var2 << 32 | var3 << 16 | var4; 
    work_array[0].low  = var5 << 48 | var6 << 32 | var7 << 16 | var8;
    printf("Word with subword0 is 0x%16.16"PRIx64".0x%16.16"PRIx64"\n",
        work_array[0].high, work_array[0].low);
    work_array[4].high = var1 << 56 | var2 << 40 | var3 << 24 | var4 << 8; 
    work_array[4].low  = var5 << 56 | var6 << 40 | var7 << 24 | var8 << 8;
    printf("Word with subword4 is 0x%16.16"PRIx64".0x%16.16"PRIx64"\n",
        work_array[4].high, work_array[4].low);
    si = _mm_setr_epi8(0, var1, 0, var2, 0, var3, 0, var4,
                       0, var5, 0, var6, 0, var7, 0, var8);

    var1 = INT128_INT16W7_SUBWORD1((l_int128_t *)tst1_p); 
    var2 = INT128_INT16W6_SUBWORD1((l_int128_t *)tst1_p);
    var3 = INT128_INT16W5_SUBWORD1((l_int128_t *)tst1_p);
    var4 = INT128_INT16W4_SUBWORD1((l_int128_t *)tst1_p);
    var5 = INT128_INT16W3_SUBWORD1((l_int128_t *)tst1_p);
    var6 = INT128_INT16W2_SUBWORD1((l_int128_t *)tst1_p);
    var7 = INT128_INT16W1_SUBWORD1((l_int128_t *)tst1_p);
    var8 = INT128_INT16W0_SUBWORD1((l_int128_t *)tst1_p);
    work_array[1].high = var1 << 48 | var2 << 32 | var3 << 16 | var4; 
    work_array[1].low  = var5 << 48 | var6 << 32 | var7 << 16 | var8;
    printf("Word with subword1 is 0x%16.16"PRIx64",0x%16.16"PRIx64"\n",
        work_array[1].high, work_array[1].low);
    work_array[5].high = var1 << 56 | var2 << 40 | var3 << 24 | var4 << 8; 
    work_array[5].low  = var5 << 56 | var6 << 40 | var7 << 24 | var8 << 8;
    printf("Word with subword5 is 0x%16.16"PRIx64".0x%16.16"PRIx64"\n",
        work_array[5].high, work_array[5].low);

    var1 = INT128_INT16W7_SUBWORD2((l_int128_t *)tst1_p); 
    var2 = INT128_INT16W6_SUBWORD2((l_int128_t *)tst1_p);
    var3 = INT128_INT16W5_SUBWORD2((l_int128_t *)tst1_p);
    var4 = INT128_INT16W4_SUBWORD2((l_int128_t *)tst1_p);
    var5 = INT128_INT16W3_SUBWORD2((l_int128_t *)tst1_p);
    var6 = INT128_INT16W2_SUBWORD2((l_int128_t *)tst1_p);
    var7 = INT128_INT16W1_SUBWORD2((l_int128_t *)tst1_p);
    var8 = INT128_INT16W0_SUBWORD2((l_int128_t *)tst1_p);
    work_array[2].high = var1 << 48 | var2 << 32 | var3 << 16 | var4; 
    work_array[2].low  = var5 << 48 | var6 << 32 | var7 << 16 | var8;
    printf("Word with subword2 is 0x%16.16"PRIx64".0x%16.16"PRIx64"\n",
        work_array[2].high, work_array[2].low);
    work_array[6].high = var1 << 56 | var2 << 40 | var3 << 24 | var4 << 8; 
    work_array[6].low  = var5 << 56 | var6 << 40 | var7 << 24 | var8 << 8;
    printf("Word with subword6 is 0x%16.16"PRIx64".0x%16.16"PRIx64"\n",
        work_array[6].high, work_array[6].low);

    var1 = INT128_INT16W7_SUBWORD3((l_int128_t *)tst1_p); 
    var2 = INT128_INT16W6_SUBWORD3((l_int128_t *)tst1_p);
    var3 = INT128_INT16W5_SUBWORD3((l_int128_t *)tst1_p);
    var4 = INT128_INT16W4_SUBWORD3((l_int128_t *)tst1_p);
    var5 = INT128_INT16W3_SUBWORD3((l_int128_t *)tst1_p);
    var6 = INT128_INT16W2_SUBWORD3((l_int128_t *)tst1_p);
    var7 = INT128_INT16W1_SUBWORD3((l_int128_t *)tst1_p);
    var8 = INT128_INT16W0_SUBWORD3((l_int128_t *)tst1_p);
    work_array[3].high = var1 << 48 | var2 << 32 | var3 << 16 | var4; 
    work_array[3].low  = var5 << 48 | var6 << 32 | var7 << 16 | var8;
    printf("Word with subword3 is 0x%16.16"PRIx64".0x%16.16"PRIx64"\n",
        work_array[3].high, work_array[3].low);
    work_array[7].high = var1 << 56 | var2 << 40 | var3 << 24 | var4 << 8; 
    work_array[7].low  = var5 << 56 | var6 << 40 | var7 << 24 | var8 << 8;
    printf("Word with subword7 is 0x%16.16"PRIx64".0x%16.16"PRIx64"\n",
        work_array[7].high, work_array[7].low);

    printf("Data for shuffle\n");
   _mm_storeu_si128((__m128i *)ret_array, gfx17_multi_table);
    printf("   %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n",
        ret_array[0], ret_array[1], ret_array[2], ret_array[3], ret_array[4],
        ret_array[5], ret_array[6], ret_array[7]);
    printf("   %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n",
        ret_array[8], ret_array[9], ret_array[10], ret_array[11], ret_array[12],
        ret_array[13], ret_array[14], ret_array[15]);
    printf("Index Entries\n");
   _mm_storeu_si128((__m128i *)ret_array, si);
    printf("   %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n",
        ret_array[0], ret_array[1], ret_array[2], ret_array[3], ret_array[4],
        ret_array[5], ret_array[6], ret_array[7]);
    printf("   %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n",
        ret_array[8], ret_array[9], ret_array[10], ret_array[11], ret_array[12],
        ret_array[13], ret_array[14], ret_array[15]);
    res128 = _mm_shuffle_epi8((__m128i )gfx17_multi_table, (__m128i )si);
    printf("After shuffle: \n");
   _mm_storeu_si128((__m128i *)ret_array, res128);
    printf("   %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n",
        ret_array[0], ret_array[1], ret_array[2], ret_array[3], ret_array[4],
        ret_array[5], ret_array[6], ret_array[7]);
    printf("   %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n",
        ret_array[8], ret_array[9], ret_array[10], ret_array[11], ret_array[12],
        ret_array[13], ret_array[14], ret_array[15]);
    

    return 0;
}

/* based on gf_w16_clm_multiply_4 */
gf_val_32_t
l_gf16_multiply(uint32_t a16, uint32_t b16, uint32_t i_prim_poly)
{
    gf_val_32_t  rv = 0;
    __m128i      a, b;
    __m128i      result;
    __m128i      prim_poly;
    __m128i      w;

    a = _mm_insert_epi32(_mm_setzero_si128(), a16, 0); 
    b = _mm_insert_epi32(a, b16, 0);

    prim_poly = _mm_set_epi32(0, 0, 0, (i_prim_poly));

    /* start initial mulitply */
    result = _mm_clmulepi64_si128(a, b, 0);

    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    // printf("5th multiply result is \n");
    // l_print_i128(result);

    /* extract 32b value to return */
    rv = ((gf_val_32_t)_mm_extract_epi32(result, 0));
    //printf("returning 32b of x%x\n", rv);
    return rv;
    
}

/* based on gf_w16_split_4_16_lazy_sse_multiply_rewion */
int
l_gf16_multiply_region_from_2(void *src, void *dst, int size, uint32_t multiplier, uint32_t i_prim_poly)
{
    uint64_t i, j, c,  prod;
    uint64_t *s64, *d64;
    uint8_t  low[4][16];
    uint8_t  high[4][16];
    int      ii;
    __m128i  tlow[4], thigh[4], mask, lmask;
    __m128i  ta, tb, ti, tta, ttb, tpl, tph; 

    s64 = (uint64_t *)src;
    d64 = (uint64_t *)dst;
    for (j=0; j < 16; j++) {
        for (i=0; i < 4; i++) {
            c = (j << (i*4));
            prod = l_gf16_multiply(c, multiplier, i_prim_poly);
            low[i][j] = (prod & 0xff);
            high[i][j] = (prod >> 8);
        }
    }
    for (i=0;  i < 4; i++) {
        tlow[i] = _mm_loadu_si128((__m128i *)low[i]);
        thigh[i] = _mm_loadu_si128((__m128i *)high[i]);
    }
    mask = _mm_set1_epi8(0x0f);
    lmask = _mm_set1_epi16(0xff);

    //printf("%s: src x%p dst x%p size %d\n", __func__, src, dst, size);
    for (ii=0; ii < size; ii+=32) {

      ta = _mm_load_si128((__m128i *) s64);
      tb = _mm_load_si128((__m128i *) (s64+2));

      tta = _mm_srli_epi16(ta, 8);
      ttb = _mm_srli_epi16(tb, 8);
      tpl = _mm_and_si128(tb, lmask);
      tph = _mm_and_si128(ta, lmask);

      tb = _mm_packus_epi16(tpl, tph);
      ta = _mm_packus_epi16(ttb, tta);

      ti = _mm_and_si128 (mask, tb);
      tph = _mm_shuffle_epi8 (thigh[0], ti);
      tpl = _mm_shuffle_epi8 (tlow[0], ti);

      tb = _mm_srli_epi16(tb, 4);
      ti = _mm_and_si128 (mask, tb);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[1], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[1], ti), tph);

      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[2], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[2], ti), tph);

      ta = _mm_srli_epi16(ta, 4);
      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[3], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[3], ti), tph);

      ta = _mm_unpackhi_epi8(tpl, tph);
      tb = _mm_unpacklo_epi8(tpl, tph);

      _mm_store_si128 ((__m128i *)d64, ta);
      _mm_store_si128 ((__m128i *)(d64+2), tb);

      //printf("i=%d (%d), ", ii, ((uint64_t)d64-(uint64_t)dst));
      //l_print_i128(&ta);
      //l_print_i128((__m128i *)d64);
      //printf("\t");
      //l_print_i128(&tb);
      //l_print_i128((__m128i *)(d64+2));
      d64 += 4;
      s64 += 4;
      // ii  += 32;
    }
    return 0;
}

void
l_print_i128(__m128i *in1)
{
    uint32_t  *i32_p = (uint32_t *)in1;

    printf("i128:x%p::=x%x x%x x%x x%x\n",
       in1, i32_p[0], i32_p[1], i32_p[2], i32_p[3]);
}

uint16_t
l_mult_matrix_vector_i128(uint16_t *in_array, uint16_t mult1)
{
  __m128i   mm128_var1, mm128_var2, mm128_mult, mm128_prod1, mm128_prod2;
  uint32_t  prod16, cnt, bitcnt;
  uint32_t  int1, int2, int3, int4;
  uint16_t  *a, ret_array16[8];
  uint8_t   ret_array[16];
  int i;

  // start of matrix calculation. start with 0x0001
  a = in_array;
  printf("start matrix multiplication of 0x%4.4x\n", mult1);
  mm128_var1 = _mm_set_epi16(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
  mm128_var2 = _mm_set_epi16(a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]);
  mm128_mult = _mm_set1_epi16(mult1);
  _mm_storeu_si128((__m128i *)ret_array, mm128_var1);
  int1 = *(uint32_t *)&ret_array[0];
  int2 = *(uint32_t *)&ret_array[4];
  int3 = *(uint32_t *)&ret_array[8];
  int4 = *(uint32_t *)&ret_array[12];
  printf(" var1 =  %8.8x %8.8x %8.8x %8.8x\n", int4, int3, int2, int1);
  _mm_storeu_si128((__m128i *)ret_array, mm128_var2);
  int1 = *(uint32_t *)&ret_array[0];
  int2 = *(uint32_t *)&ret_array[4];
  int3 = *(uint32_t *)&ret_array[8];
  int4 = *(uint32_t *)&ret_array[12];
  printf(" var2 =  %8.8x %8.8x %8.8x %8.8x\n", int4, int3, int2, int1);
  _mm_storeu_si128((__m128i *)ret_array, mm128_mult);
  int1 = *(uint32_t *)&ret_array[0];
  int2 = *(uint32_t *)&ret_array[4];
  int3 = *(uint32_t *)&ret_array[8];
  int4 = *(uint32_t *)&ret_array[12];
  printf(" mult =  %8.8x %8.8x %8.8x %8.8x\n", int4, int3, int2, int1);
  mm128_prod1 = _mm_and_si128(mm128_var1, mm128_mult);
  mm128_prod2 = _mm_and_si128(mm128_var2, mm128_mult);
  _mm_storeu_si128((__m128i *)ret_array16, mm128_prod1);
  printf(" prod1= %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x\n",
     ret_array16[0], ret_array16[1], ret_array16[2], ret_array16[3],
     ret_array16[4], ret_array16[5], ret_array16[6], ret_array16[7]);
  bitcnt = 15;
  prod16 = 0;
  for (i=7; i>=0; i--) {
      cnt = __builtin_popcountll(ret_array16[i]); 
      if (cnt % 2)
         prod16 |= 1 << bitcnt;
      bitcnt--;
  }
  _mm_storeu_si128((__m128i *)ret_array16, mm128_prod2);
  for (i=7; i>=0; i--) {
      cnt = __builtin_popcountll(ret_array16[i]); 
      if (cnt % 2)
         prod16 |= 1 << bitcnt;
      bitcnt--;
  }
  printf(" prod2= %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x\n",
     ret_array16[0], ret_array16[1], ret_array16[2], ret_array16[3],
     ret_array16[4], ret_array16[5], ret_array16[6], ret_array16[7]);
  //printf(" bitprod=0x%x\n", prod16);
  return prod16;
}

uint16_t
l_mult_matrix_by_hand(uint16_t *in_array, uint16_t mult1, int size)
{
  int       i, cnt;
  uint16_t  prod16, temp16;

  prod16 = 0;
  temp16 = 0;
  for (i=0; i<size; i++) {
      temp16 = in_array[i] & mult1; 
      cnt = __builtin_popcountll(temp16); 
      //printf("%s: 0x%x & 0x%x temp16 is 0x%x cnt is %d\n", __func__, 
      //    in_array[i], mult1, temp16, cnt);
      if (cnt % 2)
         prod16 |= 1 << (15-i);
  }
  return prod16;
}
/* copied from gf_wgen_shift_multiply() */
int
l_shift_multiply(uint32_t a32, uint32_t b32)
{
  uint64_t old_prod, product, i, pp, a, b, one;
  int      w;

  a = a32;
  b = b32;
  one = 1;
  w = 16;
  //pp = 013; /* for 3 */
  pp = 0x1100b; /* for 16 */

  product = 0;

  for (i = 0; i < w; i++) {
    if (a & (one << i)) product ^= (b << i);
    //printf("%s: loop 1. i=%d, XOR TRUE=%d w=0x%x product=0x%x\n", __func__, i, (a & (one << i)), (b<<i), product);
  }
  for (i = w*2-1; i >= w; i--) {
    old_prod = product;
    if (product & (one << i)) product ^= (pp << (i-w));
    //printf("%s: loop 2. i=%d, XOR TRUE=%d w=0x%x product=0x%x\n", __func__, i, (old_prod & (one << i)), (pp << (i-w)), product);
  }
  //printf("%s: x%x * x%x return product=0x%x\n", __func__, a32, b32, product);
  return product;
}

/* convert a Matrix column data to a row data */
/* assume these are 16b values */
/* column and row data are arrays of 16b column data */
int
l_matrix_translate_column(void *column_data, void *dst_row, int items)
{
  uint16_t *col_p, *row_p, bit_temp, row_temp;
  int      i, j;

  col_p = (uint16_t *)column_data;
  row_p = (uint16_t *)dst_row;
  for (i=0; i<items; i++) {
    bit_temp = 1 << (15 - i); 
    row_temp = 0;
    for (j=0; j<items; j++) {
      if (col_p[j] & bit_temp) 
        row_temp |= 1 << (15 - j); 
    }
    row_p[i] = row_temp;
    printf("%s: row %d, column data 0x%x got data 0x%x\n", __func__, i, col_p[i], row_p[i]);
  }
  return 0;
}

/* based on Intel ISAL */
inline
int l_perf_start(struct timeval *tv)
{
        return gettimeofday(tv, 0);
}
inline
int l_perf_stop(struct timeval *tv)
{
        return gettimeofday(tv, 0);
}

inline
void l_perf_print(struct timeval *stop, struct timeval *start, long long dsize)
{
        long long secs = stop->tv_sec - start->tv_sec;
        long long usecs = secs * 1000000 + stop->tv_usec - start->tv_usec;

        printf("runtime = %10lld usecs", usecs);
        if (dsize != 0) {
#if 1 // not bug in printf for 32-bit
                printf(", bandwidth %lld MB in %.4f sec = %.2f MB/s\n", dsize/(1024*1024),
                        ((double) usecs)/1000000, ((double) dsize) / (double)usecs);
#else
                printf(", bandwidth %lld MB ", dsize/(1024*1024));
                printf("in %.4f sec ",(double)usecs/1000000);
                printf("= %.2f MB/s\n", (double)dsize/usecs);
#endif
        }
        else
                printf("\n");
}

inline
void l_perf_print2(struct timeval *stop, struct timeval *start, long long cnt)
{
        long long secs = stop->tv_sec - start->tv_sec;
        long long usecs = secs * 1000000 + stop->tv_usec - start->tv_usec;

        printf("runtime = %10lld usecs", usecs);
        if (cnt != 0) {
#if 1 // not bug in printf for 32-bit
                printf(", count %lld in %.4f sec = %.2f ns/count\n", cnt, 
                        ((double) usecs)/1000000, ((double) usecs * 1000.0) / (double)cnt);
#else
                printf(", bandwidth %lld MB ", dsize/(1024*1024));
                printf("in %.4f sec ",(double)usecs/1000000);
                printf("= %.2f MB/s\n", (double)dsize/usecs);
#endif
        }
        else
                printf("\n");
}

static void
l_gf_w16_split_4_16_lazy_sse_altmap_multiply_region(void *src, void *dest, int bytes, gf_val_32_t val, int xor, int pp)
{
#ifdef INTEL_SSSE3
  uint64_t i, j, *s64, *d64, *top64;;
  uint64_t c, prod;
  uint8_t low[4][16];
  uint8_t high[4][16];
  //gf_region_data rd;
  __m128i  mask, ta, tb, ti, tpl, tph, tlow[4], thigh[4];

  //printf("%s: val=%d bytes=%d val=x%x xor=%d\n", __func__, val, bytes, val, xor); 
  if (val == 0) { gf_multby_zero(dest, bytes, xor); return; }
  if (val == 1) { gf_multby_one(src, dest, bytes, xor); return; }

#if 0
  gf_set_region_data(&rd, gf, src, dest, bytes, val, xor, 32);
  gf_do_initial_region_alignment(&rd);
#endif

  for (j = 0; j < 16; j++) {
    for (i = 0; i < 4; i++) {
      c = (j << (i*4));
      //prod = gf->multiply.w32(gf, c, val);
      prod = l_gf16_multiply(c, val, pp);
      low[i][j] = (prod & 0xff);
      high[i][j] = (prod >> 8);
    }
  }

  for (i = 0; i < 4; i++) {
    tlow[i] = _mm_loadu_si128((__m128i *)low[i]);
    thigh[i] = _mm_loadu_si128((__m128i *)high[i]);
  }
  //printf("Tlow and Thigh\n");
  //l_print_i128(&tlow[0]);
  //l_print_i128(&thigh[0]);

#if 0
  s64 = (uint64_t *) rd.s_start;
  d64 = (uint64_t *) rd.d_start;
  top64 = (uint64_t *) rd.d_top;
#endif
  s64 = (uint64_t *) src; 
  d64 = (uint64_t *) dest; 
  top64 = (uint64_t *) dest + bytes/sizeof(uint64_t); 
   
  mask = _mm_set1_epi8 (0x0f);  // replicate byte 0x0f 16 times for 128b mask

  if (xor) {
    while (d64 != top64) {

      ta = _mm_load_si128((__m128i *) s64);
      tb = _mm_load_si128((__m128i *) (s64+2));

      ti = _mm_and_si128 (mask, tb);
      tph = _mm_shuffle_epi8 (thigh[0], ti);
      tpl = _mm_shuffle_epi8 (tlow[0], ti);
  
      tb = _mm_srli_epi16(tb, 4);
      ti = _mm_and_si128 (mask, tb);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[1], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[1], ti), tph);

      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[2], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[2], ti), tph);
  
      ta = _mm_srli_epi16(ta, 4);
      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[3], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[3], ti), tph);

      ta = _mm_load_si128((__m128i *) d64);
      tph = _mm_xor_si128(tph, ta);
      _mm_store_si128 ((__m128i *)d64, tph);
      tb = _mm_load_si128((__m128i *) (d64+2));
      tpl = _mm_xor_si128(tpl, tb);
      _mm_store_si128 ((__m128i *)(d64+2), tpl);

      //printf("%s: *d64=0x%llx, 0x%llx\n", __func__, *d64, *(d64+2));
      d64 += 4;
      s64 += 4;
    }
  } else {
    while (d64 != top64) {

      ta = _mm_load_si128((__m128i *) s64);
      tb = _mm_load_si128((__m128i *) (s64+2));

      // do operations per 4*64=256b 
      // start the operations to tb - high 128b of 256b value
      ti = _mm_and_si128 (mask, tb);  // AND tb with (0x0f) mask
                                      // ti has low order 4 bits for each byte
      tph = _mm_shuffle_epi8 (thigh[0], ti); // shuffle thigh[] with ti indices 
      tpl = _mm_shuffle_epi8 (tlow[0], ti);  // shuffle tlow[] with ti indices
  
      tb = _mm_srli_epi16(tb, 4);     // shift tb right by 4 bits
      ti = _mm_and_si128 (mask, tb);  // AND tb(>>4) with (0x0f) mask
      // shuffle tlow[] with ti(>>4) indices
      // then 128b XOR of shuffle'ed value and shuffled tlow[]
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[1], ti), tpl);
      // shuffle thigh[] with ti(>>4) indices
      // then 128b XOR of shuffle'ed value and shuffled thigh[]
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[1], ti), tph);

      // start the operations to ta - low 128b of 256b value
      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[2], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[2], ti), tph);
  //printf("TPL and TPH\n");
  //l_print_i128(&tpl);
  //l_print_i128(&tph);
  
      ta = _mm_srli_epi16(ta, 4);
      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[3], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[3], ti), tph);

      _mm_store_si128 ((__m128i *)d64, tph);
      _mm_store_si128 ((__m128i *)(d64+2), tpl);

      //printf("%s: *d64=0x%llx, 0x%llx\n", __func__, *d64, *(d64+2));
      d64 += 4;
      s64 += 4;
      
    }
  }
#if 0
  gf_do_final_region_alignment(&rd);
#endif

#endif
}
