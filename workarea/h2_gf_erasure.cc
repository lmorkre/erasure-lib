//
// h_gf_erasure.cc
//     HGST erasusre routines using Galois Field routines for GF(2^16)
//

#include <stdint.h>
#include <stdlib.h>
#include <errno.h>

#include <iostream>
#include <deque>

#include "h2_gf_erasure.h"
#include "h2_gf_matrix.h"

using namespace std;
using namespace h_erasure;

void
encode_buffer::set_alignment(uint32_t a)
{
    if (a >= 0x10)
        m_alignment = a;
}

void *
encode_buffer::allocate_buffer(uint64_t size)
{
    int  ret, ret2;
    void *tptr, *tptr2;

    std::cout << "allocate_buffer: called size " << size << std::endl;
    ret = posix_memalign(&tptr, m_alignment, size);
    ret2 = posix_memalign(&tptr2, m_alignment, size);
    if (ret != 0 || ret2 != 0) {
        std::cerr << "alloc_buffer ERROR " << errno << std::endl;
        return NULL;
    }
    m_src    = tptr;
    m_dst    = tptr2;
    m_size   = size;
    return tptr;
}

void
encode_buffer::free_buffer()
{
    if (m_src) {
        free(m_src);
        m_src = NULL;
    }
    if (m_dst) {
        free(m_dst);
        m_dst = NULL;
    }
    if (m_weight) {
        gf_free(&m_gf, 1);
        m_weight = 0;
    }
}

bool
encode_buffer::set_src(uint16_t val)
{
    uint32_t  i;
    uint16_t *intp;

    if (m_src) {
        intp = (uint16_t *)m_src;
        for (i=0; i < m_size; i += sizeof(uint16_t)) {
            *intp++ = val;
        }
        return true;
    } else
        return false;
}

static bool
print_buffer(void *bufp, int bytes)
{
    int i, cnt;
    uint16_t *intp;

    if (bufp == NULL ||  bytes == 0)
        return false;
    cnt = 1 ;
    intp = (uint16_t *)bufp;
    for (i=0; i < bytes; i += sizeof(uint16_t)) {
        printf(" x%x ", *intp++);
        if (!(cnt%8))
            printf("\n");
        cnt++;
    }
    return true;
}

void
encode_buffer::print_src()
{
    std::cout << "Print Src Buffer" << std::endl;
    print_buffer(m_src, m_size);
}

void
encode_buffer::print_dst()
{
    std::cout << "Print Dest Buffer" << std::endl;
    print_buffer(m_dst, m_size);
}

void
encode_buffer::create_generator(uint16_t *binary_rows, int binary_size,
                                MatrixInt16 &dst_matrix, int out_vector_length, int out_num_vectors)
{
    int      ret;
    uint16_t *out_col;

    ret = posix_memalign((void **)&out_col, 0x10, 4096);
    translate_matrix_row(binary_rows, out_col, 16);

    //printf("Output Columns\n");
    //print_buffer(out_col, 16*3);
    //free(out_col);
    gf_create_generator_matrix(binary_rows, binary_size, dst_matrix, out_vector_length, out_num_vectors);
    free(out_col);
#if 0
#endif
}

//
// add a unary_vector {1,1,1,...} to the specified row_num of the data_matrix
// Move all data in row(s) past "row_num" to row+1
//
bool
encode_buffer::add_unary_vector(MatrixInt16 &data_matrix, int row_num)
{
    int     matrix_size, vector_size;
    int     i, j;

    matrix_size = data_matrix.size();
    if (matrix_size == 0)
        return false;
    vector_size = data_matrix[0].size();
    if (vector_size == 0)
        return false;
    printf("%s: size of matrix_size is %d, vector_size %d\n", __func__, matrix_size, vector_size);
    data_matrix.resize(matrix_size+1);
    data_matrix[matrix_size].resize(vector_size);
    // move data up a row
    for (j=matrix_size; j > row_num; j--) {
        for (i=0; i < vector_size; i++) {
            data_matrix[j][i] = data_matrix[j-1][i];
        }
    }
    for (i=0; i < vector_size; i++) {
        data_matrix[row_num][i] = 1;
    }
    return true;
}

//
// take 2 input matrices, a row and a column
// calculate the Cauchy output matrix
//     which will be #
bool
encode_buffer::create_cauchy_encoder(MatrixInt16 &row_matrix, MatrixInt16 &col_matrix,
                                     MatrixInt16 &out_cauchy_matrix)
{
    int row_size, col_size, row_vec_size, col_vec_size;
    int i, j, k;
    uint16_t temp_1, temp_sum;

    row_size = row_matrix.size();
    col_size = col_matrix.size();
    row_vec_size = row_matrix[0].size();
    col_vec_size = col_matrix[0].size();

    printf("%s: size of row size is %d/%d, col size %d/%d\n", __func__,
      row_size, row_vec_size, col_size, col_vec_size);

    out_cauchy_matrix.resize(col_size);
    for (i=0; i<col_size; i++)
        out_cauchy_matrix[i].resize(col_vec_size);

    for (j=0; j<col_size; j++) {
        for (i=0; i<row_size; i++) {
            temp_1 = m_gf.multiply.w32(&m_gf, row_matrix[i][0], col_matrix[j][0]);
#if 0
            temp_1 = row_matrix[i][0] & col_matrix[j][0];
            temp_1 = h_gf16_multiply(row_matrix[i][0], col_matrix[j][0]);
#endif
            for (k=1; k<col_vec_size; k++) {
                //temp_sum = (row_matrix[i][k] & col_matrix[j][k]);
                temp_sum = m_gf.multiply.w32(&m_gf, row_matrix[i][k], col_matrix[j][k]);
#if 0
                temp_sum = h_gf16_multiply(row_matrix[i][k], col_matrix[j][k]);
#endif
                temp_1 ^= temp_sum;
            }
            //printf("%s: row %d, col %d result=x%x\n", __func__, i, j, temp_1);
            out_cauchy_matrix[j][i] = temp_1;
        }
    }
    return true;
}

int
encode_buffer::translate_matrix_column(void *column_data, void *dst_row,
               int items)
{
  uint16_t *col_p, *row_p, bit_temp, row_temp;
  int      i, j;

  col_p = (uint16_t *)column_data;
  row_p = (uint16_t *)dst_row;
  for (i=0; i<items; i++) {
    bit_temp = 1 << (15 - i);
    row_temp = 0;
    for (j=0; j<items; j++) {
      if (col_p[j] & bit_temp)
        row_temp |= 1 << (15 - j);
    }
    row_p[i] = row_temp;
    //printf("%s: row %d, column data 0x%x got data 0x%x\n", __func__, i, col_p[i], row_p[i]);
  }
  return 0;
}

int
encode_buffer::translate_matrix_row(void *row_data, void *dst_column, int items)
{
  uint16_t *col_p, *row_p, bit_temp, col_temp;
  int      i, j;

  row_p = (uint16_t *)row_data;
  col_p = (uint16_t *)dst_column;
  for (i=0; i<items; i++) {
    bit_temp = 1 << (15 - i);
    col_temp = 0;
    for (j=0; j<items; j++) {
      if (row_p[j] & bit_temp)
        col_temp |= 1 << (15 - j);
    }
    col_p[i] = col_temp;
    //printf("%s: row %d, column data 0x%x got data 0x%x\n", __func__, i, col_p[i], row_p[i]);
  }
  return 0;
}

// TODO: this is hardwired for w-16 bit at this time
uint32_t
encode_buffer::multiply_matrix(uint16_t *in_array, uint16_t mult1, int size)
{
  int       i, cnt;
  uint16_t  prod16, temp16;

  prod16 = 0;
  temp16 = 0;
  for (i=0; i<size; i++) {
      temp16 = in_array[i] & mult1;
      cnt = __builtin_popcountll(temp16);
      //printf("%s: 0x%x & 0x%x temp16 is 0x%x cnt is %d\n", __func__,
      //    in_array[i], mult1, temp16, cnt);
      if (cnt % 2)
         prod16 |= 1 << (15-i);
  }
  return prod16;
}

/*
 * generate a row/column vector based on a binary matrix input
 * input: binary matrix is 16 bit x 16 bit
 * output: a 2 dimensional array vector_length * num_vectors
 */
bool
encode_buffer::gf_create_generator_matrix(uint16_t *binary_rows,
                             int binary_size, MatrixInt16 &dst_matrix,
                             int vector_length, int num_vectors)
{
    int      ret, i, j, vec_size;
    uint16_t *input1, *temp16, *temp_column, *temp_row, *binary_columns;

    vec_size = binary_size * 2 + 1024;
    ret = posix_memalign((void **)&input1, 0x10, vec_size);
    ret = posix_memalign((void **)&temp16, 0x10, vec_size);
    ret = posix_memalign((void **)&temp_column, 0x10, vec_size);
    ret = posix_memalign((void **)&temp_row, 0x10, vec_size);
    ret = posix_memalign((void **)&binary_columns, 0x10, vec_size);

    translate_matrix_row(binary_rows, binary_columns, binary_size);
    dst_matrix.resize(num_vectors);
    for (i=0; i<num_vectors; i++)
        dst_matrix[i].resize(vector_length);

    temp16[0] = multiply_matrix(binary_rows, 0x0001, 16);
    for (i=0; i<(vector_length-1); i++) {
        temp16[i+1] = multiply_matrix(binary_rows, temp16[i], binary_size);
    }

    // Output Vector[j][0] is always 1
    dst_matrix[0][0] = 1;
    for (i=1; i<vector_length; i++)
        dst_matrix[0][i] = temp16[i-1];

    // calculate Matrix ^ 2
    for (i=0; i < 16; i++) {
        temp_column[i] = multiply_matrix(binary_rows, binary_columns[i],
                          binary_size);
        //printf("A * A Matrix column %d is 0x%x\n", i, temp_column[i]);
    }
    translate_matrix_column(temp_column, temp_row, 16);

    /* temp_row has Matrix ^ 2 to start */
    for (j=1; j < num_vectors; j++) {

        temp16[0] = multiply_matrix(temp_row, 0x0001, 16);
        // TODO: Does this need to related to binary_size?
        //       Think we must do 16 multiplication for next power of Matrix
        //printf("temp16 A^%d  0 output is 0x%x\n", j+2, temp16[0]);
        for (i=0; i<15; i++) {
            temp16[i+1] = multiply_matrix(temp_row, temp16[i], binary_size);
            //printf("temp16 A^%d: %d output is 0x%x\n", j+2, i, temp16[i+1]);
        }
        dst_matrix[j][0] = 1;    // Output Vector[j][0] is always 1
        for (i=1; i<vector_length; i++)
            dst_matrix[j][i] = temp16[i-1];

        // calculate next power of matrix
        for (i=0; i < 16; i++) {
            temp_column[i] = multiply_matrix(temp_row, binary_columns[i], 16);
            //printf("Matrix power %d, col %d is 0x%x\n", j+3, i, temp_column[i]);
        }
        translate_matrix_column(temp_column, temp_row, 16);
        for (i=0; i < 16; i++) {
            //printf(" Matrix power %d, col %d is 0x%x\n", j+3, i, temp_row[i]);
        }
    }
    free((void *)input1);
    free((void *)temp16);
    free((void *)temp_column);
    free((void *)temp_row);
    free((void *)binary_columns);
}

void
encode_buffer::print_timer_diff_usecs()
{
    uint64_t secs, usecs;
    secs = m_stoptime.tv_sec - m_starttime.tv_sec;
    usecs = secs * 1000000 + m_stoptime.tv_usec - m_starttime.tv_usec;
    printf("runtime=%10lu usecs\n", usecs);
}

/* size in bytes, print out in MB */

void
encode_buffer::print_timer_diff_usecs(uint32_t size)
{
    uint64_t secs, usecs;
    uint64_t mb_size;

    secs = m_stoptime.tv_sec - m_starttime.tv_sec;
    usecs = secs * 1000000 + m_stoptime.tv_usec - m_starttime.tv_usec;
    printf("runtime=%10lu usecs\n", usecs);
    if (size != 0) {
        printf(" size=%u in %.4f sec = %.2f ns/size\n", size,
          ((double)usecs)/1000000, ((double)usecs*1000.0)/(double)size);
    }
}

//==========================================================================
bool
gf_shift_multi::init(uint32_t w)
{
     if (gf_init_hard(&m_gf, 16, GF_MULT_SHIFT, GF_REGION_DEFAULT, 
            GF_DIVIDE_DEFAULT,
            0, 16, 4, NULL, NULL) == 0) {
        std::cerr << "gf_init_hard failed: " << _gf_errno << std::endl;
        return false;
    }
    return true;
}

bool
gf_shift_multi::encode(uint32_t multiplier)
{
    m_gf.multiply_region.w32(&m_gf, get_src(), get_dst(),
      multiplier, (int)get_size(), 0);
    return true;
}

bool
gf_shift_multi::decode(uint32_t multiplier)
{
    return true;
}

//==========================================================================
bool
gf_simd_table::init(uint32_t w)
{
     if (gf_init_hard(&m_gf, 16, GF_MULT_SPLIT_TABLE, GF_REGION_DEFAULT, 
            GF_DIVIDE_DEFAULT,
            0, 16, 4, NULL, NULL) == 0) {
        std::cerr << "gf_init_hard failed: " << _gf_errno << std::endl;
        return false;
    }
    return true;
}

bool
gf_simd_table::encode(uint32_t multiplier)
{
    //std::cout << "simd_table::encode() called " << std::endl;

    m_gf.multiply_region.w32(&m_gf, get_src(), get_dst(),
      multiplier, (int)get_size(), 0);
#if 0
    h_gf16_multiply_region_from_2(get_src(), get_dst(), get_size(),
        multiplier, get_polynomial());
#endif
    return true;
}

bool
gf_simd_table::decode(uint32_t multiplier)
{
    std::cout << "simd_table::decode() called " << std::endl;
    return true;
}

//==========================================================================
bool
gf_simd_altmap::init(uint32_t w)
{
    
    m_weight = w;
    if (gf_init_hard(&m_gf, 16, GF_MULT_SPLIT_TABLE, GF_REGION_ALTMAP, 
            GF_DIVIDE_DEFAULT, 0, 16, 4, NULL, NULL) == 0) {
        std::cerr << "gf_init_hard failed: " << _gf_errno << std::endl;
        return false;
    }
    return true;
}

bool
gf_simd_altmap::encode(uint32_t multiplier)
{
    std::cout << "simd_altmap::encode() called " << std::endl;

    m_gf.multiply_region.w32(&m_gf, get_src(), get_dst(),
      multiplier, (int)get_size(), 0);
#if 0
    h_encode_altmap(get_src(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
    h_gf16_split_4_16_lazy_sse_altmap_multiply_region(get_buffer(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
#endif
    return true;
}

bool
gf_simd_altmap::decode(uint32_t multiplier)
{
    std::cout << "simd_altmap::decode() called " << std::endl;
    return true;
}

//==========================================================================
bool
gf_simd_cauchy::init(uint32_t w)
{
    
    m_weight = w;
    if (gf_init_hard(&m_gf, 16, GF_MULT_LOG_TABLE, GF_REGION_CAUCHY,
      GF_DIVIDE_DEFAULT, 0, 0, 0, NULL, NULL) == 0) {
        std::cerr << "gf_init_hard failed: " << _gf_errno << std::endl;
        return false;
    }
    return true;
}

bool
gf_simd_cauchy::encode(uint32_t multiplier)
{
    std::cout << "simd_cauchy::encode() called " << std::endl;

    m_gf.multiply_region.w32(&m_gf, get_src(), get_dst(),
      multiplier, (int)get_size(), 0);
#if 0
    h_encode_altmap(get_src(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
    h_gf16_split_4_16_lazy_sse_altmap_multiply_region(get_buffer(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
#endif
    return true;
}

bool
gf_simd_cauchy::decode(uint32_t multiplier)
{
    std::cout << "simd_cauchy::decode() called " << std::endl;
    return true;
}
