/*
 * h_gf16_matrix library code
 *    do GF(2^16) Matrix calculations using code from GF-complete
 *
 * GF-Complete: A Comprehensive Open Source Library for Galois Field Arithmetic
 * James S. Plank, Ethan L. Miller, Kevin M. Greenan,
 * Benjamin A. Arnold, John A. Burnum, Adam W. Disney, Allen C. McBride.
 *
 */


#include <stdio.h>
#include <getopt.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include <vector>

#include <smmintrin.h>
#ifdef INTEL_SSE4
  #include <nmmintrin.h>
#endif

#ifdef INTEL_SSSE3
  #include <tmmintrin.h>
#endif

#ifdef INTEL_SSE2
  #include <emmintrin.h>
#endif

#ifdef INTEL_SSE4_PCLMUL
  #include <wmmintrin.h>
#endif

using namespace std;

#define GF16_PRIM_POLY 0x1100b
typedef std::vector<vector<uint16_t> >   MatrixInt16;

int         h_gf16_multiply_region_from_2(void *src, void *dst, int size, uint32_t m, uint32_t ipp);
uint32_t    h_gf16_multiply(uint32_t a16, uint32_t b16);
uint32_t    h_gf16_multiply_pp(uint32_t a16, uint32_t b16, uint32_t i_prim_poly);
uint16_t    h_mult_matrix_vector_i128(uint16_t *in_array, uint16_t mult1);
uint16_t    h_mult_matrix_by_hand(uint16_t *in_array, uint16_t mult1, int size);
int         h_shift_multiply(uint32_t a32, uint32_t b32);
int         h_matrix_translate_column(void *column_data, void *dst_row, int items);
int         h_matrix_translate_row(void *row_data, void *dst_column, int items);
int         h_perf_start(struct timeval *tv);
int         h_perf_stop(struct timeval *tv);
void        h_perf_print(struct timeval *stop, struct timeval *start, long long dsize);
void        h_print_i128(__m128i *in1);
void        h_perf_print2(struct timeval *stop, struct timeval *start, long long count);
bool        h_gf_create_generator_matrix(uint16_t *binary_matrix,
                                         MatrixInt16 &dst_matrix, int vector_length, int num_vectors);

/* based on GF_complete's gf_w16_split_4_16_lazy_sse_multiply_rewion */
int
h_gf16_multiply_region_from_2(void *src, void *dst, int size, uint32_t multiplier, uint32_t i_prim_poly)
{
    uint64_t i, j, c, prod;
    uint64_t *s64, *d64;
    uint8_t  low[4][16];
    uint8_t  high[4][16];
    int      ii;
    __m128i  tlow[4], thigh[4], mask, lmask;
    __m128i  ta, tb, ti, tta, ttb, tpl, tph;

    s64 = (uint64_t *)src;
    d64 = (uint64_t *)dst;
    for (j=0; j < 16; j++) {
        for (i=0; i < 4; i++) {
            c = (j << (i*4));
            prod = h_gf16_multiply_pp(c, multiplier, i_prim_poly);
            low[i][j] = (prod & 0xff);
            high[i][j] = (prod >> 8);
        }
    }
    for (i=0;  i < 4; i++) {
        tlow[i] = _mm_loadu_si128((__m128i *)low[i]);
        thigh[i] = _mm_loadu_si128((__m128i *)high[i]);
    }
    mask = _mm_set1_epi8(0x0f);
    lmask = _mm_set1_epi16(0xff);

    //printf("%s: src x%p dst x%p size %d\n", __func__, src, dst, size);
    for (ii=0; ii < size; ii+=32) {

      ta = _mm_load_si128((__m128i *) s64);
      tb = _mm_load_si128((__m128i *) (s64+2));

      tta = _mm_srli_epi16(ta, 8);
      ttb = _mm_srli_epi16(tb, 8);
      tpl = _mm_and_si128(tb, lmask);
      tph = _mm_and_si128(ta, lmask);

      tb = _mm_packus_epi16(tpl, tph);
      ta = _mm_packus_epi16(ttb, tta);

      ti = _mm_and_si128 (mask, tb);
      tph = _mm_shuffle_epi8 (thigh[0], ti);
      tpl = _mm_shuffle_epi8 (tlow[0], ti);

      tb = _mm_srli_epi16(tb, 4);
      ti = _mm_and_si128 (mask, tb);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[1], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[1], ti), tph);

      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[2], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[2], ti), tph);

      ta = _mm_srli_epi16(ta, 4);
      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[3], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[3], ti), tph);

      ta = _mm_unpackhi_epi8(tpl, tph);
      tb = _mm_unpacklo_epi8(tpl, tph);

      _mm_store_si128 ((__m128i *)d64, ta);
      _mm_store_si128 ((__m128i *)(d64+2), tb);

      //printf("i=%d (%d), ", ii, ((uint64_t)d64-(uint64_t)dst));
      //l_print_i128(&ta);
      //l_print_i128((__m128i *)d64);
      //printf("\t");
      //l_print_i128(&tb);
      //l_print_i128((__m128i *)(d64+2));
      d64 += 4;
      s64 += 4;
      // ii  += 32;
    }
    return 0;
}

void
h_print_i128(__m128i *in1)
{
    uint32_t  *i32_p = (uint32_t *)in1;

    printf("i128:x%p::=x%x x%x x%x x%x\n",
       in1, i32_p[0], i32_p[1], i32_p[2], i32_p[3]);
}

uint16_t
h_mult_matrix_vector_i128(uint16_t *in_array, uint16_t mult1)
{
  __m128i   mm128_var1, mm128_var2, mm128_mult, mm128_prod1, mm128_prod2;
  uint32_t  prod16, cnt, bitcnt;
  uint32_t  int1, int2, int3, int4;
  uint16_t  *a, ret_array16[8];
  uint8_t   ret_array[16];
  int i;

  // start of matrix calculation. start with 0x0001
  a = in_array;
  printf("start matrix multiplication of 0x%4.4x\n", mult1);
  mm128_var1 = _mm_set_epi16(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
  mm128_var2 = _mm_set_epi16(a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]);
  mm128_mult = _mm_set1_epi16(mult1);
  _mm_storeu_si128((__m128i *)ret_array, mm128_var1);
  int1 = *(uint32_t *)&ret_array[0];
  int2 = *(uint32_t *)&ret_array[4];
  int3 = *(uint32_t *)&ret_array[8];
  int4 = *(uint32_t *)&ret_array[12];
  printf(" var1 =  %8.8x %8.8x %8.8x %8.8x\n", int4, int3, int2, int1);
  _mm_storeu_si128((__m128i *)ret_array, mm128_var2);
  int1 = *(uint32_t *)&ret_array[0];
  int2 = *(uint32_t *)&ret_array[4];
  int3 = *(uint32_t *)&ret_array[8];
  int4 = *(uint32_t *)&ret_array[12];
  printf(" var2 =  %8.8x %8.8x %8.8x %8.8x\n", int4, int3, int2, int1);
  _mm_storeu_si128((__m128i *)ret_array, mm128_mult);
  int1 = *(uint32_t *)&ret_array[0];
  int2 = *(uint32_t *)&ret_array[4];
  int3 = *(uint32_t *)&ret_array[8];
  int4 = *(uint32_t *)&ret_array[12];
  printf(" mult =  %8.8x %8.8x %8.8x %8.8x\n", int4, int3, int2, int1);
  mm128_prod1 = _mm_and_si128(mm128_var1, mm128_mult);
  mm128_prod2 = _mm_and_si128(mm128_var2, mm128_mult);
  _mm_storeu_si128((__m128i *)ret_array16, mm128_prod1);
  printf(" prod1= %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x\n",
     ret_array16[0], ret_array16[1], ret_array16[2], ret_array16[3],
     ret_array16[4], ret_array16[5], ret_array16[6], ret_array16[7]);
  bitcnt = 15;
  prod16 = 0;
  for (i=7; i>=0; i--) {
      cnt = __builtin_popcountll(ret_array16[i]);
      if (cnt % 2)
         prod16 |= 1 << bitcnt;
      bitcnt--;
  }
  _mm_storeu_si128((__m128i *)ret_array16, mm128_prod2);
  for (i=7; i>=0; i--) {
      cnt = __builtin_popcountll(ret_array16[i]);
      if (cnt % 2)
         prod16 |= 1 << bitcnt;
      bitcnt--;
  }
  printf(" prod2= %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x %4.4x\n",
     ret_array16[0], ret_array16[1], ret_array16[2], ret_array16[3],
     ret_array16[4], ret_array16[5], ret_array16[6], ret_array16[7]);
  //printf(" bitprod=0x%x\n", prod16);
  return prod16;
}

uint16_t
h_mult_matrix_by_hand(uint16_t *in_array, uint16_t mult1, int size)
{
  int       i, cnt;
  uint16_t  prod16, temp16;

  prod16 = 0;
  temp16 = 0;
  for (i=0; i<size; i++) {
      temp16 = in_array[i] & mult1;
      cnt = __builtin_popcountll(temp16);
      //printf("%s: 0x%x & 0x%x temp16 is 0x%x cnt is %d\n", __func__,
      //    in_array[i], mult1, temp16, cnt);
      if (cnt % 2)
         prod16 |= 1 << (15-i);
  }
  return prod16;
}

/* copied from gf_wgen_shift_multiply() */
int
h_shift_multiply(uint32_t a32, uint32_t b32)
{
  uint64_t old_prod, product, i, pp, a, b, one;
  int      w;

  a = a32;
  b = b32;
  one = 1;
  w = 16;
  //pp = 013; /* for 3 */
  pp = GF16_PRIM_POLY; /* for 16 */

  product = 0;

  for (i = 0; i < w; i++) {
    if (a & (one << i)) product ^= (b << i);
    //printf("%s: loop 1. i=%d, XOR TRUE=%d w=0x%x product=0x%x\n", __func__, i, (a & (one << i)), (b<<i), product);
  }
  for (i = w*2-1; i >= w; i--) {
    old_prod = product;
    if (product & (one << i)) product ^= (pp << (i-w));
    //printf("%s: loop 2. i=%d, XOR TRUE=%d w=0x%x product=0x%x\n", __func__, i, (old_prod & (one << i)), (pp << (i-w)), product);
  }
  //printf("%s: x%x * x%x return product=0x%x\n", __func__, a32, b32, product);
  return product;
}

/* convert a Matrix column data to a row data */
/* assume these are 16b values */
/* column and row data are arrays of 16b column data */
int
h_matrix_translate_column(void *column_data, void *dst_row, int items)
{
  uint16_t *col_p, *row_p, bit_temp, row_temp;
  int      i, j;

  col_p = (uint16_t *)column_data;
  row_p = (uint16_t *)dst_row;
  for (i=0; i<items; i++) {
    bit_temp = 1 << (15 - i);
    row_temp = 0;
    for (j=0; j<items; j++) {
      if (col_p[j] & bit_temp)
        row_temp |= 1 << (15 - j);
    }
    row_p[i] = row_temp;
    //printf("%s: row %d, column data 0x%x got data 0x%x\n", __func__, i, col_p[i], row_p[i]);
  }
  return 0;
}

/* convert a Matrix row data to column data */
/* assume these are 16b values */
/* column and row data are arrays of 16b column data */
int
h_matrix_translate_row(void *row_data, void *dst_column, int items)
{
  uint16_t *col_p, *row_p, bit_temp, col_temp;
  int      i, j;

  row_p = (uint16_t *)row_data;
  col_p = (uint16_t *)dst_column;
  for (i=0; i<items; i++) {
    bit_temp = 1 << (15 - i);
    col_temp = 0;
    for (j=0; j<items; j++) {
      if (row_p[j] & bit_temp)
        col_temp |= 1 << (15 - j);
    }
    col_p[i] = col_temp;
    //printf("%s: row %d, column data 0x%x got data 0x%x\n", __func__, i, col_p[i], row_p[i]);
  }
  return 0;
}

/* based on Intel ISAL */
inline
int h_perf_start(struct timeval *tv)
{
        return gettimeofday(tv, 0);
}

inline
int h_perf_stop(struct timeval *tv)
{
        return gettimeofday(tv, 0);
}

inline
void h_perf_print(struct timeval *stop, struct timeval *start, long long dsize)
{
        long long secs = stop->tv_sec - start->tv_sec;
        long long usecs = secs * 1000000 + stop->tv_usec - start->tv_usec;

        printf("runtime = %10lld usecs", usecs);
        if (dsize != 0) {
#if 1 // not bug in printf for 32-bit
                printf(", bandwidth %lld MB in %.4f sec = %.2f MB/s\n", dsize/(1024*1024),
                        ((double) usecs)/1000000, ((double) dsize) / (double)usecs);
#else
                printf(", bandwidth %lld MB ", dsize/(1024*1024));
                printf("in %.4f sec ",(double)usecs/1000000);
                printf("= %.2f MB/s\n", (double)dsize/usecs);
#endif
        }
        else
                printf("\n");
}

inline
void h_perf_print2(struct timeval *stop, struct timeval *start, long long cnt)
{
        long long secs = stop->tv_sec - start->tv_sec;
        long long usecs = secs * 1000000 + stop->tv_usec - start->tv_usec;

        printf("runtime = %10lld usecs", usecs);
        if (cnt != 0) {
                printf(", count %lld in %.4f sec = %.2f ns/count\n", cnt,
                        ((double) usecs)/1000000, ((double) usecs * 1000.0) / (double)cnt);
        }
        else
                printf("\n");
}

/* based on gf_w16_clm_multiply_4 */
uint32_t
h_gf16_multiply_pp(uint32_t a16, uint32_t b16, uint32_t i_prim_poly)
{
    uint32_t    rv = 0;
    __m128i      a, b;
    __m128i      result;
    __m128i      prim_poly;
    __m128i      w;

    a = _mm_insert_epi32(_mm_setzero_si128(), a16, 0);
    b = _mm_insert_epi32(a, b16, 0);

    prim_poly = _mm_set_epi32(0, 0, 0, i_prim_poly);

    /* start initial mulitply */
    result = _mm_clmulepi64_si128(a, b, 0);

    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    // printf("5th multiply result is \n");
    // l_print_i128(result);

    /* extract 32b value to return */
    rv = ((uint32_t)_mm_extract_epi32(result, 0));
    //printf("returning 32b of x%x\n", rv);
    return rv;
}

uint32_t
h_gf16_multiply(uint32_t a16, uint32_t b16)
{
    uint32_t    pp = GF16_PRIM_POLY;
    uint32_t    rv = 0;
    __m128i      a, b;
    __m128i      result;
    __m128i      prim_poly;
    __m128i      w;

    a = _mm_insert_epi32(_mm_setzero_si128(), a16, 0);
    b = _mm_insert_epi32(a, b16, 0);

    prim_poly = _mm_set_epi32(0, 0, 0, GF16_PRIM_POLY);

    /* start initial mulitply */
    result = _mm_clmulepi64_si128(a, b, 0);

    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    w = _mm_clmulepi64_si128(prim_poly, _mm_srli_si128(result, 2), 0);
    result = _mm_xor_si128(result, w);
    // printf("5th multiply result is \n");
    // l_print_i128(result);

    /* extract 32b value to return */
    rv = ((uint32_t)_mm_extract_epi32(result, 0));
    //printf("returning 32b of x%x\n", rv);
    return rv;
}

void
h_encode_altmap(void *src, void *dest, int bytes,
                uint32_t val, int xxor, int pp)
{
#ifdef INTEL_SSSE3
  uint64_t i, j, *s64, *d64, *top64;;
  uint64_t c, prod;
  uint8_t low[4][16];
  uint8_t high[4][16];
  //gf_region_data rd;
  __m128i  mask, ta, tb, ti, tpl, tph, tlow[4], thigh[4];

  printf("%s: val=%d bytes=%d val=x%x xor=%d\n", __func__, val, bytes, val, xxor);
#if 0
  if (val == 0) { gf_multby_zero(dest, bytes, xor); return; }
  if (val == 1) { gf_multby_one(src, dest, bytes, xor); return; }

  gf_set_region_data(&rd, gf, src, dest, bytes, val, xor, 32);
  gf_do_initial_region_alignment(&rd);
#endif

  for (j = 0; j < 16; j++) {
    for (i = 0; i < 4; i++) {
      c = (j << (i*4));
      //prod = gf->multiply.w32(gf, c, val);
      prod = h_gf16_multiply_pp(c, val, pp);
      low[i][j] = (prod & 0xff);
      high[i][j] = (prod >> 8);
    }
  }

  for (i = 0; i < 4; i++) {
    tlow[i] = _mm_loadu_si128((__m128i *)low[i]);
    thigh[i] = _mm_loadu_si128((__m128i *)high[i]);
  }
  //printf("Tlow and Thigh\n");
  //l_print_i128(&tlow[0]);
  //l_print_i128(&thigh[0]);

#if 0
  s64 = (uint64_t *) rd.s_start;
  d64 = (uint64_t *) rd.d_start;
  top64 = (uint64_t *) rd.d_top;
#endif
  s64 = (uint64_t *) src;
  d64 = (uint64_t *) dest;
  top64 = (uint64_t *) dest + bytes/sizeof(uint64_t);
   
  mask = _mm_set1_epi8 (0x0f);  // replicate byte 0x0f 16 times for 128b mask

  if (xxor) {
    while (d64 != top64) {

      ta = _mm_load_si128((__m128i *) s64);
      tb = _mm_load_si128((__m128i *) (s64+2));

      ti = _mm_and_si128 (mask, tb);
      tph = _mm_shuffle_epi8 (thigh[0], ti);
      tpl = _mm_shuffle_epi8 (tlow[0], ti);
  
      tb = _mm_srli_epi16(tb, 4);
      ti = _mm_and_si128 (mask, tb);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[1], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[1], ti), tph);

      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[2], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[2], ti), tph);
  
      ta = _mm_srli_epi16(ta, 4);
      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[3], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[3], ti), tph);

      ta = _mm_load_si128((__m128i *) d64);
      tph = _mm_xor_si128(tph, ta);
      _mm_store_si128 ((__m128i *)d64, tph);
      tb = _mm_load_si128((__m128i *) (d64+2));
      tpl = _mm_xor_si128(tpl, tb);
      _mm_store_si128 ((__m128i *)(d64+2), tpl);

      //printf("%s: *d64=0x%llx, 0x%llx\n", __func__, *d64, *(d64+2));
      d64 += 4;
      s64 += 4;
    }
  } else {
    while (d64 != top64) {

      ta = _mm_load_si128((__m128i *) s64);
      tb = _mm_load_si128((__m128i *) (s64+2));

      // do operations per 4*64=256b
      // start the operations to tb - high 128b of 256b value
      ti = _mm_and_si128 (mask, tb);  // AND tb with (0x0f) mask
                                      // ti has low order 4 bits for each byte
      tph = _mm_shuffle_epi8 (thigh[0], ti); // shuffle thigh[] with ti indices
      tpl = _mm_shuffle_epi8 (tlow[0], ti);  // shuffle tlow[] with ti indices
  
      tb = _mm_srli_epi16(tb, 4);     // shift tb right by 4 bits
      ti = _mm_and_si128 (mask, tb);  // AND tb(>>4) with (0x0f) mask
      // shuffle tlow[] with ti(>>4) indices
      // then 128b XOR of shuffle'ed value and shuffled tlow[]
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[1], ti), tpl);
      // shuffle thigh[] with ti(>>4) indices
      // then 128b XOR of shuffle'ed value and shuffled thigh[]
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[1], ti), tph);

      // start the operations to ta - low 128b of 256b value
      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[2], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[2], ti), tph);
  //printf("TPL and TPH\n");
  //l_print_i128(&tpl);
  //l_print_i128(&tph);
  
      ta = _mm_srli_epi16(ta, 4);
      ti = _mm_and_si128 (mask, ta);
      tpl = _mm_xor_si128(_mm_shuffle_epi8 (tlow[3], ti), tpl);
      tph = _mm_xor_si128(_mm_shuffle_epi8 (thigh[3], ti), tph);

      _mm_store_si128 ((__m128i *)d64, tph);
      _mm_store_si128 ((__m128i *)(d64+2), tpl);

      //printf("%s: *d64=0x%llx, 0x%llx\n", __func__, *d64, *(d64+2));
      d64 += 4;
      s64 += 4;
      
    }
  }
#if 0
  gf_do_final_region_alignment(&rd);
#endif

#endif
}

#define DSIZE 4096
/*
 * generate a row/column vector based on a binary matrix input
 * input: binary matrix is 16 bit x 16 bit
 * output: a 2 dimensional array vector_length * num_vectors
 */
bool
h_gf_create_generator_matrix(uint16_t *binary_rows,
                             int binary_size, MatrixInt16 &dst_matrix,
                             int vector_length, int num_vectors)
{
    int      ret, i, j, vec_size;
    uint16_t *input1, *temp16, *temp_column, *temp_row, *binary_columns;

    vec_size = binary_size * 2 + 1024;
    ret = posix_memalign((void **)&input1, 0x10, vec_size);
    ret = posix_memalign((void **)&temp16, 0x10, vec_size);
    ret = posix_memalign((void **)&temp_column, 0x10, vec_size);
    ret = posix_memalign((void **)&temp_row, 0x10, vec_size);
    ret = posix_memalign((void **)&binary_columns, 0x10, vec_size);
   
    h_matrix_translate_row(binary_rows, binary_columns, binary_size);
    dst_matrix.resize(num_vectors);
    for (i=0; i<num_vectors; i++)
        dst_matrix[i].resize(vector_length);


    temp16[0] = h_mult_matrix_by_hand(binary_rows, 0x0001, 16);
    for (i=0; i<(vector_length-1); i++) {
        temp16[i+1] = h_mult_matrix_by_hand(binary_rows, temp16[i], binary_size);
    }

    // Output Vector[j][0] is always 1
    dst_matrix[0][0] = 1;
    for (i=1; i<vector_length; i++)
        dst_matrix[0][i] = temp16[i-1];

    // calculate Matrix ^ 2
    for (i=0; i < 16; i++) {
        temp_column[i] = h_mult_matrix_by_hand(binary_rows, binary_columns[i], binary_size);
        //printf("A * A Matrix column %d is 0x%x\n", i, temp_column[i]);
    }
    h_matrix_translate_column(temp_column, temp_row, 16);

    /* temp_row has Matrix ^ 2 to start */
    for (j=1; j < num_vectors; j++) {

        temp16[0] = h_mult_matrix_by_hand(temp_row, 0x0001, 16);
        // TODO: Does this need to related to binary_size?
        //       Think we must do 16 multiplication for next power of Matrix
        //printf("temp16 A^%d  0 output is 0x%x\n", j+2, temp16[0]);
        for (i=0; i<15; i++) {
            temp16[i+1] = h_mult_matrix_by_hand(temp_row, temp16[i], binary_size);
            //printf("temp16 A^%d: %d output is 0x%x\n", j+2, i, temp16[i+1]);
        }
        dst_matrix[j][0] = 1;    // Output Vector[j][0] is always 1
        for (i=1; i<vector_length; i++)
            dst_matrix[j][i] = temp16[i-1];

        // calculate next power of matrix
        for (i=0; i < 16; i++) {
            temp_column[i] = h_mult_matrix_by_hand(temp_row, binary_columns[i], 16);
            //printf("Matrix power %d, col %d is 0x%x\n", j+3, i, temp_column[i]);
        }
        h_matrix_translate_column(temp_column, temp_row, 16);
        for (i=0; i < 16; i++) {
            //printf(" Matrix power %d, col %d is 0x%x\n", j+3, i, temp_row[i]);
        }
    }
    free((void *)input1);
    free((void *)temp16);
    free((void *)temp_column);
    free((void *)temp_row);
    free((void *)binary_columns);
}
