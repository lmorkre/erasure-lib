//
// h_gf_encode.cc
//     HGST erasusre routines using Galois Field routines for GF(2^16)
//

#include <stdint.h>
#include <stdlib.h>
#include <errno.h>

#include <iostream>
#include <deque>

#include "h_gf_encode.h"
#include "h_gf_matrix.h"

using namespace std;
using namespace h_erasure;

void
encode_buffer::set_alignment(uint32_t a)
{
    if (a >= 0x10)
        m_alignment = a;
}

void *
encode_buffer::allocate_buffer(uint64_t size)
{
    int  ret, ret2;
    void *tptr, *tptr2;

    std::cout << "allocate_buffer: called size " << size << std::endl;
    ret = posix_memalign(&tptr, m_alignment, size);
    ret2 = posix_memalign(&tptr2, m_alignment, size);
    if (ret != 0 || ret2 != 0) {
        std::cerr << "alloc_buffer ERROR " << errno << std::endl;
        return NULL;
    }
    m_src    = tptr;
    m_dst    = tptr2;
    m_size   = size;
    return tptr;
}

void
encode_buffer::free_buffer()
{
    if (m_src) {
        free(m_src);
        m_src = NULL;
    }
    if (m_dst) {
        free(m_dst);
        m_dst = NULL;
    }
}

bool
encode_buffer::set_src(uint16_t val)
{
    int      i;
    uint16_t *intp;

    if (m_src) {
        intp = (uint16_t *)m_src;
        for (i=0; i < m_size; i += sizeof(uint16_t)) {
            *intp++ = val;
        }
        return true;
    } else
        return false;
}

static bool
print_buffer(void *bufp, int bytes)
{
    int i, cnt;
    uint16_t *intp;

    if (bufp == NULL ||  bytes == 0)
        return false;
    cnt = 1 ;
    intp = (uint16_t *)bufp;
    for (i=0; i < bytes; i += sizeof(uint16_t)) {
        printf(" x%x ", *intp++);
        if (!(cnt%8))
            printf("\n");
        cnt++;
    }
    return true;
}

void
encode_buffer::print_src()
{
    std::cout << "Print Src Buffer" << std::endl;
    print_buffer(m_src, m_size);
}

void
encode_buffer::print_dst()
{
    std::cout << "Print Dest Buffer" << std::endl;
    print_buffer(m_dst, m_size);
}

void
encode_buffer::create_generator(uint16_t *binary_rows, int binary_size,
                                MatrixInt16 &dst_matrix, int out_vector_length, int out_num_vectors)
{
    int      ret;
    uint16_t *out_col;

#if 0
    ret = posix_memalign((void **)&out_col, 0x10, 4096);
    h_matrix_translate_row(binary_rows, out_col, 16);

    //printf("Output Columns\n");
    //print_buffer(out_col, 16*3);
    //free(out_col);
#endif
    h_gf_create_generator_matrix(binary_rows, binary_size, dst_matrix, out_vector_length, out_num_vectors);
}

//
// add a unary_vector {1,1,1,...} to the specified row_num of the data_matrix
// Move all data in row(s) past "row_num" to row+1
//
bool
encode_buffer::add_unary_vector(MatrixInt16 &data_matrix, int row_num)
{
    int     matrix_size, vector_size;
    int     i, j;

    matrix_size = data_matrix.size();
    if (matrix_size == 0)
        return false;
    vector_size = data_matrix[0].size();
    if (vector_size == 0)
        return false;
    printf("%s: size of matrix_size is %d, vector_size %d\n", __func__, matrix_size, vector_size);
    data_matrix.resize(matrix_size+1);
    data_matrix[matrix_size].resize(vector_size);
    // move data up a row
    for (j=matrix_size; j > row_num; j--) {
        for (i=0; i < vector_size; i++) {
            data_matrix[j][i] = data_matrix[j-1][i];
        }
    }
    for (i=0; i < vector_size; i++) {
        data_matrix[row_num][i] = 1;
    }
    return true;
}

//
// take 2 input matrices, a row and a column
// calculate the Cauchy output matrix
//     which will be #
bool
encode_buffer::create_cauchy_encoder(MatrixInt16 &row_matrix, MatrixInt16 &col_matrix,
                                     MatrixInt16 &out_cauchy_matrix)
{
    int row_size, col_size, row_vec_size, col_vec_size;
    int i, j, k;
    uint16_t temp_1, temp_sum;

    row_size = row_matrix.size();
    col_size = col_matrix.size();
    row_vec_size = row_matrix[0].size();
    col_vec_size = col_matrix[0].size();

    printf("%s: size of row size is %d/%d, col size %d/%d\n", __func__,
      row_size, row_vec_size, col_size, col_vec_size);

    out_cauchy_matrix.resize(col_size);
    for (i=0; i<col_size; i++)
        out_cauchy_matrix[i].resize(col_vec_size);

    for (j=0; j<col_size; j++) {
        for (i=0; i<row_size; i++) {
            temp_1 = row_matrix[i][0] & col_matrix[j][0];
            temp_1 = h_gf16_multiply(row_matrix[i][0], col_matrix[j][0]);
            for (k=1; k<col_vec_size; k++) {
                //temp_sum = (row_matrix[i][k] & col_matrix[j][k]);
                temp_sum = h_gf16_multiply(row_matrix[i][k], col_matrix[j][k]);
                temp_1 ^= temp_sum;
            }
            //printf("%s: row %d, col %d result=x%x\n", __func__, i, j, temp_1);
            out_cauchy_matrix[j][i] = temp_1;
        }
    }
    return true;
}

void
encode_buffer::print_timer_diff_usecs()
{
    uint64_t secs, usecs;
    secs = m_stoptime.tv_sec - m_starttime.tv_sec;
    usecs = secs * 1000000 + m_stoptime.tv_usec - m_starttime.tv_usec;
    printf("runtime=%10lu usecs\n", usecs);
}

/* size in bytes, print out in MB */

void
encode_buffer::print_timer_diff_usecs(uint32_t size)
{
    uint64_t secs, usecs;
    uint64_t mb_size;

    secs = m_stoptime.tv_sec - m_starttime.tv_sec;
    usecs = secs * 1000000 + m_stoptime.tv_usec - m_starttime.tv_usec;
    printf("runtime=%10lu usecs\n", usecs);
    if (size != 0) {
        printf(" size=%u in %.4f sec = %.2f ns/size\n", size,
          ((double)usecs)/1000000, ((double)usecs*1000.0)/(double)size);
    }
}

bool
encode_shift_multi::encode(uint32_t multiplier)
{
    return true;
}

bool
encode_shift_multi::decode(uint32_t multiplier)
{
    return true;
}

bool
encode_simd_table::encode(uint32_t multiplier)
{
    //std::cout << "simd_table::encode() called " << std::endl;

    h_gf16_multiply_region_from_2(get_src(), get_dst(), get_size(),
        multiplier, get_polynomial());
    return true;
}

bool
encode_simd_table::decode(uint32_t multiplier)
{
    std::cout << "simd_table::decode() called " << std::endl;
    return true;
}

bool
encode_simd_altmap::encode(uint32_t multiplier)
{
    void *v1, *v2;
    int   i1, i2, i3;

    std::cout << "simd_altmap::encode() called " << std::endl;

    h_encode_altmap(get_src(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
#if 0
    h_gf16_split_4_16_lazy_sse_altmap_multiply_region(get_buffer(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
#endif
    return true;
}

bool
encode_simd_altmap::decode(uint32_t multiplier)
{
    std::cout << "simd_altmap::decode() called " << std::endl;
    return true;
}
