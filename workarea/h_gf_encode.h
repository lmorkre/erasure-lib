//
//  h_erasure.h
//

#ifndef _H_ERASURE_H
#define _H_ERASURE_H

#include <stdint.h>
#include <sys/time.h>
#include <vector>

using namespace std;

typedef std::vector<vector<uint16_t> >   MatrixInt16;

namespace h_erasure {

class encode_op {
  public:
    virtual bool encode(uint32_t val) = 0;
    virtual bool decode(uint32_t val) = 0;
};

class encode_buffer : public encode_op {
  public:
    encode_buffer() {
        m_alignment = 0x10;
        m_polynomial = 0x1100b; // for GF(2^16)
    }
    typedef enum {
        ENCODE_OP = 0,
        DECODE_OP,
    } encode_operation_t;
    typedef enum {
        SHIFT_MULTI = 0,
        SIMD_TABLE,
        SIMD_ALTMAP
    } encode_type_t;

#if 0
    bool encode();
    bool decode();
#endif

    void *allocate_buffer(uint64_t size);
    void free_buffer();
    void set_alignment(uint32_t x);
    bool set_src(uint16_t val);
    void print_src();
    void print_dst();
    void start_timer()   { gettimeofday(&m_starttime, 0); }
    void stop_timer()    { gettimeofday(&m_stoptime, 0); }
    void print_timer_diff_usecs();
    void print_timer_diff_usecs(uint32_t size);

    bool set_encode_type();
    void *get_src()     { return m_src; }
    void *get_dst()     { return m_dst;   }
    uint64_t get_size() { return m_size;  }
    uint32_t get_polynomial() { return m_polynomial; }
    void create_generator(uint16_t *binary_rows, int binary_size,
                          MatrixInt16 &dst_matrix, int vector_length, int num_vectors);
    bool add_unary_vector(MatrixInt16 &data_matrix, int row);
    bool create_cauchy_encoder(MatrixInt16 &row_matrix, MatrixInt16 &col_matrix,
                                     MatrixInt16 &out_cauchy_matrix);
    

  private:
    uint32_t   m_tag;  // unique id for each encode if needed
    void      *m_src;
    void      *m_dst;
    uint32_t   m_alignment;
    uint64_t   m_size;
    uint32_t   m_encode_type;
    uint32_t   m_polynomial;
    struct     timeval m_starttime;
    struct     timeval m_stoptime;
};

class encode_shift_multi : public encode_buffer {
  public:
    bool encode(uint32_t val);
    bool decode(uint32_t val);
};

class encode_simd_table : public encode_buffer {
  public:
    bool encode(uint32_t val);
    bool decode(uint32_t val);
};

class encode_simd_altmap : public encode_buffer {
  public:
    bool encode(uint32_t val);
    bool decode(uint32_t val);
}; 

};

#endif


