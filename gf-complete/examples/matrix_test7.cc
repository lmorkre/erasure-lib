//
// matrix_test7.cc
//    Cauchy Encoder and Decoder example
//    change parity_p (parity data) from MatrixInt16 to raw uint16_t *
//    add a loop for calculating parity with GF-complete calls
//    add support to use GF-complete for all the encoding
//    add support for both GF-complete SPLIT_TABLE and ALTMAP
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <iostream>

#include "h2_gf_erasure.h"

#define DSIZE    4096
#define NSEC     32
#define PSEC     5
#define ITERS    80000

using namespace h_erasure;
using namespace std;

// define  GF(2^16) with flat arithmatic
// min poly = x^16+x^12+x^3+1
galois16_t power(galois16_t base, uint16_t exp) {
    galois16_t ans = 1;
    galois16_t mult = base;
    while (exp > 0) {
        if (exp & 1)
            ans *= mult;
        mult *= mult;
        exp >>= 1;
    }
    return ans;
}

int
main(int argc, char *argv[])
{
    //class gf_simd_table table1;
    class gf_simd_altmap table1;
    uint64_t        tot, dsize;
    int             nelements;
    int             i, j, k, cnt;
    uint16_t        *a, *b, *d2_p, *s2_p;
    int             g2_a[NSEC], g2_b[PSEC], max_parity, g_roots, g_parity;
    int             disk_map[NSEC], parity_map[PSEC];
    int             erased_loc[NSEC], parity_loc[PSEC];
    MatrixInt16     g_cauchy;
    uint16_t        *parity_p[PSEC]; 
    MatrixInt16     calc_p;
    MatrixInt16     syndrome_p;
    MatrixInt16     recovered_p;
    VectorInt16     g_a, g_bi;
    VectorInt16     temp_out;
    int             nsec = NSEC, psec = PSEC;
    bool            bool1 = false;
    uint8_t         *sector_p[NSEC];   // data sector
    uint16_t        *cal2_p[NSEC], *par2_p[NSEC];
    char            ch;
    uint16_t        *p_ptr, *e_ptr;

    dsize = DSIZE;
    while ((ch = getopt(argc, argv, "d:")) != -1) {
        switch (ch) {
          case 'd':
             dsize = atoi(optarg);
             break;
          default:
             printf("unknown argument %c, exiting\n", ch);
             break;
        }
    }
    nelements = dsize/2;
    g_roots = 0;
    g_parity = 0;

    i = posix_memalign((void **)&a, 0x10, dsize);
    i = posix_memalign((void **)&b, 0x10, dsize);

    table1.init(16);
    table1.allocate_src(dsize*(NSEC+PSEC));
    table1.allocate_dst(dsize*(NSEC+PSEC));
    table1.set_size(dsize);
    table1.set_src(0x4);
    // do GF16 multiplication with specified multiplier with the src buffer
    //table1.print_dst();
    table1.create_cauchy_matrix(g_cauchy, nsec, psec, bool1, g2_a, g2_b);

    for (i=0; i<nsec; i++) {
        sector_p[i] = (uint8_t *)malloc(dsize);
        table1.fill_data_sector(sector_p[i], i*4, dsize, 0xab);
         j = posix_memalign((void **)&cal2_p[i], 0x10, nelements*sizeof(uint16_t));
         memset(cal2_p[i], 0, nelements*sizeof(uint16_t));
         j = posix_memalign((void **)&par2_p[i], 0x10, nelements*sizeof(uint16_t));
    }
    for (i=0; i<psec; i++) {
         j = posix_memalign((void **)&parity_p[i], 0x10, nelements*sizeof(uint16_t));
    }
    calc_p.resize(psec);
    for (i=0; i<psec; i++) {
       calc_p[i].resize(nelements);
    }
    syndrome_p.resize(psec);
    for (i=0; i<psec; i++) {
       syndrome_p[i].resize(nelements);
    }
    recovered_p.resize(psec);
    for (i=0; i<psec; i++) {
        recovered_p[i].resize(nelements);
    }
    temp_out.resize(nsec);
    for (i=0; i<nsec; i++) {
        printf("Data Sector %d:\n", i);
        d2_p = (uint16_t *)sector_p[i];
        for (j=0; j< nelements; j++) {
            printf(" x%hx", *d2_p++);
        }
        printf("\n");
    }
    for (i=0; i<PSEC; i++) {
        arrout("Cauchy row = ", g_cauchy, i, NSEC);
    }

#ifdef OLD
    for (i=0; i<nsec; i++) {
        table1.cauchy_encode_parity(sector_p[i], g_cauchy, parity_p, i, psec, nelements, false, false);
    }
#else
    for (j=0; j<psec; j++) {
        for (i=0; i<nsec; i++) {
            table1.multiply_region(sector_p[i], par2_p[j], g_cauchy[j][i], nelements*2, true); 
        }
    }
#endif
    printf("Parity Sector: \n");
    for (j=0; j<psec; j++) {
        printf(" [");
        for (i=0; i<nelements; i++) {
#ifdef OLD
            printf(" x%hx", parity_p[j][i]);
#else
            printf(" x%hx", par2_p[j][i]);
#endif
        }
        printf("]\n");
    }

    //==========================================================================
    // Start Decoding Test
    //==========================================================================
    cout << "+++++++++++++++++++++++++ << Decoding Test >> ++++++++++++++++++++++" << endl;
    cout << " o Mark Erased Data Sectors " << endl;
    cout << " o Mark Erased Parity Sectors" << endl;
    // Data Skip Mask: set to 1 for bad data locations
    // For test, mark disk 23-26 as bad
    bzero(disk_map, sizeof(disk_map));
    bzero(parity_map, sizeof(parity_map));
    disk_map[0]=0; disk_map[1]=0; disk_map[2]=0; disk_map[3]=0; disk_map[4]=0;
    disk_map[5]=0; disk_map[6]=0; disk_map[7]=0; disk_map[8]=0; disk_map[9]=0;
    disk_map[10]=0; disk_map[11]=0; disk_map[12]=0; disk_map[13]=0; disk_map[14]=0;
    disk_map[15]=0; disk_map[16]=0; disk_map[17]=0; disk_map[18]=0; disk_map[19]=0;
    disk_map[20]=0; disk_map[21]=0; disk_map[22]=0; disk_map[23]=1; disk_map[24]=1;
    disk_map[25]=1; disk_map[26]=1; disk_map[27]=0; disk_map[28]=0; disk_map[29]=0;
    // Parity Skip Mask
    // Not used at this point. All parity data is good
    parity_map[0]=0; parity_map[1]=0; parity_map[2]=0; parity_map[3]=0; parity_map[4]=0;
    // erased_loc - array to remember bad data sectors
    g_roots = 0;
    for (j=0; j<nsec; j++) {
        if (disk_map[j] == 1) {
            erased_loc[g_roots] = j;
            g_roots++;
        }
    }
    // parity_loc - array to remember bad parity data
    g_parity = 0;
    for (j=0; j<psec; j++) {
        if (parity_map[j] != 1) {
            parity_loc[g_parity] = j;
            g_parity++;
        }
    }
    arrout("diskipm[] = ", disk_map, nsec);
    arrout("pskipm[] = ", parity_map, psec);
    cout << "g_roots   : " << g_roots << endl;
    cout << "g_parity  : " <<  g_parity << endl;
    arrout("eras_loc[] = ", erased_loc, g_roots);
    arrout("prty_loc[] = ", parity_loc, g_parity);
    if (g_roots <= g_parity) {
       max_parity = g_roots;
    } else {
        cout << "ERROR: More bad data than parity sectors" << endl;
        exit(1);
    }
    cout << "++++++++++++++++++++++++++ Start Decoding +++++++++++++++++++++++" << endl;
    cout << " 1) Syndrome Generation: " << endl;
    cout << "   o Read all data back except the erased (bad) data sectors     " << endl;
    cout << "   o Syndrome = Good Data Sectors * Dmatrix + Parity Sectors     " << endl;
    cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    // Read in good data sectors
    //    This is determined by disk_map parameter
    //    ?? calc_p contains the temporary decode parity for the calculation
#ifdef OLD
    for (i=0; i<nsec; i++) {
        table1.cauchy_encode_parity(sector_p[i], g_cauchy, calc_p, i, psec, nelements, disk_map[i], false);
    }
#else
    for (j=0; j<psec; j++) {
        for (i=0; i<nsec; i++) {
            if (!disk_map[i])
                table1.multiply_region(sector_p[i], cal2_p[j], g_cauchy[j][i], nelements*2, true); 
        }
    }
#endif
    // Read in parity sectors, calculate into symdrome_p
#ifdef OLD
    for (i=0; i<psec; i++) {
        table1.cauchy_encode_syndrome(parity_p, calc_p, syndrome_p, i, nelements, false);
    }
#else
    for (i=0; i<psec; i++) {
        table1.cauchy_encode_syndrome(par2_p, cal2_p, syndrome_p, i, nelements, false);
    }
#endif
#ifdef DEBUG
    printf("cal2_p \n");
    for (j=0; j<nsec; j++) { 
        p_ptr = cal2_p[j];
        for (i=0; i<nsec; i++) {
            printf("x%hx ", *p_ptr++);
        }
        printf("\n");
    }
#endif
    cout << "+++++++++++++++++++++++++ Syndrome Sectors +++++++++++++++++++++++" << endl;
    for (i=0; i<psec; i++) {
        arrout("Syndrome Sector = ", syndrome_p, i, nelements);
    }
    cout << "++++++++++++++++++++++++ DMatrix +++++++++++++++++++++++++++++++++" << endl;
    cout << " 2) Find Dmatrix D[i,j] i=good parity, j=erased sectors           " << endl;
    cout << " 3) Invert DMatirix                                               " << endl;
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    // DMatrix - recreate Cauchy using recovery data and parity sectors
    table1.cauchy_dmatrix(g_cauchy, g_a, g_bi, nsec, psec, g_roots, max_parity, true, erased_loc, parity_loc, true);

    cout << "Invert Dmatrix here" << endl;
    table1.cauchy_dinvert(g_cauchy, g_a, g_bi, g_roots, psec, true);
    
    cout << "+++++++++++++++++++++++++ Syndrome Sectors +++++++++++++++++++++++" << endl;
    for (i=0; i<g_roots; i++) {
        arrout("Syndrome = ", syndrome_p, i, nelements);
    }

    for (int i=0; i < g_roots; i++) {
        for (int j=0; j < nelements; j++) {
            for (int s=0; s < g_roots; s++) {
                galois16_t g_temp = g_cauchy[s][i];
                galois16_t s_temp = syndrome_p[s][j];
                galois16_t r_temp = recovered_p[i][j];

                r_temp += g_temp * s_temp;
                recovered_p[i][j] = r_temp.value();
            }
        }
    }

    std::cout << " ===== Erased Sectors. =====" << std::endl;
    for (int i=0; i<g_roots; i++) {
        arrout("Erased Sector - ", recovered_p, i, nelements);
    }

    // try to recover data using GF-complete encoder
    s2_p = (uint16_t *)table1.get_src();
    d2_p = (uint16_t *)table1.get_dst();
    //uint16_t *p_ptr, *e_ptr;
    p_ptr = (uint16_t *)s2_p;
    e_ptr = (uint16_t *)d2_p;
    for (int s=0; s< g_roots; s++) {
        for (i=0; i < nelements; i++) {
                *p_ptr++ = syndrome_p[s][i];
                *e_ptr++ = 0;
        }
    }
    printf("P_Ptr :\n");
    p_ptr = (uint16_t *)s2_p;
    for (j=0; j<g_roots; j++) {
        for (i=0; i<nelements; i++) {
            printf("x%hx ", *p_ptr++);
        }
        printf("\n");
    }

    e_ptr = (uint16_t *)d2_p;
    printf("Filled up p_ptr\n");
    int chunk_size = nelements;
    for (i=0; i< g_roots; i++) {
        p_ptr = (uint16_t *)s2_p;
        for (int s=0; s < g_roots; s++) {
            uint16_t inv_temp = g_cauchy[s][i];
            printf("multiply_region %d,%d. multiplier=x%hx, Syndrome-1(x%p)=x%hx x%x\n",
			    i, s, inv_temp, p_ptr, *p_ptr, chunk_size);
            table1.multiply_region(p_ptr, e_ptr, (int32_t)inv_temp, nelements*2, true); 
            p_ptr += nelements;
        }
        e_ptr += nelements;
    }
    printf("Erasure Recovered Sector from GF-complete encoding:\n");
    e_ptr = (uint16_t *)d2_p;
    for (j=0; j<g_roots; j++) {
        for (i=0; i<nelements; i++) {
            printf("x%hx ", *e_ptr++);
        }
        printf("\n");
    }
    

    // do a bunch of encoding for performance measurement
    cnt = 0;
    table1.start_timer();
    for (k=0; k < ITERS; k++) {
        for (i=0; i < psec; i++) {
            for (j=0; j<32; j++) {
                table1.encode((uint32_t)parity_p[i][j], false);
                cnt++;
            }
        }
    }
    table1.stop_timer();
    tot = (uint64_t)dsize*(uint64_t)cnt;
    std::cout << " ============== Encoding Performance ===========" << std::endl;
 
    printf(" Transfered x%llx MB (cnt=x%x), block size %ld\n",
		    (long long)tot/1048576, cnt, dsize);
    table1.print_timer_diff_usecs(tot);
    table1.free_buffer();
    for (i=0; i<nsec; i++) {
        if (sector_p[i] != NULL)
            free(sector_p[i]);
        if (cal2_p[i] != NULL)
	    free(cal2_p[i]);
        if (par2_p[i] != NULL)
	    free(par2_p[i]);
    }
    for (i=0; i<psec; i++) {
        if (parity_p[i] != NULL)
	    free(parity_p[i]);
    }
    free(a);
    free(b);
}
