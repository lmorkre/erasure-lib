//
// h_gf_erasure.cc
//     HGST erasusre routines using Galois Field routines for GF(2^16)
//

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>

#include <iostream>
#include <deque>

#include "h2_gf_erasure.h"
#if 0
#include "h2_gf_matrix.h"
#endif

using namespace std;
using namespace h_erasure;

int debug = 0;
int b_power = 23;
int expand_roots(galois16_t f[], galois16_t df[], VectorInt16 &g_a, int num_erased);
galois16_t evaluate_poly(galois16_t v[], galois16_t dv[], uint16_t erased_pt,
              int deg);
galois16_t evaluate_derivative(galois16_t v[], uint16_t erased_pt, int deg);

#if 0
galois16_t operator + (galois16_t x, galois16_t y) 
{
    printf("\t operator + %hx , %hx\n", x.value(), y.value());
    return galois16_t(x.value() ^ y.value());
}

galois16_t operator * (galois16_t x, galois16_t y) 
{
    printf("\t operator * %hx , %hx\n", x.value(), y.value());
    galois16_t temp = x;
    temp += y;
    return temp; 
}

galois16_t operator * (galois16_t x, uint16_t y) 
{
    printf("\t operator-2 * %hx , %hx\n", x.value(), y);
    galois16_t temp = y;
    temp += x;
    return temp; 
}
#endif

void
encode_buffer::set_alignment(uint32_t a)
{
    if (a >= 0x10)
        m_alignment = a;
}

void *
encode_buffer::allocate_buffer(uint64_t size)
{
    int  ret, ret2;
    void *tptr, *tptr2;

    std::cout << "allocate_buffer: called size " << size << std::endl;
    ret = posix_memalign(&tptr, m_alignment, size);
    ret2 = posix_memalign(&tptr2, m_alignment, size);
    if (ret != 0 || ret2 != 0) {
        std::cerr << "alloc_buffer ERROR " << errno << std::endl;
        return NULL;
    }
    m_src    = tptr;
    m_dst    = tptr2;
    m_size   = size;
    m_src_size = size;
    m_dst_size = size;
    return tptr;
}

void *
encode_buffer::allocate_src(uint64_t size)
{
    int  ret;
    void *tptr;

    std::cout << "allocate_src: called size " << size << std::endl;
    ret = posix_memalign(&tptr, m_alignment, size);
    if (ret != 0) {
        std::cerr << "alloc_src ERROR " << errno << std::endl;
        return NULL;
    }
    m_src    = tptr;
    m_src_size = size;
    return tptr;
}

void *
encode_buffer::allocate_dst(uint64_t size)
{
    int  ret;
    void *tptr;

    std::cout << "allocate_dst: called size " << size << std::endl;
    ret = posix_memalign(&tptr, m_alignment, size);
    if (ret != 0) {
        std::cerr << "alloc_dst ERROR " << errno << std::endl;
        return NULL;
    }
    m_dst    = tptr;
    m_dst_size = size;
    return tptr;
}

void
encode_buffer::free_buffer()
{
    if (m_src) {
        free(m_src);
        m_src = NULL;
        m_src_size = 0;
    }
    if (m_dst) {
        free(m_dst);
        m_dst = NULL;
        m_dst_size = 0;
    }
    if (m_weight) {
        gf_free(&m_gf, 1);
        m_weight = 0;
    }
}

bool
encode_buffer::set_src(uint16_t val)
{
    uint32_t  i;
    uint16_t *intp;

    if (m_src) {
        intp = (uint16_t *)m_src;
        for (i=0; i < m_src_size; i += sizeof(uint16_t)) {
            *intp++ = val;
        }
        return true;
    } else
        return false;
}

static bool
print_buffer(void *bufp, int bytes)
{
    int i, cnt;
    uint16_t *intp;

    if (bufp == NULL ||  bytes == 0)
        return false;
    cnt = 1 ;
    intp = (uint16_t *)bufp;
    for (i=0; i < bytes; i += sizeof(uint16_t)) {
        printf(" x%x ", *intp++);
        if (!(cnt%8))
            printf("\n");
        cnt++;
    }
    return true;
}

void
encode_buffer::print_src()
{
    std::cout << "Print Src Buffer" << std::endl;
    print_buffer(m_src, m_size);
}

void
encode_buffer::print_dst()
{
    std::cout << "Print Dest Buffer" << std::endl;
    print_buffer(m_dst, m_size);
}

void
encode_buffer::create_generator(uint16_t *binary_rows, int binary_size,
                                MatrixInt16 &dst_matrix, int out_vector_length, int out_num_vectors)
{
    int      ret;
    uint16_t *out_col;

    ret = posix_memalign((void **)&out_col, 0x10, 4096);
    if (ret)
        return;
    translate_matrix_row(binary_rows, out_col, 16);

    //printf("Output Columns\n");
    //print_buffer(out_col, 16*3);
    //free(out_col);
    gf_create_generator_matrix(binary_rows, binary_size, dst_matrix, out_vector_length, out_num_vectors);
    free(out_col);
}

//
// add a unary_vector {1,1,1,...} to the specified row_num of the data_matrix
// Move all data in row(s) past "row_num" to row+1
//
bool
encode_buffer::add_unary_vector(MatrixInt16 &data_matrix, int row_num)
{
    int     matrix_size, vector_size;
    int     i, j;

    matrix_size = data_matrix.size();
    if (matrix_size == 0)
        return false;
    vector_size = data_matrix[0].size();
    if (vector_size == 0)
        return false;
    printf("%s: size of matrix_size is %d, vector_size %d\n", __func__, matrix_size, vector_size);
    data_matrix.resize(matrix_size+1);
    data_matrix[matrix_size].resize(vector_size);
    // move data up a row
    for (j=matrix_size; j > row_num; j--) {
        for (i=0; i < vector_size; i++) {
            data_matrix[j][i] = data_matrix[j-1][i];
        }
    }
    for (i=0; i < vector_size; i++) {
        data_matrix[row_num][i] = 1;
    }
    return true;
}

//
// take 2 input matrices, a row and a column
// calculate the Cauchy output matrix
//     which will be #
bool
encode_buffer::create_cauchy_encoder(MatrixInt16 &row_matrix, MatrixInt16 &col_matrix,
                                     MatrixInt16 &out_cauchy_matrix)
{
    int row_size, col_size, row_vec_size, col_vec_size;
    int i, j, k;
    uint16_t temp_1, temp_sum;

    row_size = row_matrix.size();
    col_size = col_matrix.size();
    row_vec_size = row_matrix[0].size();
    col_vec_size = col_matrix[0].size();

    printf("%s: size of row size is %d/%d, col size %d/%d\n", __func__,
      row_size, row_vec_size, col_size, col_vec_size);

    out_cauchy_matrix.resize(col_size);
    for (i=0; i<col_size; i++)
        out_cauchy_matrix[i].resize(col_vec_size);

    for (j=0; j<col_size; j++) {
        for (i=0; i<row_size; i++) {
            temp_1 = m_gf.multiply.w32(&m_gf, row_matrix[i][0], col_matrix[j][0]);
#if 0
            temp_1 = row_matrix[i][0] & col_matrix[j][0];
            temp_1 = h_gf16_multiply(row_matrix[i][0], col_matrix[j][0]);
#endif
            for (k=1; k<col_vec_size; k++) {
                //temp_sum = (row_matrix[i][k] & col_matrix[j][k]);
                temp_sum = m_gf.multiply.w32(&m_gf, row_matrix[i][k], col_matrix[j][k]);
#if 0
                temp_sum = h_gf16_multiply(row_matrix[i][k], col_matrix[j][k]);
#endif
                temp_1 ^= temp_sum;
            }
            //printf("%s: row %d, col %d result=x%x\n", __func__, i, j, temp_1);
            out_cauchy_matrix[j][i] = temp_1;
        }
    }
    return true;
}

int
encode_buffer::translate_matrix_column(void *column_data, void *dst_row,
               int items)
{
  uint16_t *col_p, *row_p, bit_temp, row_temp;
  int      i, j;

  col_p = (uint16_t *)column_data;
  row_p = (uint16_t *)dst_row;
  for (i=0; i<items; i++) {
    bit_temp = 1 << (15 - i);
    row_temp = 0;
    for (j=0; j<items; j++) {
      if (col_p[j] & bit_temp)
        row_temp |= 1 << (15 - j);
    }
    row_p[i] = row_temp;
    //printf("%s: row %d, column data 0x%x got data 0x%x\n", __func__, i, col_p[i], row_p[i]);
  }
  return 0;
}

int
encode_buffer::translate_matrix_row(void *row_data, void *dst_column, int items)
{
  uint16_t *col_p, *row_p, bit_temp, col_temp;
  int      i, j;

  row_p = (uint16_t *)row_data;
  col_p = (uint16_t *)dst_column;
  for (i=0; i<items; i++) {
    bit_temp = 1 << (15 - i);
    col_temp = 0;
    for (j=0; j<items; j++) {
      if (row_p[j] & bit_temp)
        col_temp |= 1 << (15 - j);
    }
    col_p[i] = col_temp;
    //printf("%s: row %d, column data 0x%x got data 0x%x\n", __func__, i, col_p[i], row_p[i]);
  }
  return 0;
}

// TODO: this is hardwired for w-16 bit at this time
uint32_t
encode_buffer::multiply_matrix(uint16_t *in_array, uint16_t mult1, int size)
{
  int       i, cnt;
  uint16_t  prod16, temp16;

  prod16 = 0;
  temp16 = 0;
  for (i=0; i<size; i++) {
      temp16 = in_array[i] & mult1;
      cnt = __builtin_popcountll(temp16);
      //printf("%s: 0x%x & 0x%x temp16 is 0x%x cnt is %d\n", __func__,
      //    in_array[i], mult1, temp16, cnt);
      if (cnt % 2)
         prod16 |= 1 << (15-i);
  }
  return prod16;
}

/*
 * generate a row/column vector based on a binary matrix input
 * input: binary matrix is 16 bit x 16 bit
 * output: a 2 dimensional array vector_length * num_vectors
 */
bool
encode_buffer::gf_create_generator_matrix(uint16_t *binary_rows,
                             int binary_size, MatrixInt16 &dst_matrix,
                             int vector_length, int num_vectors)
{
    int      i, j, vec_size;
    uint16_t *input1, *temp16, *temp_column, *temp_row, *binary_columns;

    vec_size = binary_size * 2 + 1024;
    i = posix_memalign((void **)&input1, 0x10, vec_size);
    i = posix_memalign((void **)&temp16, 0x10, vec_size);
    i = posix_memalign((void **)&temp_column, 0x10, vec_size);
    i = posix_memalign((void **)&temp_row, 0x10, vec_size);
    i = posix_memalign((void **)&binary_columns, 0x10, vec_size);

    translate_matrix_row(binary_rows, binary_columns, binary_size);
    dst_matrix.resize(num_vectors);
    for (i=0; i<num_vectors; i++)
        dst_matrix[i].resize(vector_length);

    temp16[0] = multiply_matrix(binary_rows, 0x0001, 16);
    for (i=0; i<(vector_length-1); i++) {
        temp16[i+1] = multiply_matrix(binary_rows, temp16[i], binary_size);
    }

    // Output Vector[j][0] is always 1
    dst_matrix[0][0] = 1;
    for (i=1; i<vector_length; i++)
        dst_matrix[0][i] = temp16[i-1];

    // calculate Matrix ^ 2
    for (i=0; i < 16; i++) {
        temp_column[i] = multiply_matrix(binary_rows, binary_columns[i],
                          binary_size);
        //printf("A * A Matrix column %d is 0x%x\n", i, temp_column[i]);
    }
    translate_matrix_column(temp_column, temp_row, 16);

    /* temp_row has Matrix ^ 2 to start */
    for (j=1; j < num_vectors; j++) {

        temp16[0] = multiply_matrix(temp_row, 0x0001, 16);
        // TODO: Does this need to related to binary_size?
        //       Think we must do 16 multiplication for next power of Matrix
        //printf("temp16 A^%d  0 output is 0x%x\n", j+2, temp16[0]);
        for (i=0; i<15; i++) {
            temp16[i+1] = multiply_matrix(temp_row, temp16[i], binary_size);
            //printf("temp16 A^%d: %d output is 0x%x\n", j+2, i, temp16[i+1]);
        }
        dst_matrix[j][0] = 1;    // Output Vector[j][0] is always 1
        for (i=1; i<vector_length; i++)
            dst_matrix[j][i] = temp16[i-1];

        // calculate next power of matrix
        for (i=0; i < 16; i++) {
            temp_column[i] = multiply_matrix(temp_row, binary_columns[i], 16);
            //printf("Matrix power %d, col %d is 0x%x\n", j+3, i, temp_column[i]);
        }
        translate_matrix_column(temp_column, temp_row, 16);
        for (i=0; i < 16; i++) {
            //printf(" Matrix power %d, col %d is 0x%x\n", j+3, i, temp_row[i]);
        }
    }
    free((void *)input1);
    free((void *)temp16);
    free((void *)temp_column);
    free((void *)temp_row);
    free((void *)binary_columns);
    return true;
}

void
encode_buffer::create_cauchy_matrix(MatrixInt16 &out_cauchy_matrix,
                             int num_data, int num_parity, bool is_decode,
                             int err_loc_arg[], int parity_loc_arg[])
{
    galois16_t  data_matrix[num_data], parity_matrix[num_parity];
    galois16_t prim = galois16_t::prime_elem();
    int erase_loc;
    int parity_loc;

    out_cauchy_matrix.resize(num_parity);
    for (int i=0; i < num_parity; i++) {
        out_cauchy_matrix[i].resize(num_data);
    }
    for (int i=0; i < num_parity; i++) {
        if (is_decode) {
            parity_loc = parity_loc_arg[i];
            if (debug) {
                std::cout << "parity_loc = " << parity_loc << std::endl;
            }
            parity_matrix[i] = prim.power(prim, 1*(parity_loc));
        } else {
            parity_matrix[i] = prim.power(prim, 1*(i));
        }
    }
    for (int j=0; j < num_data; j++) {
        if (is_decode) {
            erase_loc = err_loc_arg[j];
            if (debug) {
                std::cout << "erase_loc = " << erase_loc << std::endl;
            }
            data_matrix[j] = prim.power(prim, b_power*(erase_loc+1));
        } else {
            data_matrix[j] = prim.power(prim, b_power*(j+1));
        }
    }
    for (int i=0; i < num_parity; i++) {
        for (int j=0; j < num_data; j++) {
            galois16_t g = parity_matrix[i] + data_matrix[j];
            //galois16_t g2 = g.invert();
            out_cauchy_matrix[i][j] = g.invert().value();
        }
    }
}

bool
encode_buffer::fill_data_sector(uint8_t *data_p, int sector_num,
                                      int sector_size,   int random_fill)
{
    uint16_t *d16_p = (uint16_t *)data_p;

    for (int i=0; i<sector_size; i+=2) {
        *d16_p++ = (uint16_t)random_fill;
    }
    if (sector_size < 16)
        return false;
    d16_p = (uint16_t *)data_p;
    *d16_p++ = 0x99;
    *d16_p++ = 0x77;
    *d16_p++ = sector_num;
    *d16_p++ = sector_num+1;
    *d16_p++ = sector_num+2;
    *d16_p++ = sector_num+3;
    return true;
}

//
// calculate parity from data and Cauchy encoder
//     input: data sector
//            cauchy matrix
//     ouput: parity
bool
encode_buffer::cauchy_encode_parity(uint8_t *sector_p, MatrixInt16 &cauchy_matrix,
                             MatrixInt16 &out_parity_p, int sector_num, int num_parity, int num_symbols,
                             bool bad_sector, bool is_parity)
{
    uint16_t *d_p = (uint16_t *)sector_p;
    galois16_t out16, out2;
 
    if (!bad_sector) {
        for (int j=0; j< num_symbols; j++) {
            galois16_t data_in = d_p[j];
            if (is_parity) {
                out16 = out_parity_p[sector_num][j];
                out16 += data_in;
                out_parity_p[sector_num][j] = out16.value();
            } else {
                for (int i=0; i < num_parity; i++) {
                    out16 = out_parity_p[i][j];
                    out2 = (data_in * cauchy_matrix[i][sector_num]);
                    out16 += out2;
                    out_parity_p[i][j] = out16.value();
                }
            }
        }
    }
    return true;
}

bool
encode_buffer::cauchy_encode_parity(uint8_t *sector_p, MatrixInt16 &cauchy_matrix,
                             uint16_t  *parity_p[], int sector_num, int num_parity, int num_symbols,
                             bool bad_sector, bool is_parity)
{
    uint16_t *d_p = (uint16_t *)sector_p;
    galois16_t out16, out2;
 
    if (!bad_sector) {
        for (int j=0; j< num_symbols; j++) {
            galois16_t data_in = d_p[j];  
            if (is_parity) {
                out16 = parity_p[sector_num][j];
                out16 += data_in;
                parity_p[sector_num][j] = out16.value();
            } else {
                for (int i=0; i < num_parity; i++) {
                    out16 = parity_p[i][j];
                    out2 = (data_in * cauchy_matrix[i][sector_num]);
                    out16 += out2;
                    parity_p[i][j] = out16.value();
                }
            }
        }
    }
    return true;
}

bool
encode_buffer::cauchy_encode_syndrome(MatrixInt16 &parity_p, MatrixInt16 &calc_p, MatrixInt16 &syndrome_p, 
                                    int sector_num, int num_symbols, bool bad_sector)
{
    galois16_t out16, out2;
 
    if (!bad_sector) {
        for (int i=0; i< num_symbols; i++) {
            galois16_t data_in = parity_p[sector_num][i];
            out16 =  calc_p[sector_num][i];
            out16 += data_in;
            syndrome_p[sector_num][i] = out16.value();
        }
    }
    return true;
}

bool
encode_buffer::cauchy_encode_syndrome(uint16_t *parity_p[], MatrixInt16 &calc_p, MatrixInt16 &syndrome_p, 
                                    int sector_num, int num_symbols, bool bad_sector)
{
    galois16_t out16, out2;
 
    if (!bad_sector) {
        for (int i=0; i< num_symbols; i++) {
            galois16_t data_in = parity_p[sector_num][i];
            out16 =  calc_p[sector_num][i];
            out16 += data_in;
            syndrome_p[sector_num][i] = out16.value();
        }
    }
    return true;
}

bool
encode_buffer::cauchy_encode_syndrome(uint16_t *parity_p[], uint16_t *calc_p[], MatrixInt16 &syndrome_p, 
                                    int sector_num, int num_symbols, bool bad_sector)
{
    galois16_t out16, out2;
 
    if (!bad_sector) {
        for (int i=0; i< num_symbols; i++) {
            galois16_t data_in = parity_p[sector_num][i];
            out16 =  calc_p[sector_num][i];
            out16 += data_in;
            syndrome_p[sector_num][i] = out16.value();
        }
    }
    return true;
}

void
encode_buffer::cauchy_dmatrix(MatrixInt16 &out_cauchy, VectorInt16 &out_a,
                                    VectorInt16 &out_bi, int num_data, int num_parity,
                                    int num_roots, int max_num_parity, bool is_decode, 
                                    int erased_arg[], int parity_arg[], bool display)
{
    galois16_t  data_matrix[num_data], parity_matrix[num_parity];
    galois16_t prim = galois16_t::prime_elem();
    int erase_loc;
    int parity_loc;

    out_a.resize(num_parity);
    out_bi.resize(num_data);
    for (int i=0; i < max_num_parity; i++) {
        if (is_decode) {
            parity_loc = parity_arg[i];
            if (debug) { std::cout << "parity loc = " << parity_loc << std::endl; }
            parity_matrix[i] = prim.power(prim, 1*parity_loc);
        } else {
            parity_matrix[i] = prim.power(prim, 1*i);
        }
        out_a[i] = (int)parity_matrix[i].value();
    }
    for (int j=0; j < num_roots; j++) {
        if (is_decode) {
            erase_loc = erased_arg[j];
            if (debug) { std::cout << "num_roots "<< num_roots << " erase_loc = " << erase_loc << std::endl; }
            data_matrix[j] = prim.power(prim, b_power*(erase_loc+1));
        } else {
            data_matrix[j] = prim.power(prim, b_power*(j+1));
        }
        out_bi[j] = (int)data_matrix[j].value();
        if (debug) { printf(" Dmatrix Cauchy: bi: %d: x%hx\n", j, out_bi[j]); } 
    }
    if (debug) { printf(" Dmatrix Cauchy: roots=%d, max_parity=%d\n", num_roots, max_num_parity); }
    for (int i=0; i < max_num_parity; i++) {
        for (int j=0; j < num_roots; j++) {
            galois16_t g = parity_matrix[i] + data_matrix[j];
            //galois16_t g2 = g.invert(); 
            out_cauchy[i][j] = g.invert().value();
            if (debug) { printf(" x%hx", out_cauchy[i][j]); }
        }
        if (debug) { printf("\n"); }
    }
}

void
encode_buffer::cauchy_dinvert(MatrixInt16 &g_cauchy, VectorInt16 &g_a, 
                              VectorInt16 &g_bi, 
                              int num_erased, int num_parity, bool display) 
{
    galois16_t  f[num_parity+1], g[num_parity+1];
    galois16_t  df[num_parity+1], dg[num_parity+1];
    galois16_t  wl[num_parity+1], wr[num_parity+1];
    galois16_t  left_nu, left_dn, right_nu, right_dn; 

    std::cout << "====== Parity Sector Polynomial F(x) ===============" << std::endl;
    for (int i=0; i < num_erased; i++) {
        f[i] = g[i] = 0; 
    }
    f[0] = 1;
    expand_roots(f, df, g_a, num_erased); 
    printf("Erspt (a) = [ ");
    for (int i=0; i<num_erased; i++) {
        printf(" %hx", g_a[i]);
    }
    printf(" ] \n");
    printf("F(x) (f) = [ ");
    for (int i=0; i<num_erased+1; i++) {
        printf(" %hx", f[i].value());
    }
    printf(" ] \n");
    printf("dF(x) (df) = [ ");
    for (int i=0; i<((num_erased+1)/2); i++) {
        printf(" %hx", df[i].value());
    }
    printf(" ] \n");
    std::cout << "===== Erased Data Sector Polynomical G(x) ======" << std::endl;
    g[0] = 1;
    expand_roots(g, dg, g_bi, num_erased);
    printf("Erspt (a) = [ ");
    for (int i=0; i<num_erased; i++) {
        printf(" %hx", g_bi[i]);
    }
    printf(" ] \n");
    printf("G(x) (g) = [ ");
    for (int i=0; i<num_erased+1; i++) {
        printf(" %hx", g[i].value());
    }
    printf(" ] \n");
    printf("dG(x) (dg) = [ ");
    for (int i=0; i<((num_erased+1)/2); i++) {
        printf(" %hx", dg[i].value());
    }
    printf(" ] \n");
    std::cout << " ===== Evaluate F(x)/dG(x) at bi[i], i=0..3 =====" << std::endl;
    for (int k=0; k < num_erased; k++) {
        left_nu = evaluate_poly(f, df, g_bi[k], num_erased);
        left_dn = evaluate_derivative(dg, g_bi[k], (num_erased+1)/2);
        wl[k] = left_nu * left_dn.invert();
        if (debug) {
            printf("\tleft_nu = x%hx\n", left_nu.value());
            printf("\tleft_dn = x%hx\n", left_dn.value());
            printf("\twl      = x%hx\n", wl[k].value()); 
        }
    }
    printf("Left-MW = [ ");
    for (int i=0; i<num_erased; i++) {
        printf(" x%x", wl[i].value());
    }
    printf(" ] \n");
    std::cout << " ===== Evaluate G(x)/dF(x) at a[i], i=0..3 =====" << std::endl;
    for (int k=0; k < num_erased; k++) {
        right_nu = evaluate_poly(g, dg, g_a[k], num_erased);
        right_dn = evaluate_derivative(df, g_a[k], (num_erased+1)/2);
        wr[k] = right_nu * right_dn.invert();
        if (debug) {
            printf("\tright_nu = x%hx\n", right_nu.value());
            printf("\tright_dn = x%hx\n", right_dn.value());
            printf("\twr      = x%hx\n", wr[k].value()); 
        }
    }
    printf("Right-MW = [ ");
    for (int i=0; i<num_erased; i++) {
        printf(" x%x", wr[i].value());
    }
    printf(" ] \n");
    // Find the inverse of D^-1..=wl*d*wr
    for (int j=0; j < num_erased; j++) {
        galois16_t t = wl[j];
        printf("\t wl[j] %d: = x%hx\n", j, t.value());
        for (int i=0; i < num_erased; i++) {
            galois16_t temp = g_cauchy[i][j];
            temp *= t;
            g_cauchy[i][j] = temp.value();
        }
    }
    for (int i=0; i < num_erased; i++) {
        galois16_t t = wr[i];
        printf("\t wr[i] %d: = x%hx\n", i, t.value());
        for (int j=0; j < num_erased; j++) {
            galois16_t temp = g_cauchy[i][j];
            temp *= t;
            g_cauchy[i][j] = temp.value();
        }
    }
    std::cout << "===== Inverse of Cauchy Matrix    =====" << std::endl;
    for (int i=0; i<num_erased; i++) {
        printf("D[i] %d = [ ", i);
        for (int j=0; j < num_erased; j++) {
            printf(" x%hx", g_cauchy[i][j]);
        }
        printf(" ] \n");
    }
}

//==========================================================================
void
encode_buffer::print_timer_diff_usecs()
{
    uint64_t secs, usecs;
    secs = m_stoptime.tv_sec - m_starttime.tv_sec;
    usecs = secs * 1000000 + m_stoptime.tv_usec - m_starttime.tv_usec;
    printf("runtime=%10lu usecs\n", usecs);
}

/* size in bytes, print out in MB */

void
encode_buffer::print_timer_diff_usecs(uint64_t size)
{
    uint64_t secs, usecs;

    secs = m_stoptime.tv_sec - m_starttime.tv_sec;
    usecs = secs * 1000000 + m_stoptime.tv_usec - m_starttime.tv_usec;
    printf("runtime=%10lu usecs\n", usecs);
    if (size != 0) {
        printf(" size=%jd MB in %.4f sec = %.2f MB/s\n", size/1048576,
          ((double)usecs/1000000), (double)(size/1048576)/((double)usecs/1000000.0));
#if 0
        printf(" size=%u in %.4f sec = %.2f ns/size\n", size,
          ((double)usecs)/1000000, ((double)usecs*1000.0)/(double)size);
#endif
    }
}

//==========================================================================
bool
gf_shift_multi::init(uint32_t w)
{
     if (gf_init_hard(&m_gf, 16, GF_MULT_SHIFT, GF_REGION_DEFAULT, 
            GF_DIVIDE_DEFAULT,
            0, 16, 4, NULL, NULL) == 0) {
        std::cerr << "gf_init_hard failed: " << _gf_errno << std::endl;
        return false;
    }
    return true;
}

bool
gf_shift_multi::encode(uint32_t multiplier, bool xor_flag)
{
    m_gf.multiply_region.w32(&m_gf, get_src(), get_dst(),
      multiplier, (int)get_size(), xor_flag);
    return true;
}

bool
gf_shift_multi::decode(uint32_t multiplier)
{
    return true;
}

//==========================================================================
bool
gf_simd_table::init(uint32_t w)
{
     if (gf_init_hard(&m_gf, 16, GF_MULT_SPLIT_TABLE, GF_REGION_DEFAULT, 
            GF_DIVIDE_DEFAULT,
            0, 16, 4, NULL, NULL) == 0) {
        std::cerr << "gf_init_hard failed: " << _gf_errno << std::endl;
        return false;
    }
    return true;
}

bool
gf_simd_table::encode(uint32_t multiplier, bool xor_flag)
{
    //std::cout << "simd_table::encode() called size=0x"<< get_size() << std::endl;

    m_gf.multiply_region.w32(&m_gf, get_src(), get_dst(),
      multiplier, (int)get_size(), xor_flag);
#if 0
    h_gf16_multiply_region_from_2(get_src(), get_dst(), get_size(),
        multiplier, get_polynomial());
    return true;
#endif
    return true;
}

bool
gf_simd_table::decode(uint32_t multiplier)
{
    std::cout << "simd_table::decode() called " << std::endl;
    return true;
}

bool
gf_simd_table::multiply_region(void *src, void *dst, uint32_t multiplier, uint32_t size, bool xor_flag)
{
    m_gf.multiply_region.w32(&m_gf, src, dst, multiplier, size, xor_flag);
    return true;
}

//==========================================================================
bool
gf_simd_altmap::init(uint32_t w)
{
    
    m_weight = w;
    if (gf_init_hard(&m_gf, 16, GF_MULT_SPLIT_TABLE, GF_REGION_ALTMAP, 
            GF_DIVIDE_DEFAULT, 0, 16, 4, NULL, NULL) == 0) {
        std::cerr << "gf_init_hard failed: " << _gf_errno << std::endl;
        return false;
    }
    return true;
}

bool
gf_simd_altmap::encode(uint32_t multiplier, bool xor_flag)
{
    //std::cout << "simd_altmap::encode() called " << std::endl;

    m_gf.multiply_region.w32(&m_gf, get_src(), get_dst(),
      multiplier, (int)get_size(), xor_flag);
#if 0
    h_encode_altmap(get_src(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
    h_gf16_split_4_16_lazy_sse_altmap_multiply_region(get_buffer(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
#endif
    return true;
}

bool
gf_simd_altmap::decode(uint32_t multiplier)
{
    std::cout << "simd_altmap::decode() called " << std::endl;
    return true;
}

bool
gf_simd_altmap::multiply_region(void *src, void *dst, uint32_t multiplier, uint32_t size, bool xor_flag)
{
    m_gf.multiply_region.w32(&m_gf, src, dst, multiplier, size, xor_flag);
    return true;
}

//==========================================================================
bool
gf_simd_cauchy::init(uint32_t w)
{
    
    m_weight = w;
    if (gf_init_hard(&m_gf, 16, GF_MULT_LOG_TABLE, GF_REGION_CAUCHY,
      GF_DIVIDE_DEFAULT, 0, 0, 0, NULL, NULL) == 0) {
        std::cerr << "gf_init_hard failed: " << _gf_errno << std::endl;
        return false;
    }
    return true;
}

bool
gf_simd_cauchy::encode(uint32_t multiplier, bool xor_flag)
{
    //std::cout << "simd_cauchy::encode() called " << std::endl;

    m_gf.multiply_region.w32(&m_gf, get_src(), get_dst(),
      multiplier, (int)get_size(), xor_flag);
#if 0
    h_encode_altmap(get_src(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
    h_gf16_split_4_16_lazy_sse_altmap_multiply_region(get_buffer(),
      get_dst(), (int)get_size(),
      multiplier, 0 /* XOR */, get_polynomial());
#endif
    return true;
}

bool
gf_simd_cauchy::decode(uint32_t multiplier)
{
    std::cout << "simd_cauchy::decode() called " << std::endl;
    return true;
}

// expand_roots
//    do expansion of roots.
//    V(x) = x(x-x1)(x-x2)...(x-xn) = vn + x(n-1)x^(n-1) + ... v(0)
//
int
expand_roots(galois16_t f[], galois16_t df[], VectorInt16 &erased_pt, 
             int num_erased) 
{
    // printf("expanded roots\n");
    for (int k=0; k < num_erased; k++) {
        galois16_t pt = erased_pt[k];
        galois16_t pt2 = erased_pt[k];
        //printf("    pt: %hx: f[k] %hx\n", pt.value(), f[k].value()); 
        for (int j=k+1; j >= 1; j--) {
            f[j] = pt*f[j] + f[j-1];
            //printf("V[j] %d:= ", j);
            for (int h=0; h< num_erased; h++) {
                //printf("    %hx", f[h].value()); 
            }
            //printf("\n");
        }
        f[0] = pt2 * f[0];
        //printf(" f[0] %hx pt=%hx, pt2=%hx\n", f[0].value(), pt.value(), pt2.value());
    }
    //printf("\n");
    for (int k=0; k < num_erased; k++) {
         //printf("      f[%d] = %hx", k, f[k].value());
    }
    //printf("\n");
    
    // pick only the add parmeters for derivative
    for (int k=0; k < (num_erased+1)/2; k++) {
        df[k] = f[k*2+1];
    }
    return 0;
}

galois16_t
evaluate_poly(galois16_t v[], galois16_t dv[], uint16_t erased_pt,
              int deg)
{
    galois16_t poly;
    galois16_t horner = 1;
    galois16_t pt = erased_pt;

    for (int i=deg-1; i >= 0; i--) {
        galois16_t c = v[i] + horner * pt;
        horner = c;
    }
    poly = horner;
    return poly;
}
 
galois16_t
evaluate_derivative(galois16_t v[], uint16_t erased_pt, int deg)
{
    galois16_t pt    = erased_pt;
    galois16_t valsq = pt.square();
    galois16_t ans = v[deg-1];

    //printf("\tn,v[n],pt,ans    :    = x%hx x%hx x%hx\n", deg, erased_pt, ans.value()); 
    for (int i=deg-2; i>=0; i--) {
        ans = (ans * valsq) + v[i];
    }
    return ans;
}

void
arrout(std::string msg, MatrixInt16 &data, int item, int size)
{
    cout << msg;
    cout << "[ ";
    for (int i=0; i<size; i++)
        cout << " x" << hex << data[item][i];
    cout << "]" << endl;
}



