//
// matrix_test3.cc
//

#include <stdio.h>
#include <stdlib.h>

#include "h2_gf_erasure.h"

#define DSIZE    4096
#define SEC_SIZE 16
#define NSEC     30
#define PSEC     20

using namespace h_erasure;
using namespace std;

// define  GF(2^16) with flat arithmatic
// min poly = x^16+x^12+x^3+1
typedef galois_base<int, 1 << 16, (1<<16)+(1<<12)+(1<<3)+(1<<1)+1, 1<<15> galois16_t;


galois16_t power(galois16_t base, uint16_t exp) {
    galois16_t ans = 1;
    galois16_t mult = base;
    //printf("power: base=x%hx, exp=x%hx\n", base.value(), exp);
    while (exp > 0) {
        if (exp & 1)
            ans *= mult;
        mult *= mult;
        exp >>= 1;
        //printf("power: loop mult=x%hx, exp=x%hx\n", mult.value(), exp);
    }
    return ans;
}

int
main(int argc, char *argv[])
{
    MatrixInt16   matrix_a;
    MatrixInt16   matrix_b;
    MatrixInt16   matrix_cauchy;
    class gf_simd_table table1;
    int     i, j, cnt;
    uint32_t multiplier = 0x89ad;
    uint16_t *a, *b;

#define OUT_VEC_LENGTH 5
#define OUT_VEC_NUM_A  20
#define OUT_VEC_NUM_B  100

    i = posix_memalign((void **)&a, 0x10, DSIZE);
    i = posix_memalign((void **)&b, 0x10, DSIZE);

    table1.init(16);
    table1.allocate_buffer(DSIZE);
    table1.set_src(0x4);
    table1.print_src();
    // do GF16 multiplication with specified multiplier with the src buffer
    table1.encode(multiplier);
    table1.print_dst();


    a[0] = 0x0001;
    a[1] = 0x8000;
    a[2] = 0x4000;
    a[3] = 0x2000;
    a[4] = 0x1001;
    a[5] = 0x0800;
    a[6] = 0x0400;
    a[7] = 0x0200;
    a[8] = 0x0100;
    a[9] = 0x0080;
    a[10] = 0x0040;
    a[11] = 0x0020;
    a[12] = 0x0010;
    a[13] = 0x0009;
    a[14] = 0x0004;
    a[15] = 0x0003;

    // initialize Generator Matrix B
    b[0] = 0xb0b1;
    b[1] = 0x5858;
    b[2] = 0x2c2c;
    b[3] = 0x9616;
    b[4] = 0xfbba;
    b[5] = 0xfddd;
    b[6] = 0xfeee;
    b[7] = 0xff77;
    b[8] = 0xffbb;
    b[9] = 0x7fdd;
    b[10] = 0xbfee;
    b[11] = 0xdff7;
    b[12] = 0xeffb;
    b[13] = 0x474c;
    b[14] = 0xa3a6;
    b[15] = 0x6162;


    table1.create_generator(a, 16,
                            matrix_a, OUT_VEC_LENGTH /* output vector_length */,
                            OUT_VEC_NUM_A /* output num_vectors */);
    table1.add_unary_vector(matrix_a, 0);
    for (j=0; j<OUT_VEC_NUM_A; j++) {
        printf("Output Generator Matrix A: Vector %d:", j);
        for (i=0; i<OUT_VEC_LENGTH; i++) {
            printf("x%4.4x ", matrix_a[j][i]);
        }
        printf("\n");
    }
    printf("\n");

    table1.create_generator(b, 16,
                            matrix_b, OUT_VEC_LENGTH /* vector_length */,
                            OUT_VEC_NUM_B /* num_vectors */);
    for (j=0; j<OUT_VEC_NUM_B; j++) {
        printf("Output Generator Matrix B: Vector %d:", j);
        for (i=0; i<OUT_VEC_LENGTH; i++) {
            printf("x%4.4x ", matrix_b[j][i]);
        }
        printf("\n");
    }
    printf("\n");

    table1.create_cauchy_encoder(matrix_a, matrix_b, matrix_cauchy);
    for (j=0; j<OUT_VEC_NUM_B; j++) {
        printf("Output Matrix Cauchy: Vector %d:", j);
        for (i=0; i<OUT_VEC_LENGTH; i++) {
            printf("x%4.4x ", matrix_cauchy[j][i]);
        }
        printf("\n");
    }

    cnt = 0;
    table1.start_timer();
    for (i=0; i<OUT_VEC_NUM_B; i++) {
        for (j=0; j<OUT_VEC_LENGTH; j++) {
//            printf("%d.%d, doing encode with x%4.4x \n", j, i, matrix_cauchy[j][i]);
            cnt++;
            table1.encode(matrix_cauchy[j][i]);
        }
    }
    table1.stop_timer();
    table1.print_timer_diff_usecs(cnt*DSIZE);
    table1.free_buffer();
    free(a);
    free(b);
}
