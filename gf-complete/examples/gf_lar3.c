/*
 * GF-Complete: A Comprehensive Open Source Library for Galois Field Arithmetic
 * James S. Plank, Ethan L. Miller, Kevin M. Greenan,
 * Benjamin A. Arnold, John A. Burnum, Adam W. Disney, Allen C. McBride.
 *
 * gf_example_5.c
 *
 * Demonstrating altmap and extract_word
 */

#include <stdio.h>
#include <getopt.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "gf_complete.h"
#include "gf_rand.h"

#define SECTOR_SIZE 4096
#define DSIZE       4096

uint16_t *l_init_data_sector(int size, uint16_t initial);

void usage(char *s)
{
    fprintf(stderr, "usage: gf_example_5\n");
    exit(1);
}

uint16_t *
l_init_data_sector(int size, uint16_t initial)
{
    uint16_t *dp, *rp;
    int      i;

    dp = rp = (uint16_t *)malloc(size);
    if (dp == NULL)
        return NULL;
    for (i=0; i < size; i += sizeof(uint16_t))
        *rp++ = initial;
    return dp;
}

int main(int argc, char **argv)
{
    uint16_t *d0, *d1, *d2, *d3, *d4, *d5, *d6, *d7, *d8, *d9;
    uint16_t *a, *b, *g;
    int i, j;
    gf_t gf;

    if (gf_init_hard(&gf, 16, GF_MULT_LOG_TABLE, GF_REGION_CAUCHY, GF_DIVIDE_DEFAULT, 
                   0, 0, 0, NULL, NULL) == 0) {
        fprintf(stderr, "gf_init_hard failed: x%x:", _gf_errno);
        gf_error();
        exit(1);
    }
#if 0
     if (gf_init_hard(&gf, 16, GF_MULT_SPLIT_TABLE, GF_REGION_DEFAULT, GF_DIVIDE_DEFAULT, 
                   0, 16, 4, NULL, NULL) == 0) {
        fprintf(stderr, "gf_init_hard failed\n");
        exit(1);
    }
#endif

    //a = (uint16_t *) malloc(200);
    i = posix_memalign((void **)&a, 0x10, DSIZE);
    //for (i = 0; i < 32; i++) a[i] = 0x4;
    bzero((void *)a, DSIZE);
    a[0] = 0x640f;
    a[1] = 0x07e5;
    a[2] = 0x2fba;
    a[3] = 0xcedf;
    a[4] = 0xf1f9;
    a[5] = 0x3ab8;
    a[6] = 0xcd18;
    a[7] = 0x1d97;
    a[8] = 0x45a7;
    a[9] = 0x0160;
    a[10] = 0x0160;
    a[11] = 0x0160;
    a[12] = 0x0160;
    a[13] = 0x0160;
    a[14] = 0x0160;
    a[15] = 0x0160;
    //b = (uint16_t *) malloc(200);
    i = posix_memalign((void **)&b, 0x10, DSIZE);

    //a += 6;
    //b += 6;

    MOA_Seed(0);

  gf.multiply_region.w32(&gf, a, b, 0x1234, 16*4*2, 0);
  //gf.multiply_region.w32(&gf, a, b, 0x89ad, 32*2, 0);
  //gf.multiply_region.w32(&gf, a, b, 0x4000, 32*2, 0);

  printf("multiplier: 0x%x a: 0x%lx    b: 0x%lx\n", (unsigned long) 0x1234, (unsigned long) a, (unsigned long) b);

  for (i = 0; i < 30; i += 10) {
    printf("\n");
    printf("  ");
    for (j = 0; j < 10; j++) printf(" %4d", i+j);
    printf("\n");

    printf("a:");
    for (j = 0; j < 10; j++) printf(" %04x", a[i+j]);
    printf("\n");

    printf("b:");
    for (j = 0; j < 10; j++) printf(" %04x", b[i+j]);
    printf("\n");
    printf("\n");
  }

  for (i = 0; i < 15; i ++) {
    printf("Word %2d: 0x%04x * 0x1234 = 0x%04x    ", i,
           gf.extract_word.w32(&gf, a, 16*4*2, i),
           gf.extract_word.w32(&gf, b, 16*4*2, i));
    printf("Word %2d: 0x%04x * 0x1234 = 0x%04x\n", i+15,
           gf.extract_word.w32(&gf, a, 16*4*2, i+15),
           gf.extract_word.w32(&gf, b, 16*4*2, i+15));
  }
  return 0;
}
