//
// matrix_test4.cc
//    Cauchy Encoder example
//

#include <stdio.h>
#include <stdlib.h>

#include "h2_gf_erasure.h"

#define DSIZE    4096
#define SEC_SIZE 32
#define SEC_16ELEM SEC_SIZE/2
#define NSEC     30
#define PSEC     20

using namespace h_erasure;
using namespace std;

// define  GF(2^16) with flat arithmatic
// min poly = x^16+x^12+x^3+1


galois16_t power(galois16_t base, uint16_t exp) {
    galois16_t ans = 1;
    galois16_t mult = base;
    //printf("power: base=x%hx, exp=x%hx\n", base.value(), exp);
    while (exp > 0) {
        if (exp & 1)
            ans *= mult;
        mult *= mult;
        exp >>= 1;
        //printf("power: loop mult=x%hx, exp=x%hx\n", mult.value(), exp);
    }
    return ans;
}

int
main(int argc, char *argv[])
{
    class gf_simd_table table1;
    int     i, j;
    uint16_t *a, *b, *d2_p, d3;
    int             g2_a[NSEC], g2_b[PSEC];
    MatrixInt16     g_cauchy, parity_p;
    int             nsec = NSEC, psec = PSEC;
    bool            bool1 = false;
    uint8_t         *sector_p[NSEC];

#define OUT_VEC_LENGTH 5
#define OUT_VEC_NUM_A  20
#define OUT_VEC_NUM_B  100

    i = posix_memalign((void **)&a, 0x10, DSIZE);
    i = posix_memalign((void **)&b, 0x10, DSIZE);

    table1.init(16);
    table1.allocate_buffer(DSIZE);
    table1.set_src(0x4);
    //table1.print_src();
    // do GF16 multiplication with specified multiplier with the src buffer
    //table1.print_dst();
    table1.create_cauchy_matrix(g_cauchy, nsec, psec, bool1, g2_a /*dummy*/, g2_b /*dummy*/, bool1);

    for (i=0; i<nsec; i++) {
        sector_p[i] = (uint8_t *)malloc(DSIZE);
        table1.fill_data_sector(sector_p[i], i, SEC_SIZE, 0x69);
    }
    parity_p.resize(psec);
    for (i=0; i<psec; i++) {
       parity_p[i].resize(nsec);
    }
    for (i=0; i<nsec; i++) {
        printf("Data Sector %d:\n", i);
        d2_p = (uint16_t *)sector_p[i];
        for (j=0; j< SEC_16ELEM; j++) {
            printf(" x%hx", *d2_p++); 
        }
        printf("\n");
    }
    for (i=0; i<PSEC; i++) {
        printf("Cauchy row %d\n", i);
        for (j=0; j<NSEC; j++) {
            printf(" x%hx", g_cauchy[i][j]);
        }
        printf("\n");
    }

    for (i=0; i<nsec; i++) {
        table1.cauchy_encode(sector_p[i], g_cauchy, parity_p, i, psec, SEC_SIZE, false, false);
    }
    for (i=0; i<psec; i++) {
        printf("Parity Sector %d:\n", i);
        for (j=0; j< SEC_16ELEM; j++) {
            d3= (uint16_t) parity_p[i][j];
             printf(" x%hx", d3); 
        }
        printf("\n");
    }


    table1.free_buffer();
    for (i=0; i<nsec; i++) {
        if  (sector_p[i] != NULL)
            free(sector_p[i]);
    }
    free(a);
    free(b);
}
