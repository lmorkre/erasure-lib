//
//

#ifndef _H_GF_MATRIX
#define _H_GF_MATRIX

#include <stdio.h>
#include <getopt.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include <stdint.h>

#ifdef INTEL_SSE4
  #include <nmmintrin.h>
#endif

#ifdef INTEL_SSSE3
  #include <tmmintrin.h>
#endif

#ifdef INTEL_SSE2
  #include <emmintrin.h>
#endif

#ifdef INTEL_SSE4_PCLMUL
  #include <wmmintrin.h>
#endif


typedef std::vector<vector<uint16_t> >   MatrixInt16;

extern uint16_t    h_mult_matrix_vector_i128(uint16_t *in_array, uint16_t mult1);
extern uint16_t    h_mult_matrix_by_hand(uint16_t *in_array, uint16_t mult1, int size);
extern int         h_matrix_translate_column(void *column_data, void *dst_row, int items);
extern int         h_matrix_translate_row(void *row_data, void *dst_column, int items);
extern int         h_perf_start(struct timeval *tv);
extern int         h_perf_stop(struct timeval *tv);
extern void        h_perf_print(struct timeval *stop, struct timeval *start, long long dsize);
extern void        h_print_i128(__m128i *in1);
extern void        h_perf_print2(struct timeval *stop, struct timeval *start, long long count);


extern void        h_encode_altmap(void *src, void *dest, int bytes,
                        uint32_t val, int xxor, int pp);
extern bool        h_gf_create_generator_matrix(uint16_t *binary_row, int size,
                             MatrixInt16 &dst_matrix, int vector_length, int num_vectors);
#endif
