/**
 *  h2_gf_erasure.h
 * @defgroup h2_gf_encode GF-complete encoding  API
 * @ingroup h_erasure
 */

#ifndef _H_GF_ERASURE
#define _H_GF_ERASURE

#include <stdint.h>
#include <sys/time.h>
#include <assert.h>

#include <vector>

extern "C" {
#include "gf_complete.h"
}

using namespace std;

typedef std::vector<vector<uint16_t> >   MatrixInt16;
typedef std::vector<uint16_t>            VectorInt16;

void arrout(std::string msg, MatrixInt16 &data, int item, int size);

/**
 * @ingroup h_erasure
 *
 * @brief HGST GF-complete namespace
 */
namespace h_erasure {

/**
 * @brief pure virtual encoding op's
 *
 * @see h_erasure::encode_buffer
 * @ingroup h_erasure
 */
class encode_op {
  public:
    virtual bool init(uint32_t w) = 0;
    virtual bool encode(uint32_t val, bool xor_flag) = 0;
    virtual bool decode(uint32_t val) = 0;
};

/**
 * @class encode_buffer
 *
 * @brief main encoding class, subclass for each type of encoding class
 *
 * @ingroup h_erasure
 */
class encode_buffer : public encode_op {
  public:
    /** 
     * @brief encode_buffer constructor
     *  Set up defaults
     */
    encode_buffer() {
        m_alignment = 0x20;
        m_polynomial = 0x1100b; // for GF(2^16)
        m_weight = 0;
    }
    typedef enum {
        ENCODE_OP = 0,
        DECODE_OP,
    } encode_operation_t;
    typedef enum {
        SHIFT_MULTI = 0,
        SIMD_TABLE,
        SIMD_ALTMAP
    } encode_type_t;

    /** 
     * @brief allocate buffers for both source and destination for data encoding
     *
     * @param size
     */
    void *allocate_buffer(uint64_t size);
    void *allocate_src(uint64_t size);
    void *allocate_dst(uint64_t size);
    /**
     * @brief free buffers (src and dst)
     */
    void free_buffer();
    /**
     * @brief set byte alignment for the allocation buffers
     *        needs to be called before allocate_buffer()
     *
     * @param byte_size - size of alignment  in bytes
     */
    void set_alignment(uint32_t byte_size);
    /** 
     * @brief fill the source buffer to specified value
     *
     * @param val - value to fill into source buffer
     */
    bool set_src(uint16_t val);
    void print_src();
    void print_dst();
    /**
     * @brief start the gettimeofday() timer
     */
    void start_timer()   { gettimeofday(&m_starttime, 0); }
    /** 
     * @brief stop the gettimeofday() timer
     */
    void stop_timer()    { gettimeofday(&m_stoptime, 0); }
    void print_timer_diff_usecs();
    /**
     * @brief  print out the timer difference numbers based on size in bytes
     *         print out MB/s
     *
     * @param size - size in bytes
     */
    void print_timer_diff_usecs(uint64_t size);

    bool set_encode_type();
    /**
     * @brief return the memory pointer of source buffer
     */
    void *get_src()     { return m_src; }
    /**
     * @brief return the memory pointer for the destiation buffer
     */
    void *get_dst()     { return m_dst;   }
    /** 
     * @brief switch source and destination buffers
     */
    void switch_src_dst() { void *temp = m_dst; m_dst = m_src; m_src = temp; } 
    /**
     * @brief return the size of memory buffer(s)
     */
    uint64_t get_size()   { return m_size;  }
    void     set_size(uint64_t sz) { m_size = sz;    }
    /**
     * @brief return the hex representation of the polynomial
     */
    uint32_t get_polynomial() { return m_polynomial; }
    /**
     * @brief full the data pointer with some pre-determined data which
     *        contains a pattern and the sector number
     */
    bool fill_data_sector(uint8_t *data_p, int sector_num,
                                      int sector_size,   int random_fill);
    /**
     * @brief create a generator Matrix
     */
    void create_generator(uint16_t *binary_rows, int binary_size,
                          MatrixInt16 &dst_matrix, int vector_length,
                          int num_vectors);
    bool add_unary_vector(MatrixInt16 &data_matrix, int row);
    bool create_cauchy_encoder( MatrixInt16 &row_matrix, MatrixInt16 &col_matrix,
                             MatrixInt16 &out_cauchy_matrix);
    int  translate_matrix_column(void *col_data, void *dst_row, int items);
    int  translate_matrix_row(void *row_data, void *dst_column, int items);
    bool gf_create_generator_matrix(uint16_t *binary_rows,
                             int binary_size, MatrixInt16 &dst_matrix,
                             int vector_length, int num_vectors);
    uint32_t multiply_matrix(uint16_t *in_array, uint16_t mult1, int size);
    /**
     * creates and returns Cauchy Matrix based on data error and parity error location information
     *
     * @param out_cauchy_matrix - output MatrixInt16 of the Cauchy Matrix
     *                            this is resized based on input parameters
     * @param num_data          - number of data elements
     * @param num_parity        - number of parity elements
     * @param is_decode         - is this for decoding, if so use error location params
     * @param err_loc_arg       - int array containing data error locations: 1=error,0=good
     * @param parity_loc_arg    - int array containing parity error locations: 1=error,0=good  
     */
    void  create_cauchy_matrix( MatrixInt16 &out_cauchy_matrix,
                             int num_data, int num_parity, bool is_decode,
                             int err_loc_arg[], int parity_loc_arg[]);
    /**
     * calculate Parity Matrix from data and Cauchy Matrix
     *
     * @param sector_p      - pointer to data sector
     * @param cauchy_matrix - MatrixInt16 of Cauchy Matrix
     * @param out_parity_p  - output MatrixInt16 of the parity 
     *                        not reallocated. Should be pre-allocated and
     *                        could have existing data
     * @param sector_num    - data sector number
     * @param num_parity    - number of parity elements
     * @param num_symbols   - number of data elements
     * @param bad_sector    - indicate whether this data sector is a bad/missing one
     * @param is_parity     - indicate whether this is for a parity calculation.
     *                        Do not use Cauchy Matrix in calculation
     */
    bool cauchy_encode_parity(uint8_t *sector_p, MatrixInt16 &cauchy_matrix,
                             MatrixInt16 &out_parity_p, int sector_num, int num_parity, int sector_size,
                             bool bad_sector, bool is_parity);
    /**
     * calculate Parity Matrix from data and Cauchy Matrix. Output parity data buffer.
     *
     * @param sector_p      - pointer to data sector
     * @param cauchy_matrix - MatrixInt16 of Cauchy Matrix
     * @param out_parity_p  - output uint16_t of the parity information. 
     *                        Should be pre-initialized and have existing data
     * @param sector_num    - data sector number
     * @param num_parity    - number of parity elements
     * @param num_symbols   - number of data elements
     * @param bad_sector    - indicate whether this data sector is a bad/missing one
     * @param is_parity     - indicate whether this is for a parity calculation.
     *                        Do not use Cauchy Matrix in calculation
     */
    bool cauchy_encode_parity(uint8_t *sector_p, MatrixInt16 &cauchy_matrix,
                             uint16_t *out_parity_p[], int sector_num, int num_parity, int sector_size,
                             bool bad_sector, bool is_parity);
    bool cauchy_encode_syndrome(MatrixInt16 &parity_p, MatrixInt16 &calc_p, MatrixInt16 &syndrome_p, 
                                    int sector_num, int num_symbols, bool bad_sector);
    bool cauchy_encode_syndrome(uint16_t *parity_p[], MatrixInt16 &calc_p, MatrixInt16 &syndrome_p, 
                                    int sector_num, int num_symbols, bool bad_sector);
    bool cauchy_encode_syndrome(uint16_t *parity_p[], uint16_t *calc_p[], MatrixInt16 &syndrome_p, 
                                    int sector_num, int num_symbols, bool bad_sector);
    void cauchy_dmatrix(MatrixInt16 &out_cauchy, VectorInt16 &out_a,
                                    VectorInt16 &out_bi, int num_data, int num_parity,
                                    int num_roots, int max_parity_num, bool is_decode, 
                                    int erased_arg[], int parity_arg[], bool display);
    void cauchy_dinvert(MatrixInt16 &g_cauchy, VectorInt16 &g_a, VectorInt16 &g_bi, 
                                    int num_erased, int num_parity, bool display);
    
  protected:
    uint32_t   m_weight;
    gf_t       m_gf;

  private:
    uint32_t   m_tag;  // unique id for each encode if needed
    void      *m_src;
    void      *m_dst;
    uint32_t   m_alignment;
    uint64_t   m_size;
    uint64_t   m_src_size;
    uint64_t   m_dst_size;
    uint32_t   m_encode_type;
    uint32_t   m_polynomial;
    struct     timeval m_starttime;
    struct     timeval m_stoptime;
};

class gf_shift_multi : public encode_buffer {
  public:
    bool init(uint32_t w);
    bool encode(uint32_t val, bool xor_flag);
    bool decode(uint32_t val);
};

class gf_simd_table : public encode_buffer {
  public:
    bool init(uint32_t w);
    bool encode(uint32_t val, bool xor_flag);
    bool decode(uint32_t val);
    bool multiply_region(void *src, void *dst, uint32_t multiplier, uint32_t size, bool xor_flag);
};

class gf_simd_altmap : public encode_buffer {
  public:
    bool init(uint32_t w);
    bool encode(uint32_t val, bool xor_flag);
    bool decode(uint32_t val);
    bool multiply_region(void *src, void *dst, uint32_t multiplier, uint32_t size, bool xor_flag);
}; 

class gf_simd_cauchy : public encode_buffer {
  public:
    bool init(uint32_t w);
    bool encode(uint32_t val, bool xor_flag);
    bool decode(uint32_t val);
}; 

// Galois base template class.
// based on tarith.h
//
// size if the field size
// poly respresents the min poly of the prim elt
//

template<class data, int size, int poly, int trace_1>
class galois_base {
  public:
    galois_base(data intval) : m_data(intval) {
        assert(intval < size);
    }
    galois_base() : m_data(0) {};
    int   is_zero() const { return (m_data == 0); }
    int   is_one()  const { return (m_data == 1); }
    static galois_base prime_elem() {
        return galois_base(2);
    }
    inline int operator == (const data &y) {
        return (m_data == y);
    }
    inline int operator == (const galois_base &y) {
        return (m_data == y.value());
    }
    inline galois_base operator / (const galois_base &y) {
        return m_data * y.invert();
    }
    inline galois_base &operator += (galois_base x) {
        m_data = m_data ^ x.value();
        return *this;
    }
    galois_base &operator *= (const galois_base x) {
        *this = prod(x);
        return *this;
    }
    galois_base invert() const {
        assert(!is_zero());
        return (power(size-2));
    }
    galois_base square() const {
        return (*this) * (*this);
    }
    galois_base alphatimes() const {
        data sbits = m_data << 1;
        if (sbits & size) {
            //printf("\t\talphatimes: RESET size=x%x if=%d sbits=x%x\n", size, (sbits & size), sbits);
            sbits ^= poly;
        }
        //printf("\t\talphatimes: return poly=x%x size=x%x if=%d sbits=x%x\n", poly, size, (sbits & size), sbits);
        return galois_base(sbits);
    }
    galois_base prod(galois_base x) {
        data ans = 0;
        data val = m_data;
        //printf("\t>prod: val=x%x x=x%x\n", m_data, x.value());
        while (val != 0) {
            if (val & 1)
                ans ^= x.value();
            val >>= 1;
            x = x.alphatimes();
            //printf("\t>prod: loop val=x%x ans=x%x, x=x%x\n", val, ans, x.value());
        }
        //printf("\t<prod: return ans=x%x\n", ans);
        return galois_base(ans);
    }
    galois_base power(int n) const {
        if (n < 0)
            return invert().power(-n);
        galois_base res = 1;
        galois_base sq = *this;
        while (n > 0) {
            if (n & 1)
                res *= sq;
            sq *= sq;
            n >>= 1;
        }
        return res;
    }
    galois_base power(galois_base x, int n) const {
        //printf("power2: x=x%hx n=x%x\n", x.value(), n);
        if (n < 0) return power(x.invert(), -n);
        galois_base res = 1;
        galois_base sq  = x;
        while (n > 0) {
            if (n & 1) res *= sq;
            sq *= sq;
            n >>= 1;
        }
        return res;
    }
    data value() { return m_data; }

  private:
    data    m_data;
};

typedef galois_base<int, 1 << 16, (1<<16)+(1<<12)+(1<<3)+(1<<1)+1, 1<<15> galois16_t;

// derived from tarith.h
// in the following operators, galois represents any galois class
template<class galois>
inline int operator== (const galois& x, const galois& y) {
    return (x.value() == y.value());
}

template<class galois>
inline int operator== (const galois& x, const int y) {
    return (x.value() == y);
}

template<class galois>
inline int operator!= (const galois& x, const galois& y) {
    return (x.value() != y.value());
}

template<class galois>
inline int operator!= (const galois& x, const int y) {
    return (x.value() != y);
}

template<class galois>
inline galois operator+ (galois x, galois y) {
    //printf("\t + of %hx, %hx\n", x.value(), y.value());
    return galois(x.value() ^ y.value());
}

template <class galois>
inline galois operator* (const galois x, const galois y) {
        galois ans = x;
        ans *= y;
        return ans;
}

template <class galois>
inline galois operator* (const galois x, const uint16_t y) {
        galois ans = y;
        ans *= x;
        return ans;
}

template<class galois>
inline galois operator/ (const galois& x, const galois& y) {
    return x * y.invert();
}

// prints out contents of array argument
template<class Field>
void arrout(Field* arr, int size) {
  cout << "[ " ;
  for (int i=0; i<size; i++) cout << arr[i] << " ";
  cout << "]" << endl;
}

template<class Field>
void arrout(std::string message, Field* arr, int size) {
  cout << message;
  arrout(arr, size);
}

template<class Field>
void arrout(Field* arr, int nsec, int size) {
  cout << "[ " ;
  for (int i=0; i<size; i++) cout << arr[nsec][i] << " ";
  cout << "]" << endl;
}

template<class Field>
void arrout(std::string message, Field* arr, int nsec, int size) {
  cout << message << " " << nsec  << " ";
  arrout(arr, nsec,size);
}

template<class Field>
void arrout(Field* arr, int nsec, int top, int size) {
  cout << "[ " ;
  for (int i=top; i<size; i++) cout << arr[nsec][i] << " ";
  cout << "]" << endl;
}

template<class Field>
void arrout(std::string message, Field* arr, int nsec, int top, int size) {
  cout << message << " " << nsec  << " ";
  arrout(arr, nsec, top, size);
}
};

#endif
