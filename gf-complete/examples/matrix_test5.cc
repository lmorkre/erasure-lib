//
// matrix_test5.cc
//    Cauchy Encoder and Decoder example
//

#include <stdio.h>
#include <stdlib.h>

#include <iostream>

#include "h2_gf_erasure.h"

#define DSIZE    4096
#define SEC_SIZE 46
#define SEC_16ELEM SEC_SIZE/2
#define NSEC     30
#define PSEC     20

using namespace h_erasure;
using namespace std;

// define  GF(2^16) with flat arithmatic
// min poly = x^16+x^12+x^3+1


galois16_t power(galois16_t base, uint16_t exp) {
    galois16_t ans = 1;
    galois16_t mult = base;
    while (exp > 0) {
        if (exp & 1)
            ans *= mult;
        mult *= mult;
        exp >>= 1;
    }
    return ans;
}

int
main(int argc, char *argv[])
{
    class gf_simd_table table1;
    int     i, j;
    uint16_t *a, *b, *d2_p;
    int             g2_a[NSEC], g2_b[PSEC], max_parity, g_roots, g_parity;
    int             disk_map[NSEC], parity_map[PSEC];
    int             erased_loc[NSEC], parity_loc[PSEC];
    MatrixInt16     g_cauchy;
    MatrixInt16     parity_p;
    MatrixInt16     calc_p;
    MatrixInt16     syndrome_p;
    MatrixInt16     recovered_p;
    VectorInt16     g_a, g_bi;
    int             nsec = NSEC, psec = PSEC;
    bool            bool1 = false;
    uint8_t         *sector_p[NSEC];   // data sector

#define OUT_VEC_LENGTH 5
#define OUT_VEC_NUM_A  20
#define OUT_VEC_NUM_B  100
    g_roots = 0;
    g_parity = 0;

    i = posix_memalign((void **)&a, 0x10, DSIZE);
    i = posix_memalign((void **)&b, 0x10, DSIZE);

    table1.init(16);
    table1.allocate_buffer(DSIZE);
    table1.set_src(0x4);
    //table1.print_src();
    // do GF16 multiplication with specified multiplier with the src buffer
    //table1.print_dst();
    table1.create_cauchy_matrix(g_cauchy, nsec, psec, bool1, g2_a, g2_b, bool1);

    for (i=0; i<nsec; i++) {
        sector_p[i] = (uint8_t *)malloc(DSIZE);
        table1.fill_data_sector(sector_p[i], i*4, SEC_SIZE, 0xab);
    }
    parity_p.resize(psec);
    for (i=0; i<psec; i++) {
       parity_p[i].resize(nsec);
    }
    calc_p.resize(psec);
    for (i=0; i<psec; i++) {
       calc_p[i].resize(nsec);
    }
    syndrome_p.resize(psec);
    for (i=0; i<psec; i++) {
       syndrome_p[i].resize(nsec);
    }
    recovered_p.resize(psec);
    for (i=0; i<psec; i++) {
        recovered_p[i].resize(nsec);
    }
    for (i=0; i<nsec; i++) {
        printf("Data Sector %d:\n", i);
        d2_p = (uint16_t *)sector_p[i];
        for (j=0; j< SEC_16ELEM; j++) {
            printf(" x%hx", *d2_p++); 
        }
        printf("\n");
    }
    for (i=0; i<PSEC; i++) {
        arrout("Cauchy row = ", g_cauchy, i, NSEC);
    }

    for (i=0; i<nsec; i++) {
        table1.cauchy_encode_parity(sector_p[i], g_cauchy, parity_p, i, psec, SEC_16ELEM, false, false);
    }
    for (i=0; i<psec; i++) {
        arrout("Parity Sector = ", parity_p, i, SEC_16ELEM);
    }

#if 0
    // zero out Parity for decoding
    for (i=0; i<psec; i++) {
        for (j=0; j< SEC_16ELEM; j++) {
            parity_p[i][j] = 0;
        }
    }
#endif

    //==========================================================================
    // Start Decoding Test
    cout << "+++++++++++++++++++++++++ << Decoding Test >> ++++++++++++++++++++++" << endl;
    cout << " o Mark Erased Data Sectors " << endl;
    cout << " o Mark Erased Parity Sectors" << endl;
    // Data Skip Mask: set to 1 for bad data locations
    // For test, mark disk 23-26 as bad
    disk_map[0]=0; disk_map[1]=0; disk_map[2]=0; disk_map[3]=0; disk_map[4]=0;
    disk_map[5]=0; disk_map[6]=0; disk_map[7]=0; disk_map[8]=0; disk_map[9]=0;
    disk_map[10]=0; disk_map[11]=0; disk_map[12]=0; disk_map[13]=0; disk_map[14]=0;
    disk_map[15]=0; disk_map[16]=0; disk_map[17]=0; disk_map[18]=0; disk_map[19]=0;
    disk_map[20]=0; disk_map[21]=0; disk_map[22]=0; disk_map[23]=1; disk_map[24]=1;
    disk_map[25]=1; disk_map[26]=1; disk_map[27]=0; disk_map[28]=0; disk_map[29]=0;
    // Parity Skip Mask
    // Not used at this point. All parity data is good
    parity_map[0]=0; parity_map[1]=0; parity_map[2]=0; parity_map[3]=0; parity_map[4]=0;
    parity_map[5]=0; parity_map[6]=0; parity_map[7]=0; parity_map[8]=0; parity_map[9]=0;
    parity_map[10]=0; parity_map[11]=0; parity_map[12]=0; parity_map[13]=0; parity_map[14]=0;
    parity_map[15]=0; parity_map[16]=0; parity_map[17]=0; parity_map[18]=0; parity_map[19]=0;
    // erased_loc - array to remember bad data sectors
    g_roots = 0;
    for (j=0; j<nsec; j++) {
        if (disk_map[j] == 1) {
            erased_loc[g_roots] = j;
            g_roots++;
        }
    }
    // parity_loc - array to remember bad parity data 
    g_parity = 0;
    for (j=0; j<psec; j++) {
        if (parity_map[j] != 1) {
            parity_loc[g_parity] = j;
            g_parity++;
        }
    }
    arrout("diskipm[] = ", disk_map, nsec);
    arrout("pskipm[] = ", parity_map, psec);
    cout << "g_roots   : " << g_roots << endl;
    cout << "g_parity  : " <<  g_parity << endl;
    arrout("eras_loc[] = ", erased_loc, g_roots);
    arrout("prty_loc[] = ", parity_loc, g_parity);
    if (g_roots <= g_parity) {
       max_parity = g_roots;
    } else {
        cout << "ERROR: More bad data than parity sectors" << endl;
        exit(1);
    }
    cout << "++++++++++++++++++++++++++ Start Decoding +++++++++++++++++++++++" << endl;
    cout << " 1) Syndrome Generation: " << endl;
    cout << "   o Read all data back except the erased (bad) data sectors     " << endl;
    cout << "   o Syndrome = Good Data Sectors * Dmatrix + Parity Sectors     " << endl;
    cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    // Read in good data sectors
    //    This is determined by disk_map parameter
    //    ?? calc_p contains the temporary decode parity for the calculation
    for (i=0; i<nsec; i++) {
        table1.cauchy_encode_parity(sector_p[i], g_cauchy, calc_p, i, psec, SEC_16ELEM, disk_map[i], false); 
    }
    // Read in parity sectors, calculate into symdrome_p
    for (i=0; i<psec; i++) {
        table1.cauchy_encode_syndrome(parity_p, calc_p, syndrome_p, i, SEC_16ELEM, false); 
    }
    cout << "+++++++++++++++++++++++++ Syndrome Sectors +++++++++++++++++++++++" << endl;
    for (i=0; i<psec; i++) {
        arrout("Syndrome Sector = ", syndrome_p, i, SEC_16ELEM);
    }
    cout << "++++++++++++++++++++++++ DMatrix +++++++++++++++++++++++++++++++++" << endl;
    cout << " 2) Find Dmatrix D[i,j] i=good parity, j=erased sectors           " << endl;
    cout << " 3) Invert DMatirix                                               " << endl;
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    // DMatrix - recreate Cauchy using recovery data and parity sectors
    table1.cauchy_dmatrix(g_cauchy, g_a, g_bi, nsec, psec, g_roots, max_parity, true, erased_loc, parity_loc, true);

    cout << "Invert Dmatrix here" << endl;
    table1.cauchy_dinvert(g_cauchy, g_a, g_bi, g_roots, psec, true); 
    
    cout << "+++++++++++++++++++++++++ Syndrome Sectors +++++++++++++++++++++++" << endl;
    for (i=0; i<g_roots; i++) {
        arrout("Syndrome = ", syndrome_p, i, SEC_16ELEM);
    }

    for (int i=0; i < g_roots; i++) {
        for (int j=0; j < SEC_16ELEM; j++) {
            for (int s=0; s < g_roots; s++) {
                galois16_t g_temp = g_cauchy[s][i];
                galois16_t s_temp = syndrome_p[s][j];
                galois16_t r_temp = recovered_p[i][j];

                r_temp += g_temp * s_temp;

                recovered_p[i][j] = r_temp.value(); 
            }
        }
    }

    std::cout << " ===== Erased Sectors. =====" << std::endl;
    for (int i=0; i<g_roots; i++) {
        arrout("Erased Sector - ", recovered_p, i, SEC_16ELEM);
    }

    table1.free_buffer();
    for (i=0; i<nsec; i++) {
        if  (sector_p[i] != NULL)
            free(sector_p[i]);
    }
    free(a);
    free(b);
}
