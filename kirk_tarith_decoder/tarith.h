#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <assert.h>
//typedef GaloisFlat<Galois10flat, Galois10TowerInner, 10> Galois10Flat;

//Comments History
// May 21, 2014: Modified to run gf^16.                                         
 
using namespace std;

typedef unsigned char data8;
typedef unsigned short data16;

// in the following operators, galois represents any galois class

template<class galois>
inline int operator== (const galois& x, const galois& y) {
    return (x.value == y.value);
}

template<class galois>
inline int operator== (const galois& x, const int y) {
    return (x.value == y);
}

template<class galois>
inline int operator!= (const galois& x, const galois& y) {
    return (x.value != y.value);
}

template<class galois>
inline int operator!= (const galois& x, const int y) {
    return (x.value != y);
}

template<class galois>
inline galois operator+ (const galois& x, const galois& y) {
    return galois(x.value ^ y.value);
}

template <class galois>
inline galois operator* (const galois x, const galois y) {
        galois ans = x;
        ans *= y;
        return ans;
}

template<class galois>
inline galois operator/ (const galois& x, const galois& y) {
    return x * y.invert();
}

template<class galois>
galois power (galois x, int n) {
  //printf("power2: x=x%hx, n=x%hx\n", x.value, n);
  if (n < 0) return power(x.invert(), -n);
  galois res = 1;
  galois sq = x;
  while (n > 0) {
    if (n & 1) res *= sq;
    sq *= sq;
    n >>= 1;
  }
  return res;
}
      

// size is the field size, minp represents the min poly of prim elt
template<class data, int size, int minp, int trace_1>
class GaloisBase
{
 public:

    data value;

    GaloisBase(data intval)
        :value(intval) {
        assert(intval < size);
    }

    GaloisBase()
        :value(0) {
    }

    static GaloisBase primElt() {
        return GaloisBase(2); // binary "10" represents x
    }

    static GaloisBase trace1() {
      return GaloisBase(trace_1);
    }

    int isZero() const {
        return (value == 0);
    }

    int isOne() const {
        return (value == 1);
    }

    GaloisBase& operator+= (const GaloisBase& x) {
        value = value ^ x.value;
        return *this;
    }

    GaloisBase& operator*= (const GaloisBase& x) {
      *this = prod(x);
      return *this;
    }

    GaloisBase invert() const {
        assert(!isZero());
        return power(size-2);
    }

    GaloisBase square() const {
      return (*this) * (*this);
    }

    GaloisBase alphatimes() const {
        data sbits = value << 1;
        if (sbits & size) {
            //printf("\t\talphatimes: RESET size=x%x if=%d sbits=x%x\n", size, (sbits & size), sbits);
            sbits ^= minp;
         }
        //printf("\t\talphatimes: return poly=x%x if=%d sbits=x%x\n", minp, (sbits & size), sbits);
        return GaloisBase(sbits);
    }

    GaloisBase prod (GaloisBase x) {
        data ans = 0;
        data val = value;
        //printf("\t>prod: val=x%x x=x%x\n", value, x.value);
        while (val != 0) {
            if (val & 1) ans ^= x.value;
            val >>= 1;
            x = x.alphatimes();
            //printf("\t>prod: loop val=x%x ans=x%x x=x%x\n", val, ans, x.value);
        }
        //printf("\t<prod: return ans=x%x\n", ans);
        return GaloisBase(ans);
    }

    GaloisBase power (int n) const {
      printf("power3: n=x%hx\n", n);
      if (n < 0) return invert().power(-n);
      GaloisBase res = 1;
      GaloisBase sq = *this;
      while (n > 0) {
        if (n & 1) res *= sq;
        sq *= sq;
        n >>= 1;
      }
      return res;
      
    }

};


// assumes an extension by x^2+x+1
template <class data, class subfield, int nbits, int primelt>
class GaloisTower1
{
 public:
    data value;
    
    GaloisTower1(data intval)
        :value(intval) {
        assert(value < (1<<nbits));
    }

    GaloisTower1(subfield high, subfield low) // build from subfield values
      :value(mergebits(high, low)) {
        assert(value < (1<<nbits));
    }

    GaloisTower1()
        :value(0) {
    }

    static GaloisTower1 primElt() {
        return GaloisTower1(primelt);  // alpha + gen of GF(2^3)
    }
    
    static GaloisTower1 trace1() {
      assert((nbits >> 1) & 1); // degree is 2 * odd
      return GaloisTower1(1<<(nbits>>1));
    }

    int isZero() const {
        return (value == 0);
    }

    int isOne() const {
        return (value == 1);
    }

    GaloisTower1& operator+= (const GaloisTower1& x) {
        value = value ^ x.value;
        return *this;
    }

    subfield hibits () const {
        return subfield(value>>(nbits/2));
    }

    subfield lobits () const {
        return subfield(value & ((1<<nbits/2)-1));
    }

    subfield sumbits () const {
        return subfield(lobits() + hibits());
    }

    static data mergebits(subfield high, subfield low) {
        return ((high.value << (nbits/2)) | low.value);
    }

//  (a1,a0) * (b1,b0) = ((a1+a0)*(b1+b0) + a0*b0, a1*b1 + a0*b0)
    GaloisTower1& operator*= (const GaloisTower1& x) {
        if (hibits().isZero()) {
	    value = mergebits(lobits() * x.hibits(), 
			      lobits() * x.lobits());
	    assert(value < (1 << nbits));
            return *this;
        }
        subfield lowprod = lobits() * x.lobits();
        value = mergebits(sumbits() * x.sumbits() + lowprod,
			  hibits() * x.hibits() + lowprod);
	assert(value < (1 << nbits));
        return *this;
    }

//   1/(a1,a0) = (a1 , a1+a0) / (a1^2 + a0*(a1+a0))
    GaloisTower1 invert() const {
        assert(!isZero());
        subfield deninv = (hibits().square() + lobits()*sumbits()).invert();
        return GaloisTower1(hibits() * deninv,
			   sumbits() * deninv);
    }

    GaloisTower1 square() const {
        subfield hisq = hibits().square();
        subfield losq = lobits().square();
        return GaloisTower1(hisq, hisq + losq);
    }

//multiply by (1,0) = x
//multiply  x*(a*x+b) = a*(x+1) + b*x = (a+b)*x + a
    GaloisTower1 alphatimes() const {
        return GaloisTower1(sumbits(), hibits());
    }

    GaloisTower1 power (int n) const {
      printf("power4: n=x%hx\n", n);
      if (n < 0) return invert().power(-n);
      GaloisTower1 res = 1;
      GaloisTower1 sq = *this;
      while (n > 0) {
        if (n & 1) res *= sq;
        sq *= sq;
        n >>= 1;
      }
      return res;
      
    }

};

// assumes an extension by x^2+x+constcoef
template <class data, class subfield, int nbits, int primelt>
  class GaloisTower : public GaloisTower1<data, subfield, nbits, primelt>
{
 public:

    GaloisTower(data16 intval)
        :GaloisTower1<data, subfield, nbits, primelt>(intval) {
    }

    GaloisTower(subfield high, subfield low) // build from subfield values
        :GaloisTower1<data, subfield, nbits, primelt>(high, low) {
    }

    GaloisTower()
        :GaloisTower1<data, subfield, nbits, primelt>(0) {
    }

    static GaloisTower primElt() {
      return GaloisTower(primelt); 
    }

    static GaloisTower trace1() {
      return GaloisTower(subfield::trace1(), 0);
    }

    static subfield constcoef() {
      return subfield::trace1();
    }

//(a1*x + a0) *(b1*x + b0) = a1*b1*(x+constcoef) + (a0*b1+a1*b0)*x + a0*b0
//(a1, a0) * (b1, b0) = ((a1 + a0)*(b1 + b0) + a0*b0, constcoef*a1*b1 + a0*b0)
   
    GaloisTower& operator*= (const GaloisTower& x) {
        if (this->hibits().isZero()) {
            this->value = this->mergebits(this->lobits() * x.hibits(),
			      this->lobits() * x.lobits());
            return *this;
        }
        subfield lowprod = this->lobits() * x.lobits();
        this->value = this->mergebits(this->sumbits() * x.sumbits() + lowprod,
				      constcoef() * this->hibits() * x.hibits() + lowprod);
        return *this;
    }

// 1/(a1*x+a0) = (a1*(x+1)+a0) / (a1^2*c + a0*(a1+a0))
// 1/(a1,a0) = (a1, a1 + a0) / (c*a1^2 + a0*(a1 + a0))

    GaloisTower invert() const {
        assert(!this->isZero());
        subfield deninv =
            (this->constcoef() * this->hibits().square() + this->lobits()*this->sumbits()).invert();
        return GaloisTower(this->hibits()  * deninv,
			   this->sumbits() * deninv);
    }


// (a + b*x)^2 = a^2 + b^2*(x+constcoef)
    GaloisTower square() const {
        subfield hisq = this->hibits().square();
        subfield losq = this->lobits().square();
        return GaloisTower(hisq, this->constcoef() * hisq + losq);
    }


//multiply by (1,0) = x
//multiply  x*(a*x+b) = a*(x+constcoef) + b*x = (a+b)*x + a
    GaloisTower alphatimes() const {
        return GaloisTower(this->sumbits(),  this->constcoef() * this->hibits());
    }

    GaloisTower power (int n) const {
      printf("power5: n=x%hx\n", n);
      if (n < 0) return invert().power(-n);
      GaloisTower res = 1;
      GaloisTower sq = *this;
      while (n > 0) {
        if (n & 1) res *= sq;
        sq *= sq;
        n >>= 1;
      }
      return res;
      
    }


};

// assumes an extension by x^2+x+constcoef
template <class subfield>
  class GaloisTowerPair
{
 public:
    subfield high;
    subfield low;
    
    GaloisTowerPair(subfield highval, subfield lowval) // build from subfield values
      :high(highval), low(lowval) {
    }

    GaloisTowerPair()
      :high(subfield()), low(subfield()) {
    }

    GaloisTowerPair(int intval) {
      high = 0;
      low = subfield(intval);
    }

    static GaloisTowerPair trace1() {
      return GaloisTowerPair(subfield::trace1(), 0);
    }

    static subfield constcoef() {
      return subfield::trace1();
    }

    int isZero() const {
        return high.isZero() && low.isZero();
    }

    int isOne() const {
        return high.isZero() && low.isOne();
    }

    GaloisTowerPair& operator+= (const GaloisTowerPair& x) {
        high += x.high;
	low  += x.low; 
	return *this;
    }

//(a1*x + a0) *(b1*x + b0) = a1*b1*(x+constcoef) + (a0*b1+a1*b0)*x + a0*b0
//(a1, a0) * (b1, b0) = ((a1 + a0)*(b1 + b0) + a0*b0, constcoef*a1*b1 + a0*b0)
   
    GaloisTowerPair& operator*= (const GaloisTowerPair& x) {
        if (high.isZero()) {
	  high = low * x.high;
	  low  *= x.low;
	  return *this;
        }
        subfield lowprod = low * x.low;
	subfield hiprod =  high * x.high;
	high = (high + low)*(x.high + x.low) + lowprod;
	low  = constcoef() * hiprod + lowprod;
        return *this;
    }

// 1/(a1*x+a0) = (a1*(x+1)+a0) / (a1^2*c + a0*(a1+a0))
// 1/(a1,a0) = (a1, a1 + a0) / (c*a1^2 + a0*(a1 + a0))

    GaloisTowerPair invert() const {
        assert(!isZero());
        subfield deninv =
            (constcoef() * high.square() + low*(high + low)).invert();
        return GaloisTowerPair(high  * deninv,
			       (high + low) * deninv);
    }

// (a + b*x)^2 = a^2 + b^2*(x+constcoef)
    GaloisTowerPair square() const {
        subfield hisq = high.square();
        subfield losq = low.square();
        return GaloisTowerPair(hisq, this->constcoef() * hisq + losq);
    }


//multiply by (1,0) = x
//multiply  x*(a*x+b) = a*(x+constcoef) + b*x = (a+b)*x + a
    GaloisTowerPair alphatimes() const {
        return GaloisTowerPair(high + low,  constcoef() * high);
    }


    GaloisTowerPair power (int n) const {
      printf("power6: n=x%hx\n", n);
      if (n < 0) return invert().power(-n);
      GaloisTowerPair res = 1;
      GaloisTowerPair sq = *this;
      while (n > 0) {
        if (n & 1) res *= sq;
        sq *= sq;
        n >>= 1;
      }
      return res;
      
    }

};


/*
template <class d, class subfield, int nbits, int p>
inline GaloisTower1<d,subfield,nbits,p> operator* (const subfield x,
                                       const GaloisTower1<d,subfield,nbits,p> y) {
    return GaloisTower1<d,subfield,nbits,p>(((x*y.hibits()).value << (nbits/2)) | (x*y.lobits()).value);
}

template <class d, class subfield, int nbits, int p, subfield c>
inline GaloisTower<d,subfield,nbits,p> operator* (const subfield x,
                                       const GaloisTower<d,subfield,nbits,p> y) {
    return GaloisTower<d,subfield,nbits,p>(((x*y.hibits()).value << (nbits/2)) | (x*y.lobits()).value);
}
*/

template<class data, int size, int minp, int trace_1>
inline ostream& operator<< (ostream& co, const GaloisBase<data,size,minp, trace_1>& x) {
    return co << hex << (unsigned short) x.value << dec ;
}

template<class subfield>
inline ostream& operator<< (ostream& co, const GaloisTowerPair<subfield>& x) {
    return co << hex << "(" << x.high << "," << x.low << ")" << dec ;
}

template <class d, class s, int nbits, int p>
inline ostream& operator<< (ostream& co, const GaloisTower1<d,s,nbits,p>& x) {
#ifdef SPLITOUT
    return co << "(" << x.hibits() << "," <<  x.lobits() << ")" ;
#else
    return co << hex << (unsigned short) x.value << dec ;
#endif
}

template <class field1, class field2>
field2 convert(const field1 x, field2 basis[]);

template<class Field>
void arrout(Field* arr, int size);

template <class GaloisFlatInner, class GaloisTower, int nbits>
class GaloisFlat : public GaloisFlatInner
{
 public:

  GaloisFlat(data16 intval) :GaloisFlatInner(intval) {
  }
  
  GaloisFlat() :GaloisFlatInner() {
  }
  
  //  GaloisFlat(GaloisFlat& x) :GaloisFlatInner(x.value) {
  //  }
  
  GaloisFlat(GaloisFlatInner x) :GaloisFlatInner(x.value) {
  }

  GaloisFlat& operator= (GaloisFlat y) {
    this->value = y.value;
    return *this;
  }

  static GaloisFlat primElt() {
    GaloisFlatInner prim = GaloisFlatInner::primElt();
    return GaloisFlat(prim.value);
  }

 GaloisFlat invert(bool matout = false) const {
    assert(!this->isZero());
    GaloisFlatInner towerBasis[nbits];
    GaloisTower flatBasis[nbits];
    flatBasis[0] = 1;
    towerBasis[0] = 1;
    for (int i=1; i<nbits; i++) {
      flatBasis[i] = flatBasis[i-1]*GaloisTower::primElt();
      towerBasis[i] = 1<<i;
    }

    GaloisTower tow = convert<GaloisFlatInner, GaloisTower>(GaloisFlatInner(this->value), flatBasis);
    if (matout) {
      cout << "inverse input in flat rep = " << *this << endl;
      cout << "flatBasis = ";
      arrout(flatBasis, nbits);
      cout << "input in tower rep = " << tow << endl;
    }
    
    // convert towerBasis to inverse of flatBasis
    for (int j=0; j<nbits; j++) {
      int bitpos = 1<<j;
      int k = j;
      while ((flatBasis[k].value & bitpos) == 0) k++;
      assert(k<nbits);
      if (k > j) {
        GaloisTower ttemp = flatBasis[j];
        flatBasis[j] = flatBasis[k];
        flatBasis[k] = ttemp;
        GaloisFlatInner ftemp = towerBasis[j];
        towerBasis[j] = towerBasis[k];
        towerBasis[k] = ftemp;
      }
      for (int i=0; i<nbits; i++) {
        if ((i != j) && (flatBasis[i].value & bitpos)) {
          flatBasis[i] += flatBasis[j];
          towerBasis[i] += towerBasis[j];
        }
      }
    }

    GaloisFlatInner inv = convert<GaloisTower, GaloisFlatInner>(tow.invert(), towerBasis);

    if (matout) {
      cout << "inverse in tower rep = " << tow.invert() << endl;
      cout << "towerBasis = ";
      arrout(towerBasis, nbits);
      cout << "inverse converted to flat rep = " << inv << endl;
    }

    return inv;
 }

 GaloisFlat powerSplit (int m) const { // computes powers of primElt using direct product rep
   assert(nbits & 1 == 0);
   assert(m >= 0);
   assert(m < (1 << nbits));
   int n = nbits >> 1; // field is GF(2^(2*n))
   GaloisFlat g=primElt();
   GaloisFlat h = g.power(1<<(n-1));
   GaloisFlat g1 = h.power((1 << n) + 1);    // g1 has order 2^n-1
   GaloisFlat g2 = h.power((1 << n) - 1);    // g2 has order 2^n+1
                                             // g1 * g2 = g
   int ml = m & ((1 << n) - 1);
   int mh = m >> n;
   int n1 = ml + mh;                 // 0 <= n1 <= 2*(2^n-1)
   if (n1 & (1<<n)) {                // if (n1 >= 2^n)
     n1 &= ((1<<n) - 1);             // n1 -= 2^n
     n1 += 1;                        // n1 <= 2^n-1
   }
   int n2 = ml - mh;                 // ml + 2^n+1 - mh <= 2^(n+1)
   if (n2 < 0) {
     n2 += (1<<n) + 1;
   }
   return power(g1,n1)*power(g2,n2);
 }

};

// arithmetic over GF(2^2)
// minpoly = x^2+x+1
typedef GaloisBase<data8, 1<<2, (1<<2) + (1<<1) + 1, 1<<1> Galois2;

// arithmetic over GF(2^3)
// minpoly = x^3+x+1
typedef GaloisBase<data8, 1<<3, (1<<3) + (1<<1) + 1, 1> Galois3;

// arithmetic over GF(2^6)
// generator over GF(2^3) satisfies x^2+x+1
// high order 3 bits are coef of generator

typedef GaloisTower1<data8, Galois3, 6, (1<<3) + (1<<1)> Galois6;  // alpha + gen of GF(2^3)

// arithmetic over GF(2^12)
// generator over GF(2^6) satisfies x^2+x+(1,0)
// where (1,0) is generator of GF(2^6) over GF(2^3)
// high order 6 bits are coef of generator

typedef GaloisTower<data16, Galois6, 12, (((1<<2)+(1<<1))<<(3+6)) + (((1<<1)+1)<<3) + (1<<1)+1> Galois12Tower;
// z:=(a+1)*(a*b*c+b+1) = (a^2+a)*b*c + (a+1)*b + (a+1)

typedef GaloisTowerPair<Galois6> Galois12TowerPair;
// z:=(a+1)*(a*b*c+b+1) = (a^2+a)*b*c + (a+1)*b + (a+1)


// arithmetic over GF(2^5)
// minpoly = x^5+x^2+1
typedef GaloisBase<data8, 1<<5, (1<<5) + (1<<2) + 1, 1> Galois5;

// arithmetic over GF(2^10)
// generator over GF(2^5) satisfies x^2+x+1
// high order 5 bits are coef of generator
//(KH07302007)
//typedef GaloisTower1<data16, Galois5, 10, (((1<<2) + (1<<1))<<5) + 1> Galois10Tower;
typedef GaloisTower1<data16, Galois5, 10, (3<<5) + 4> Galois10Tower;
//   // alpha*(a^2+a) + 1 "00110 00001" from GF(2^5)
// primitive element which is a root of minpoly for Galois10flat

// flat arithmetic over GF(2^8)
// minpoly = x^8 + x^4 + x^3 + x^2 + 1
typedef GaloisBase<data8, 1<<8, (1<<8) + (1<<4) + (1<<3) + (1<<2) + 1, 1<<5> Galois8flat;

// flat arithmetic over GF(2^10)
// minpoly = x^10 + x^3 + 1
typedef GaloisBase<data16, 1<<10, (1<<10) + (1<<3) + 1, 1<<7> Galois10flat;

// following prim element is compatible with Galois10Flat
typedef GaloisTower1<data16, Galois5, 10, (((1<<2) + (1<<1))<<5) + 1> Galois10TowerInner;
// alpha(a^2+a)+ 1 "00011 00100" from GF(2^5)

// flat arithmetic over GF(2^12)
// minpoly = x^12+x^6+x^4+x+1
typedef GaloisBase<int, 1<<12, (1<<12) + (1<<6) + (1<<4) + (1<<1) + 1, 1<<11> Galois12flat;

// flat arithmetic over GF(2^16)
// minpoly = x^16+x^12+x^3+x+1
typedef GaloisBase<int, 1<<16, (1<<16) + (1<<12) + (1<<3) + (1<<1) + 1, 1<<15> Galois16flat;

//typedef GaloisFlat<Galois10flat, Galois10Tower, 10> Galois10Flat;
//(KH07392007)
typedef GaloisFlat<Galois10flat, Galois10TowerInner, 10> Galois10Flat;
typedef GaloisFlat<Galois12flat, Galois12Tower, 12> Galois12Flat;



