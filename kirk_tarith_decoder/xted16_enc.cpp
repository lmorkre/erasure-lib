#include "tarith.h"
#include <vector>

// following typedef specifies which field to operate over
// fields implemented are: Galois3, Galois5, Galois6, Galois10, Galois12
// flat fields are Galois10Flat and Galois12Flat (using change of basis inversion)
// Jan 28 2007: Barry fixed case for all erasure used.
//              Bypassed the key equation solver.
  typedef Galois16flat Galois;
//typedef Galois10Flat Galois;
//typedef Galois10Tower Galois;


static const int SEC_SIZE = 16; // number of sector size
static const int NSEC     = 30;  // number of data sectors
static const int PSEC     = 20; // number of parity sectors
const int NCHECKS  = 7;         // number of syndromes (check symbols)
const int M_ERS    = 4;         // number of erasure in data sector
const int N_ERS    = 4;         // number of erasure sectors
const int b        = 23;       // b= a^23

       int dskipm[NSEC];                             // data sector skip mask
       int pskipm[PSEC];                             // pariy sector skip mask
static Galois tbuffer[NSEC+PSEC][SEC_SIZE+NCHECKS];  // tbuffer = data sectors
static Galois sbuffer[PSEC][SEC_SIZE+NCHECKS];       // sbuffer = PParity sectors
static Galois ers_sector[PSEC][SEC_SIZE+NCHECKS];    // ers_sector = Recovered data
static Galois buffer[SEC_SIZE+NCHECKS];              // buffer
static Galois P0_sec[SEC_SIZE+NCHECKS];              // parity 0
static Galois P1_sec[SEC_SIZE+NCHECKS];              // parity 1
static Galois S0_sec[SEC_SIZE+NCHECKS];              // syndrome 0
static Galois S1_sec[SEC_SIZE+NCHECKS];              // syndrome 1
static Galois msynd[NCHECKS];                        // NCHECKS checks
static Galois merasept[NCHECKS];                     // M_ERS erasures(1 to 6)
static Galois P[PSEC][SEC_SIZE+NCHECKS];             // parity sector 0,..,3
static Galois S[PSEC][SEC_SIZE+NCHECKS];             // synd   sector 0,..,3
static Galois T[PSEC];                               // T0=1,T1=2,T2=4,T3=8 
static Galois R[NSEC];                               // R0=B^j, j=1....Nsec
static Galois G[PSEC][NSEC];                         // 1/(Ti + Ri)          
static Galois a[PSEC];                               // am
static Galois bi[NSEC];                              // bm
static Galois D[PSEC][NSEC];                         // Dm[4x4]
static Galois f[N_ERS+1];                            // fm
static Galois df[(N_ERS+1)/2];                       // dfm
static Galois g[N_ERS+1];                            // gm
static Galois dg[(N_ERS+1)/2];                       // dgm


//***********************************************
// SUM {D(j) * 1/(T0 + R^j) ,J=1,,Nsec}  ==> P0
// SUM {D(j) * 1/(T1 + R^j)           }  ==> P1
// SUM {D(j) * 1/(T2 + R^j)           }  ==> P2
// SUM {D(j) * 1/(T3 + R^j)           }  ==> P2
//***********************************************
  
static Galois ersecpt[PSEC][N_ERS];          //ersspt  = erase sector pointer (b0,b1,b2,and b3)   
static Galois invC[N_ERS+1];                 //1/c   
static Galois cv[PSEC][N_ERS];               //cv   
bool  debug = true;

Galois power(Galois base, data16 exp) {
    Galois ans = 1;
    Galois mult = base;
    printf("power: base=x%hx, exp=x%hx\n", base.value, exp);
    while (exp > 0) {
      if (exp & 1) ans *= mult;
      mult *= mult;
      exp >>= 1;
        printf("power: loop mult=x%hx, exp=x%hx\n", mult.value, exp);
    }
    return ans;
}

int loginv (Galois loc) {
  assert(loc != 0);
  Galois prim = Galois::primElt();
  for (int i=0; ;i++) {
    if (loc == 1) return i;
    loc *= prim;
  }
}


// converts between isomorphic fields where basis of source field
// is given expressed in target field
template <class field1, class field2>
field2 convert(const field1 x, field2 basis[]) {
  field2 ans = 0;
  data16 xbits = x.value;
  int i = 0;
  while (xbits > 0) {
    if (xbits & 1) ans += basis[i];
    xbits >>= 1;
    i++;
  }
  return ans;
}

// prints out contents of array argument
template<class Field>
void arrout(Field* arr, int size) {
  cout << "[ " ;
  for (int i=0; i<size; i++) cout << arr[i] << " ";
  cout << "]" << endl;
}

template<class Field>
void arrout(char* message, Field* arr, int size) {
  cout << message;
  arrout(arr, size);
}

template<class Field>
void arrout(Field* arr, int nsec, int size) {
  cout << "[ " ;
  for (int i=0; i<size; i++) cout << arr[nsec][i] << " ";
  cout << "]" << endl;
}

template<class Field>
void arrout(char* message, Field* arr, int nsec, int size) {
  cout << message << " " << nsec  << " ";
  arrout(arr, nsec,size);
}

template<class Field>
void arrout(Field* arr, int nsec, int top, int size) {
  cout << "[ " ;
  for (int i=top; i<size; i++) cout << arr[nsec][i] << " ";
  cout << "]" << endl;
}

template<class Field>
void arrout(char* message, Field* arr, int nsec, int top, int size) {
  cout << message << " " << nsec  << " ";
  arrout(arr, nsec, top, size);
}

data16 rand(unsigned int n) // random int 0<=x<n
{
    return (rand() % n);
}



void updateChecks(Galois data, Galois* checks, int nchecks) {
    checks[0] += data;
    Galois prim = Galois::primElt();
    Galois mult = prim;
    for (int i=1; i<nchecks; i++) {
      checks[i] *= mult;
      checks[i] += checks[i-1];
      mult *= prim;
    }
}

// compute the check symbols for data
void getChecks(Galois* data, int dsize, Galois* checks) {
    //assume checks start out 0.
    Galois tchecks[NCHECKS];
    for (int i=0; i<dsize; i++) updateChecks(data[i], tchecks, NCHECKS);

    Galois prim = Galois::primElt();
    for (int i=0; i<NCHECKS; i++) {
      Galois mult = 1;
      Galois sum = 0;
      for (int j=0; j<NCHECKS-i; j++) {
	sum += mult * tchecks[j];
	mult *= prim;
      }
      data[dsize+i] = sum;
      updateChecks(sum, tchecks, NCHECKS-i);
    }
}

// Data Sector Generator,
void SecGen(int no_of_dsector, int no_of_psector, int sector_size, int no_of_checks, int type)
{
   int bsize; 
 
//  Comment : Data Sector Gen 
//  Comment : Type 1. Random Data  Type 2. Sequential Data

    bsize = sector_size + no_of_checks;

    for (int i=0; i < no_of_dsector+no_of_psector; i++) {
        for (int j=0; j < bsize; j++) {
            tbuffer[i][j] = 0;
        } 
    }

    int symb =1;
    for (int i=0; i < no_of_dsector; i++) {
        symb = symb +  1;
        for (int j=0; j < sector_size; j++) {
          if (type ==1 ){
            tbuffer[i][j] = rand() % 999 +1 ;
            }
          else {
           tbuffer[i][j] = symb;
          }
        }  
    }

  for (int j=0; j < no_of_dsector; j++) {
     for (int i=0; i < bsize; i++) { buffer[i] = tbuffer[j][i];}
        buffer[0] = 0x99;
        buffer[1] = 0x77;
        buffer[2] = j*4;
        buffer[3] = j*4+1;
        buffer[4] = j*4+2;
        buffer[5] = j*4+3;
     getChecks(buffer, sector_size, buffer+sector_size); // compute check symbols for buffer
     for (int i=0; i < bsize; i++ ) {tbuffer[j][i] = buffer[i];}
  }
}



//***************************************************************
// TED Encode                                                  **
//***************************************************************

/// Cauchy Matrix 
void Cauchy_Dmatrix(int no_of_dsec, int no_of_psec) 
{ 
        Galois prim = Galois::primElt();

        for (int i=0; i < no_of_psec; i++) {
            a[i] = power(prim,1*(i));            //LM: need to convert to GF-complete
            printf("power of a: %d is x%x\n", i, a[i].value);
		}	
		for (int j=0; j < no_of_dsec; j++) { 
            bi[j] = power(prim,b*(j+1));         //LM: need to convert to GF-complete
		}
		for (int i=0; i < no_of_psec; i++) {
		     for (int j=0; j < no_of_dsec; j++) {
			    Galois g= a[i] + bi[j];
			    D[i][j] = g.invert(); //LM: need to convert to GF-complete
			}	
		}		
		
	
// Comment: Encoder Matrix D=(1/(ai+b^j)), i= Parity, j= data or erasure location.
    if (debug) {
		cout << " a  => " << endl;
		arrout(a,no_of_psec);
		
		cout << " bi  => " << endl;
		arrout(bi,no_of_dsec);

        cout << " ==== Cauchy Matrix D[Parity,Data or Erasure]       ======" << endl;	
        for (int i=0; i < no_of_psec; i++){
           arrout("D[i,j] = ", D[i], no_of_dsec); // display D[p0,d0..n] buffer
	   	}
	}	
}

void Cauchy_Encoder(Galois* in_buf, int no_of_psec, int size, int jth_sec, int bad_sec) 
{ 
// Comment: Encoder Matrix D=(1/(ai+b^j)), i= Parity, j= erasure location.
     
        if (bad_sec != 1) {
			for (int j=0; j < (size); j++) {
               Galois data_in  = in_buf[j];
               for (int i=0; i < no_of_psec; i++) {
                   P[i][j] += data_in * D[i][jth_sec]; //LM: doing Galios math in tarith.h
				}   
            }
         }	
    
}



void encode_test(){
    cout << "*****  <<  Encoding Test   >> *****" << endl;
    cout << " o Generate Data Sectors.          " << endl;
    cout << " o Generate Parity sectors.        " << endl;
    cout << " **********************************" << endl;
    cout << "Num of Data Sector : " << NSEC << "... [0,1,..," << NSEC-1 <<"] data sector " <<endl;
    cout << "Num of Prty Sector : " << PSEC << "... [0,1,..," << PSEC-1 <<"] parity sector " <<endl;
    cout << "Sector Size        : " << SEC_SIZE << endl;
    cout << "No of Checks       : " << NCHECKS << endl;
	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	cout << " Input to Encoder: Data Sectors Generation            " << endl;
    SecGen(NSEC, PSEC, SEC_SIZE, NCHECKS, 2);
	cout << "++++++++++++++++ Start Encoding +++++++++++++++++++++" << endl;
	cout << " Encoder:                                             " << endl;
	cout << "1) Cauchy_Dmatrix  D=(1/(ai+b^j)),i= Parity, j= Data  " << endl;
	cout << "2) Cauchy_Encoder  Parity Sec=Data_sec * D            " << endl;
	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
 
// Comment: Cauchy Dmatrix D=(1/(ai+b^j)), i= Parity, j= erasure location.
    Cauchy_Dmatrix(NSEC,PSEC);
// Comment: Cauchy Encoder: Parity Sector =Data Sector * Dmatrix
	for (int i=0; i < NSEC; i++) {
         Cauchy_Encoder(tbuffer[i], PSEC, SEC_SIZE+NCHECKS, i, 0); //LM: zero means good data 
    }
	cout << "++++++++++++++++ Finish Encoding +++++++++++++++++++++" << endl;
	
    // Track = Merge Data and Parity Sectors. 
    for (int j=0; j < SEC_SIZE+NCHECKS; j++ ) {
	    for(int i=0; i < PSEC; i++) {
             tbuffer[NSEC+i][j]   = P[i][j];
        }
    }		
	if (true) {
       cout << "**<< Track = Data Sector || Parity Sector >>**" << endl;

       cout << " ==========  Data Sector ===========" << endl;
       for (int i=0;    i < NSEC; i++)      arrout("Data  Sector = ", tbuffer, i, SEC_SIZE+NCHECKS);
       cout << " ==========  Encode Output ===========" << endl;
       for (int i=NSEC; i < NSEC+PSEC; i++) arrout("Prty  Sector = ", tbuffer, i, SEC_SIZE+NCHECKS); 
       cout << "*****<< Encoding Test Completed >>*****" << endl;
    }
}




int main(int nargs, char** args) {
    encode_test();
    return 0;
}



