#include "tarith.h"
#include <vector>
//***************************
// Change History
//===========================
// KHH: 9/22/2014: Modify Erasure Output to make GF_multiply_region friendly.
// KHH: 7/24/2014: Tarith.h updatede for GF16 
  typedef Galois16flat Galois;
//typedef Galois10Flat Galois;
//typedef Galois10Tower Galois;


static const int SEC_SIZE = 16; // number of sector size
static const int NSEC     = 30;  // number of data sectors
static const int PSEC     = 20; // number of parity sectors
const int NCHECKS  = 7;         // number of syndromes (check symbols)
const int b        = 23;        // b= a^23
	   
static Galois tbuffer[NSEC+PSEC][SEC_SIZE+NCHECKS];  // tbuffer = data sectors
static Galois ers_sector[PSEC][SEC_SIZE+NCHECKS];    // ers_sector = Recovered data
static Galois buffer[SEC_SIZE+NCHECKS];              // buffer
static Galois P[PSEC][SEC_SIZE+NCHECKS];             // parity sector 0,..,3
static Galois a[PSEC];                               // ai 
static Galois bi[NSEC];                              // bj
static Galois D[PSEC][NSEC];                         // Dmatrix
static Galois f[PSEC+1];                             // fm
static Galois df[(PSEC+1)/2];                        // dfm
static Galois g[PSEC+1];                             // gm
static Galois dg[(PSEC+1)/2];                        // dgm
int    dskipm[NSEC];                                 // data sector skip mask
int    pskipm[PSEC];                                 // pariy sector skip mask
int    eras_loc[PSEC];                               // Erase pointer 
int    prty_loc[PSEC];                               // Parity Pointer

bool  debug = false;


Galois power(Galois base, data16 exp) {
    Galois ans = 1;
    Galois mult = base;
    while (exp > 0) {
      if (exp & 1) ans *= mult;
      mult *= mult;
      exp >>= 1;
    }
    return ans;
}

int loginv (Galois loc) {
  assert(loc != 0);
  Galois prim = Galois::primElt();
  for (int i=0; ;i++) {
    if (loc == 1) return i;
    loc *= prim;
  }
}


// converts between isomorphic fields where basis of source field
// is given expressed in target field
template <class field1, class field2>
field2 convert(const field1 x, field2 basis[]) {
  field2 ans = 0;
  data16 xbits = x.value;
  int i = 0;
  while (xbits > 0) {
    if (xbits & 1) ans += basis[i];
    xbits >>= 1;
    i++;
  }
  return ans;
}

// prints out contents of array argument
template<class Field>
void arrout(Field* arr, int size) {
  cout << "[ " ;
  for (int i=0; i<size; i++) cout << arr[i] << " ";
  cout << "]" << endl;
}

template<class Field>
void arrout(char* message, Field* arr, int size) {
  cout << message;
  arrout(arr, size);
}

template<class Field>
void arrout(Field* arr, int nsec, int size) {
  cout << "[ " ;
  for (int i=0; i<size; i++) cout << arr[nsec][i] << " ";
  cout << "]" << endl;
}

template<class Field>
void arrout(char* message, Field* arr, int nsec, int size) {
  cout << message << " " << nsec  << " ";
  arrout(arr, nsec,size);
}

template<class Field>
void arrout(Field* arr, int nsec, int top, int size) {
  cout << "[ " ;
  for (int i=top; i<size; i++) cout << arr[nsec][i] << " ";
  cout << "]" << endl;
}

template<class Field>
void arrout(char* message, Field* arr, int nsec, int top, int size) {
  cout << message << " " << nsec  << " ";
  arrout(arr, nsec, top, size);
}

data16 rand(unsigned int n) // random int 0<=x<n
{
    return (rand() % n);
}

// computes syndromes for data
void getSynd(Galois* data, int size,
             Galois synd[NCHECKS]) {
    //assumes synd start out 0
    Galois mults[NCHECKS];
    mults[0] = 1;
    Galois prim = Galois::primElt();
    mults[1] = prim;
    for (int k=2; k<NCHECKS; k++)
        mults[k] = prim * mults[k-1];
    
    for (int i=0; i<size; i++) {
        Galois eval = data[i];
        for (int k=0; k<NCHECKS; k++) {
            synd[k] *= mults[k];
            synd[k] += eval;
        }
    }
}

void updateChecks(Galois data, Galois* checks, int nchecks) {
    checks[0] += data;
    Galois prim = Galois::primElt();
    Galois mult = prim;
    for (int i=1; i<nchecks; i++) {
      checks[i] *= mult;
      checks[i] += checks[i-1];
      mult *= prim;
    }
}

// compute the check symbols for data
void getChecks(Galois* data, int dsize, Galois* checks) {
    //assume checks start out 0.
    Galois tchecks[NCHECKS];
    for (int i=0; i<dsize; i++) updateChecks(data[i], tchecks, NCHECKS);

    Galois prim = Galois::primElt();
    for (int i=0; i<NCHECKS; i++) {
      Galois mult = 1;
      Galois sum = 0;
      for (int j=0; j<NCHECKS-i; j++) {
	sum += mult * tchecks[j];
	mult *= prim;
      }
      data[dsize+i] = sum;
      updateChecks(sum, tchecks, NCHECKS-i);
    }
}

// Data In : Data Sector generation
void SecGen(int no_of_dsector, int no_of_psector, int sector_size, int no_of_checks, int type)
{
   int bsize; 
 
//  Comment : Data Sector Gen 
//  Comment : Type 1. Random Data  Type 2. Sequential Data

    bsize = sector_size + no_of_checks;

    for (int i=0; i < no_of_dsector+no_of_psector; i++) {
        for (int j=0; j < bsize; j++) {
            tbuffer[i][j] = 0;
        } 
    }

    int symb =1;
    for (int i=0; i < no_of_dsector; i++) {
        symb = symb +  1;
        for (int j=0; j < sector_size; j++) {
          if (type ==1 ){
            tbuffer[i][j] = rand() % 999 +1 ;
            }
          else {
           tbuffer[i][j] = symb;
          }
        }  
    }

  for (int j=0; j < no_of_dsector; j++) {
     for (int i=0; i < bsize; i++) { buffer[i] = tbuffer[j][i];}
        buffer[0] = 0x99;
        buffer[1] = 0x77;
        buffer[2] = j*4;
        buffer[3] = j*4+1;
        buffer[4] = j*4+2;
        buffer[5] = j*4+3;
     getChecks(buffer, sector_size, buffer+sector_size); // compute check symbols for buffer
     for (int i=0; i < bsize; i++ ) {tbuffer[j][i] = buffer[i];}
  }
}

// Evaluate V(x) at erasure location 
Galois evalPoly(Galois* v, Galois* dv, Galois erasept, int deg) {
        Galois poly;
		Galois horner = 1;
     	Galois pt = erasept;
        for (int i=deg-1; i >=0; i--) {
          if (debug) {
     		 cout << "i, pt  = " << i <<"  " << pt    << endl;
             cout << "v[i]   = " << v[i] << endl;
           }
            Galois c = v[i] + horner * pt;
	        horner = c;
          if (debug) { cout << "Mac   = " << c << endl;}
        }   
        poly = horner;		
         
    return poly;
}
// Evaluate dV(x)/dx at erasure location.
Galois evalDeriv(Galois* v, Galois pt, int n) {
    Galois valsq = pt.square();
    Galois ans = v[n-1];
    if (debug) cout << "n,v[n],pt,ans   :         = " << n <<" "<< pt <<"  " << ans << endl;
    for (int i=n-2; i>=0; i--) {
        ans = (ans * valsq) + v[i];
        if (debug) cout << "i,v[n],ans   :         = " << i <<" "<< v[i] <<" " << ans << endl;
    }
    return ans;
}

// Expansion of roots. V(x)=x(x-x1)(x-x2)..(x-xn) = vn+ v(n-1)x^(n-1) + ...+v(0).
int expRoots(Galois* v, Galois*dv, Galois* erasept, int nerase) {
    for (int k=0; k < nerase; k++) {
        Galois pt = erasept[k];
        for (int j=k+1; j>=1; j--) {
            v[j] = pt*v[j] + v[j-1];
            if (debug) { cout << "V[j]  = ";
                arrout(v, nerase);}  
        }
	    v[0] = pt*v[0]; 
	}	
    for (int k=0; k < (nerase+1)/2; k++) { 	
	    dv[k] = v[k*2+1];	
	}
}

/// Cauchy Matrix 
void Cauchy_Dmatrix(int no_of_dsec, int no_of_psec, bool is_decode, int eloc_arg[], int ploc_arg[], bool display) 
{ 
        Galois prim = Galois::primElt();
        int erase_loc;
		int parity_loc;
        for (int i=0; i < no_of_psec; i++) {
		    if (is_decode) {
			   parity_loc = ploc_arg[i];
			   if (debug) {cout << " parity_loc = " << parity_loc << endl;}
               a[i] = power(prim,1*(parity_loc));
            } 
			else {
               a[i] = power(prim,1*(i));
			}   
		}	
		for (int j=0; j < no_of_dsec; j++) { 
		    if (is_decode) {
			   erase_loc = eloc_arg[j];
  			   if (debug) {cout << " erase_loc = " << erase_loc << endl;}
			   bi[j] = power(prim,b*(erase_loc+1));
            } 	
            else {			
               bi[j] = power(prim,b*(j+1));
			}   
		}
		for (int i=0; i < no_of_psec; i++) {
		     for (int j=0; j < no_of_dsec; j++) {
			    Galois g= a[i] + bi[j];
			    D[i][j] = g.invert();
			}	
		}		
		
	
// Comment: Encoder Matrix D=(1/(ai+b^j)), i= Parity, j= data or erasure location.
    if (debug) {
		cout << " a  => " << endl;
		arrout(a,no_of_psec);
	    cout << " bi  => " << endl;
		arrout(bi,no_of_dsec);
    }
	if (display or debug) {
        cout << " ==== Cauchy Matrix D[Parity,Data or Erasure]       ======" << endl;	
        for (int i=0; i < no_of_psec; i++){
           arrout("D[i] = ", D,i, no_of_dsec); // display D[p0,d0..n] buffer
	   	}
	}	
}

void Cauchy_Encoder(Galois* in_buf, int no_of_psec, int size, int jth_sec, int bad_sec, bool is_parity) 
{ 
// Comment: Encoder Matrix D=(1/(ai+b^j)), i= Parity, j= erasure location.
     
        if (bad_sec != 1) {
			for (int j=0; j < (size); j++) {
                Galois data_in  = in_buf[j];
			    if (is_parity) { 
                    P[jth_sec][j] += data_in;
                }
			   	else { 	   
                    for (int i=0; i < no_of_psec; i++) {
                       P[i][j] += data_in * D[i][jth_sec];}
				}   
            }
         }	
    
}


//***************************************************************
// TED Encode/Decode                                           **
//***************************************************************

void Cauchy_Dinvert(int no_of_erase, Galois* a, Galois* bi, bool display)
{
    Galois left_nu,left_dn,wl[no_of_erase],right_nu,right_dn,wr[no_of_erase];
    Galois prim = Galois::primElt();
	
    cout << " ==== Parity Sector Polynomial F(x)       ======" << endl;
    for (int i=0; i<= no_of_erase; i++){
    	f[i] = 0; g[i] =0;
    	}
	f[0]=1;
	expRoots(f,df,a,no_of_erase);
	cout << "Erspt    = ";
    arrout(a,no_of_erase);
    cout << "F(x)     = ";
    arrout(f, no_of_erase+1);
    cout << "dF(x)    = ";
	arrout(df, (no_of_erase+1)/2);
	
    cout << " ==== Erased Data Sector Polynomial G(x)  ======" << endl;
	g[0]=1;
	expRoots(g,dg,bi,no_of_erase);
	cout << "Erspt    = ";
    arrout(bi,no_of_erase);
    cout << "G(x)     = ";
    arrout(g, no_of_erase+1);
    cout << "dG(x)    = ";
	arrout(dg, (no_of_erase+1)/2);
	
    cout << " ==== Evaluate F(x)/dG(x) at bi[i],i=0..3 ======" << endl;	
	for (int k=0; k < no_of_erase; k++) {
	   left_nu =evalPoly(f,df,bi[k],no_of_erase);
	   left_dn =evalDeriv(dg,bi[k],(no_of_erase+1)/2);
	   wl[k] = left_nu * left_dn.invert();
	   if (debug) {
	      cout << "left_nu       = " << left_nu << endl;	
	      cout << "left_dn       = " << left_dn << endl;		
	      cout << "wl            = " << left_nu*left_dn.invert() << endl;
	    }
	}
	cout << "Left-MW  = ";
	arrout(wl,no_of_erase);
	
    cout << " ==== Evaluate G(x)/dF(x) at a[i],i=0..3  ======" << endl;	
	for (int k=0; k < no_of_erase; k++) {
	   right_nu =evalPoly(g,dg,a[k],no_of_erase);
	   right_dn =evalDeriv(df,a[k],(no_of_erase+1)/2);
	   wr[k] = right_nu * right_dn.invert();
	   if (debug) {
	      cout << "right_nu       = " << right_nu << endl;	
	      cout << "right_dn       = " << right_dn << endl;		
	      cout << "wr             = " << right_nu*right_dn.invert() << endl;
	    }
	}
	cout << "Right-MW = ";
	arrout(wr,no_of_erase);	
      
// Commnet: Find the inverse of D^-1..=wl*D*wr

   for (int j=0; j < no_of_erase; j++) {
       Galois t = wl[j]; 
	   if (debug) {cout << "wl_t   =: " << t << endl;}
       for (int i=0; i < no_of_erase; i++) {
	       D[i][j] *= t;
       }
    }
	
	if (debug) {
        cout << " ==== wl*D                                ======" << endl;	
       for (int i=0; i < no_of_erase ; i++){
            arrout("D[i] = ", D,i, no_of_erase);} // display D[p0,d0..n] buffer
    }   
	
   for (int i=0; i < no_of_erase; i++) {
       Galois t = wr[i]; 
	   if (debug) {cout << "wr_t   =: " << t << endl;}
       for (int j=0; j < no_of_erase; j++) {
	       D[i][j] *= t;
       }
    }
	
	if (display) {
        cout << " ==== Inverse of Cauchy Matrix            ======" << endl;	
	    for (int i=0; i < no_of_erase ; i++){
            arrout("D[i] = ", D,i, no_of_erase);} // display D[p0,d0..n] buffer
    }   
}

void track_gen(){
    cout << "***** <<     Track Data      >> *****" << endl;
    cout << " Track Data = Data Sector || Parity Sector"  << endl;
    cout << "    o Generate Data Sectors.       " << endl;
    cout << "    o Generate Parity sectors.     " << endl;
    cout << "      Num of Data Sector : " << NSEC << "... [0,1,..," << NSEC-1 <<"] data sector " <<endl;
    cout << "      Num of Prty Sector : " << PSEC << "... [0,1,..," << PSEC-1 <<"] parity sector " <<endl;
    cout << "      Sector Size        : " << SEC_SIZE << endl;
    cout << "      No of Checks       : " << NCHECKS << endl;
    SecGen(NSEC, PSEC, SEC_SIZE, NCHECKS, 2);
// Comment: Cauchy Dmatrix D=(1/(ai+b^j)), i= Parity, j= erasure location.
   Cauchy_Dmatrix(NSEC,PSEC,false, eras_loc, prty_loc, false);
	
// Comment: Cauchy Encoder: Parity Sector =Data Sector * Dmatrix
	for (int i=0; i < NSEC; i++) {
         Cauchy_Encoder(tbuffer[i], PSEC, SEC_SIZE+NCHECKS, i, 0, false); 
    }
    cout << "**  Start Track Data Generation  " << endl;
    for (int j=0; j < SEC_SIZE+NCHECKS; j++ ) {
	    for(int i=0; i < PSEC; i++) {
            tbuffer[NSEC+i][j] = P[i][j];}
    }		
    cout << " ==========  Data Sector ===========" << endl;
    for (int i=0;    i < NSEC; i++)      arrout("Data  Sector = ", tbuffer, i, SEC_SIZE+NCHECKS);
    cout << " ==========  Parity Output ===========" << endl;
    for (int i=NSEC; i < NSEC+PSEC; i++) arrout("Prty  Sector = ", tbuffer, i, SEC_SIZE+NCHECKS); 

    cout << "**  Finish Track Data Generation " << endl;
//  Comment: Reset P[i][j]=0 for syndrome generation.
    for (int i=0; i < PSEC; i++){
        for (int j=0; j < SEC_SIZE+NCHECKS; j++){    
            P[i][j] =0;}
    }		
}

void decode_test() {

    int n=0; int p;
    int g_roots=0;
    int g_parity=0;
	int max_parity=0;
    bool skipped = false; 
	
    Galois prim = Galois::primElt();
    cout << "*****  << Decoding Test >> ******" << endl;
    cout << "o Mark Erase Data Data Sectors.  " << endl;
    cout << "o Mark Erase Parity sectors.     " << endl;
// Comment: Data Skip Mask
    dskipm[0]=0;  dskipm[1]=0;  dskipm[2]=1;  dskipm[3]=0;  dskipm[4]=0;
    dskipm[5]=0;  dskipm[6]=0;  dskipm[7]=0;  dskipm[8]=0;  dskipm[9]=0;
    dskipm[10]=0; dskipm[11]=0; dskipm[12]=0; dskipm[13]=0; dskipm[14]=0;
    dskipm[15]=0; dskipm[16]=0; dskipm[17]=0; dskipm[18]=0; dskipm[19]=0;
    dskipm[20]=0; dskipm[21]=0; dskipm[22]=0; dskipm[23]=1; dskipm[24]=1;
    dskipm[25]=1; dskipm[26]=1; dskipm[27]=0; dskipm[28]=0; dskipm[29]=0;
// Comment: Parity Skip Mask
    pskipm[0]=0;  pskipm[1]=0;  pskipm[2]=0;  pskipm[3]=0;  pskipm[4]=0;
    pskipm[5]=0;  pskipm[6]=0;  pskipm[7]=0;  pskipm[8]=0;  pskipm[9]=0;
    pskipm[10]=0; pskipm[11]=0; pskipm[12]=0; pskipm[13]=0; pskipm[14]=0;
    pskipm[15]=0; pskipm[16]=0; pskipm[17]=0; pskipm[18]=0; pskipm[19]=0;

// Location of erased sectors
    for (int j=0; j < NSEC; j++){
        if (dskipm[j] == 1) {
           eras_loc[g_roots]= j;
           g_roots++;
        }
    }
// Good parity to use 
    for (int i=0; i < PSEC; i++){
        if (pskipm[i] != 1) {
           prty_loc[g_parity] = i;
		   g_parity++;
        }
    }

   arrout("dskipm[] = ", dskipm, NSEC); // display l[] buffer
   arrout("pskipm[] = ", pskipm, PSEC); // display l[] buffer

   cout << "g_roots  : " << g_roots << endl;
   cout << "g_parity : " << g_parity << endl;
   arrout("eras_loc[ ] = ", eras_loc, g_roots);  // Erased data sectors
   arrout("prty_loc[ ] = ", prty_loc, g_parity); // Good Parity Sectors

    if (g_roots <= g_parity) {
	    max_parity = g_roots;
	}
    else {
    	cout << " Error:  Roots are more than parity." << endl;
		return;
    }
	
	cout << "+++++++++++++++++ Start Decoding +++++++++++++++++++++" << endl;
	cout << " 1) Syndrome Generation:                                " << endl;
	cout << "   o Read all data except erase sectors.                " << endl;
	cout << "   o Syndrome= Good Data Sector *Dmatrix + Parity Sector" << endl;
	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++"  << endl;	
// Comment: Read in Good Data Sectors	
	for (int i=0; i < NSEC; i++) {
         Cauchy_Encoder(tbuffer[i], PSEC, SEC_SIZE+NCHECKS, i, dskipm[i],false); 
    }
// Comment: Read in Parity Sectors	
	for (int i=NSEC; i < NSEC+PSEC; i++) {
         Cauchy_Encoder(tbuffer[i], PSEC, SEC_SIZE+NCHECKS, i-NSEC, 0, true); 
    }
    cout << " ==== Syndrome Sectors =====" << endl;
	for (int i=0; i < PSEC; i++) arrout("Syndrome = ", P, i, SEC_SIZE+NCHECKS);
	
	cout << "+++++++++++++++++ Dmatrix          +++++++++++++++++++++" << endl;
	cout << " 2) Find Dmatrix D{i,j] i=good parity, j=erased sectors " << endl;
	cout << " 3) Invert Dmatrix                                      " << endl;
	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;	
// Comment: Dmatrix 
    Cauchy_Dmatrix(g_roots,max_parity,true, eras_loc, prty_loc,true);	
	
// Comment: Invert of Dmatrix
	cout << "  Invert Dmatrix Starts here.                           " << endl;
    Cauchy_Dinvert(g_roots,a,bi,true);
    cout << " ==== Syndrome Sectors =====" << endl;
	for (int i=0; i < g_roots; i++) arrout("Syndrome = ", P, i, SEC_SIZE+NCHECKS);
//************************************************************	
// Modify it for GF_multiply_region
//      ers_sector[i][j]  = P[0][j] * D[0][i] + \
//                          P[1][j] * D[1][i] + \
//                          P[2][j] * D[2][i] + \
//                          P[3][j] * D[3][i] + \
//                          P[4][j] * D[4][i] ;
// ============================================================
    for (int i=0; i < g_roots; i++) {                       // i= Jth sector
       for (int s=0; s < g_roots; s++) {                    // summation.
	      for (int j=0; j < SEC_SIZE+NCHECKS; j++) {        // j= sector data
              ers_sector[i][j]  += P[s][j] * D[s][i] ;}
      }  
    }
// ==================================================================
// Gf multiply_region                                       // int *p_ptr, *e_ptr; 
//	for (int i=0; i < g_roots; i++) {                       // for (int i=0; i < g_roots; i++ {
//                                                                e_ptr += s*(SEC_SIZE + NCHECKS);  
//    for (int s=0; s < g_roots; s++) {                     //    for (int s=0; s < g_roots; s++) {
//	      for (int j=0; j < SEC_SIZE+NCHECKS; j++) {        //       galois b_to_j = D[s][i];
//                                                          //       p_ptr += s*(SEC_SIZE + NCHECKS);
//            ers_sector[i][j]  += P[s][j] * D[s][i] ;}     //       gf.multiply_region.w32(&gf, p_ptr, e_ptr, b_to_j, SEC_SIZE+NCHECKS, 1);
//    }  
//    }
	
    cout << " ==== erased Sectors.  ======" << endl;
    for (int i=0; i < g_roots; i++) arrout("Erased Sector = ", ers_sector, i, SEC_SIZE+NCHECKS);


}


int main(int nargs, char** args) {
    track_gen();
    decode_test();
    return 0;
}



